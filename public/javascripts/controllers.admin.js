function MainController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
		$rootScope.title = $state.current.title;
		
		$scope.logout = function(){
			$cookies.remove("tokena");
			$cookies.remove("tokenSecreta");
			$location.path('/admin');
		};
		$scope.form={};
		$scope.changePassword = function(){
			if($scope.form.newpassword == $scope.form.rptpassword){
				$scope.showSpinner=true;
				$http.put('/api/users/password',
					{oldpassword:$scope.form.oldpassword,
					 newpassword:$scope.form.newpassword,
					 token:$cookies.get('tokena'),
					 tokensecret:$cookies.get('tokenSecreta')})
			   .then(function (response) {
					$scope.showSpinner=false;
					if(response.data.e===0){
						$cookies.put('token',response.data.token);
						$cookies.put('tokenSecret',response.data.tokenSecret);
						toastr.success('Haz cambiado tu contraseña','', {progressBar:true});						
					}else{
						$scope.showSpinner=false;
						toastr.error(response.data.e,'', {progressBar:true});						
					}
				}, function (error) {
					$scope.showSpinner=false;
					toastr.error('Ups!','', {progressBar:true});
				});	
			}else
				toastr.error('Las contraseñas no coinciden','', {progressBar:true});
		};
		
	}else{
		$location.path('/admin');
	}
}


function MainControllerPermisos($rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookieStore, $window) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 9;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarPermiso = true;
									$scope.subtitle = "Configuración de perfil";
								}
								else
								{
									$scope.mostrarPermiso = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});


		$rootScope.title = $state.current.title;
		$scope.pages={}
		$scope.form = {};
		$scope.list = function(){
			$scope.showSpinner = true;
			$http.get('/api/permisos/').
				success(function(data){
					$scope.showSpinner = false;
					$scope.pages = data.pages;
				});
		};
		$scope.list();

		$scope.file = false;
		$scope.send=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					name : $scope.form.name,
					pagescheck : $scope.ids
				};

				
				$http.post('/api/permisos/createperfil',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							$scope.form.name = '';
							toastr.success('Usuario agregado correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});
		};

		$scope.ids = {};
	}
}

function MainControllerPerfiles($rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookieStore, $window) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 9;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = String(response.page[0]._id);

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarListaPerfiles = true;
									$scope.subtitle = "Lista de perfiles";
								}
								else
								{
									$scope.mostrarListaPerfiles = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
		
			});

		$rootScope.title = $state.current.title;
		$scope.perfiles=[];

		$scope.list = function(){
			$scope.showSpinner = true;
			$http.get('/api/permisos/perfiles').
				success(function(data){
					$scope.showSpinner = false;
					$scope.perfiles = data.perfiles;
				});
		}
		$scope.list();

		$scope.deleteID = false;
		$scope.prepareDelete = function(id){
			$scope.deleteID = id;
		};
		
		$scope.remove = function(){
			if($scope.deleteID){
				$http.delete('/api/permisos/delete/'+$scope.deleteID ,{params:{
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta')
				}}).
				then(function(response){
					if(response.data.e===0){
						toastr.success("Perfil eliminado",'', {progressBar:true});
						$scope.list();
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});
					}
				},function(error){
					
				});
			}else
				toastr.error("Perfil inválido",'', {progressBar:true});
		};
	}
}



function MainControllerTransactionList($q,$rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window){
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 7;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarListaTransacciones = true;
									$scope.subtitle = "Lista de transacciones";
								}
								else
								{
									$scope.mostrarListaTransacciones = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});

		$rootScope.title = $state.current.title;
		
			$scope.max = 20;
			$scope.start = 0;
			$scope.term = false;
			$scope.total = 0;
			$scope.transactions = [];
			$scope.currentPage = 1;
			$scope.ratestatus = {P:'Pendiente',S:'Exitosa',C:'Cancelada',F:'Fallida'};
			
			var canceller;
			$scope.list = function(){
				canceller = $q.defer();
				var dat = {
					start : $scope.start,
					max : $scope.max,
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta')
				};
				
				
				$scope.showSpinner=true;
				$http.get('/api/transactions',{
					params:dat,
					timeout:canceller.promise}).
				success(function(data) {
					$scope.showSpinner=false;
					$scope.total = data.total;
					$scope.transactions = data.transactions;
				});
			};
			
			$scope.list();
			$scope.performSearch = function(){
				$scope.showSpinner=false;
				canceller.resolve();
				if($scope.search.length>0)
					$scope.term = $scope.search;
				else
					$scope.term = false;
				$scope.list();
			};
			
			$scope.pageChange = function(currentPage){
				$scope.currentPage = currentPage;
				$scope.start = $scope.max * (currentPage-1);
				$scope.list();
			};
		
	}else{
		$location.path('/admin');
	}
}

function MainControllerListBanners($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 8;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarListaBanners = true;
									$scope.subtitle = "Lista de banners";
								}
								else
								{
									$scope.mostrarListaBanners = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});


		$rootScope.title = $state.current.title;
		$scope.banners={};
		$scope.showSpinner=true;
		$scope.list = function(){
			$http.get('/api/site/banners').
				success(function(data) {
					$scope.showSpinner=false;
					$scope.banners = data.banners;
			});
		}
		$scope.list();
		
		
		$scope.deleteID = false;
		$scope.prepareDelete = function(id){
			$scope.deleteID = id;
		};
		
		
		$scope.remove = function(){
			if($scope.deleteID){
				$http.delete('/api/site/banner/'+$scope.deleteID ,{params:{
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta')
				}}).
				then(function(response){
					if(response.data.e===0){
						toastr.success("Banner eliminado",'', {progressBar:true});
						$scope.list();
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});
					}
				},function(error){
					
				});
			}else
				toastr.error("Banner invalido",'', {progressBar:true});
		};
		
	}else{
		$location.path('/admin');
	}
}

function addBannerController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload){
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
		$scope.idPagina = 8;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarBanners = true;
									$scope.subtitle = "Agregar banner";
								}
								else
								{
									$scope.mostrarBanners = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});

		
			$scope.form = {};
			$scope.file = false;
			$scope.changePhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
				$scope.file = $file;
			};
			
			$scope.removePhoto = function(){
				$scope.file = false;
			};
			
			$rootScope.title = $state.current.title;
				
			$scope.send=function(){
				var i=0;			
				$scope.showSpinner=true;
				
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					href: $scope.form.href,
					banner : $scope.file,
				};
					
				Upload.upload({
					url: '/api/site/banners',
					arrayKey:'',
					data: data,
					method : 'POST'
				}).then(function (response) {
					$scope.showSpinner=false;
					if(response.data.e===0){
						toastr.success('Banner agregado correctamente','', {progressBar:true});
						$scope.form.href = '';	
						$scope.removePhoto();				
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});
					}
				}, function (response) {
					console.log(response);
				}, function (evt) {
					/*$scope.progress = 
						Math.min(100, parseInt(100.0 * evt.loaded / evt.total));*/
				});
			};
			
		}else{
			$location.path('/admin');
		}
}
function MainControllerListCategory($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, $document, $templateCache) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 1;
		$scope.idPaginaEditar = 2;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarListaCategoria = true;
									$scope.subtitle = "Lista de categorías padre";
								}
								else
								{
									$scope.mostrarListaCategoria = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);

							$http.get('/api/permisos/getpage/'+$scope.idPaginaEditar)
							.success(function(data){
								$scope.showSpinner = false;
								$scope.getIdPaginaEditar = data.page[0]._id;
				
								$http.get('/api/permisos/getpagesenabled',{
									ignoreLoadingBar: true,
									params:{
										token:$cookies.get('tokena'),
										tokensecret:$cookies.get('tokenSecreta')
									}}).
									then(function(data2){
										if(data2.data.e===0){
											$scope.permisos=data2.data.permisos;
											validarIngreso = function(page){
												var result = $scope.permisos.find( x => x.pagina === page );
												if(result !== undefined)
												{
													$scope.mostrarCategoria = true;
												}
												else
												{
													$scope.mostrarCategoria = false;
												}
											};
									
											validarIngreso($scope.getIdPaginaEditar);
				
											
										}
									},function(error){
									});
							});
						}
					},function(error){
					});
			});


		
			$rootScope.title = $state.current.title;
			$scope.categories=[];
			$scope.childs = [];
			$scope.list = function(){
				$scope.showSpinner=true;
				$http.get('/api/category/parents').
				success(function(data) {
					$scope.showSpinner=false;
					$scope.categories = data.categories;
				});	
			};	
			$scope.list();
			
			
			$scope.listChilds = function(id_parent){
				$scope.showSpinner=true;
				$scope.childs = [];
				$http.get('/api/category/childs/id/'+id_parent).
				success(function(data) {
					$scope.showSpinner=false;
					$document.scrollToElementAnimated(childtop);
					$scope.childs = data.categories;
				});
			};
			
			$scope.deleteID = false;
			$scope.prepareDelete = function(id){
				$scope.deleteID = id;
			};
			
			$scope.remove = function(){
				if($scope.deleteID){
					$http.delete('/api/category/delete/'+$scope.deleteID ,{params:{
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta')
					}}).
					then(function(response){
						if(response.data.e===0){
							toastr.success("Categoria eliminada",'', {progressBar:true});
							$scope.list();
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					},function(error){
						
					});
				}else
					toastr.error("Categoria invalida",'', {progressBar:true});
			};
		
		
	}else{
		$location.path('/admin');
	}
}

function MainControllerPermisosEdit($rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookieStore, $window, filterFilter){
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 9;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarPerfil = true;
									$scope.subtitle = "Ver perfil";
								}
								else
								{
									$scope.mostrarPerfil = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});

		$scope.form = {};
		$scope.id = $stateParams.id;
		$rootScope.title = $state.current.title;
		$scope.pages={};
		$scope.pageschecked={};
		$scope.pagescheck={};
		$scope.showSpinner = true;

		$scope.list = function(){
			$scope.showSpinner=true;
			$http.get('/api/permisos/').
			success(function(data) {
				$scope.showSpinner=false;
				$scope.pages = data.pages;
			});	
		};	
		$scope.list();

		$scope.encontrarID = function(page){
			var array = $scope.pagescheck;
			var result = array.find( x => x.pagina === page._id );
			if(result !== undefined)
			  return true;
			else
			  return false;
			console.log(array);
		};

		$http.get('/api/permisos/editar/'+$scope.id)
		.success(function(response){
			$scope.showSpinner = false;
			$scope.pagescheck = response.permisos;
		});

		$http.get('/api/permisos/edit/'+$scope.id)
		.success(function(response){
			$scope.showSpinner = false;
			$scope.pageschecked = response.permisos;
		});

		$http.get('/api/permisos/getname/'+$scope.id)
		.success(function(response){
			$scope.showSpinner = false;
			$scope.form.name = response.nombrePerfil.name;
		});


	}
}


function MainControllerCategoryEdit($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

		$scope.idPagina = 2;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarCategoria = true;
									$scope.subtitle = "Editar categoría";
								}
								else
								{
									$scope.mostrarCategoria = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});

		
			$scope.form = {};
			$scope.file = false;
			$scope.changePhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
				$scope.file = $file;
			};
			
			$scope.removePhoto = function(){
				$scope.file = false;
			};
			$scope.id = $stateParams.id;
			$rootScope.title = $state.current.title;
	
			$scope.categorys={};
			$scope.showSpinner=true;
			$http.get('/api/category/parents').
				success(function(data, status, headers, config) {
					$scope.categories = data.categories;
					
					$http.get('/api/category/details/'+$scope.id)
						.then(function(response){
							$scope.showSpinner=false;
							if(response.data.e==0){
								$scope.form.name = response.data.category.name;
								$scope.form.categoryparent = response.data.category.parent;
								$scope.editfile = response.data.category.image;
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
						});
			});
				
			$scope.send=function(){
				var i=0;			
					$scope.showSpinner=true;
					var data = {
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta'),
						name : $scope.form.name,
						parent : $scope.form.categoryparent || 0
					};
					if($scope.file)
						data.categorypic = $scope.file;
					
					Upload.upload({
							url: '/api/category/edit/'+$scope.id,
							arrayKey:'',
							data: data,
							method: 'PUT'
						}).then(function (response) {
							$scope.showSpinner=false;
							console.log(response);
							if(response.data.e===0){
								$scope.removePhoto();
								toastr.success('Categoria editada correctamente','', {progressBar:true});
								$scope.form.name = '';
								$scope.form.categoryparent = '';
								$location.path('/admin/main/categorylist');						
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
								
							
						}, function (response) {
							console.log(response);
						}, function (evt) {
						});
			};
			
		}else{
			$location.path('/admin');
		}	
}

function MainControllerCategory($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

			$scope.idPagina = 2;
			$scope.permisos={};
			$http.get('/api/permisos/getpage/'+$scope.idPagina)
				.success(function(response){
					$scope.showSpinner = false;
					$scope.getIdPagina = response.page[0]._id;

					$http.get('/api/permisos/getpagesenabled',{
						ignoreLoadingBar: true,
						params:{
							token:$cookies.get('tokena'),
							tokensecret:$cookies.get('tokenSecreta')
						}}).
						then(function(response2){
							if(response2.data.e===0){
								$scope.permisos=response2.data.permisos;
								validarIngreso = function(page){
									var result = $scope.permisos.find( x => x.pagina === page );
									if(result !== undefined)
									{
										$scope.mostrarCategoria = true;
										$scope.subtitle = "Agregar categoría";
									}
									else
									{
										$scope.mostrarCategoria = false;
										$scope.subtitle = "No puede acceder a esta categoría";
									}
								};
						
								validarIngreso($scope.getIdPagina);
							}
						},function(error){
						});
				});
	

		
			$scope.form = {};
			$scope.file = false;
			$scope.changePhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
				$scope.file = $file;
			};
			
			$scope.removePhoto = function(){
				$scope.file = false;
			};
			
			$rootScope.title = $state.current.title;
	
			$scope.categorys={};
			$http.get('/api/category/parents').
				success(function(data, status, headers, config) {
					$scope.categories = data.categories;
			});
				
			$scope.send=function(){
				var i=0;			
					$scope.showSpinner=true;
					var data = {
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta'),
						name : $scope.form.name,
						parent : $scope.form.categoryparent || 0,
						categorypic: $scope.file
					};
					
					Upload.upload({
							url: '/api/category',
							arrayKey:'',
							data: data
						}).then(function (response) {
							$scope.showSpinner=false;
							console.log(response);
							if(response.data.e===0){
								$scope.removePhoto();
								toastr.success('Item agregado correctamente','', {progressBar:true});
								$scope.form.name = '';
								$scope.form.categoryparent = '';
								//$location.path('/shop/'+decodeURIComponent(response.data.url));						
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
								
							
						}, function (response) {
							console.log(response);
						}, function (evt) {
							/*$scope.progress = 
								Math.min(100, parseInt(100.0 * evt.loaded / evt.total));*/
						});
	
			};
			
		}else{
			$location.path('/admin');
		}
}


function MainControllerListUser($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

			
			$scope.idPagina = 5;
			$scope.permisos={};
			$scope.idPaginaEditar = 6;
			$http.get('/api/permisos/getpage/'+$scope.idPagina)
				.success(function(response){
					$scope.showSpinner = false;
					$scope.getIdPagina = response.page[0]._id;

					$http.get('/api/permisos/getpagesenabled',{
						ignoreLoadingBar: true,
						params:{
							token:$cookies.get('tokena'),
							tokensecret:$cookies.get('tokenSecreta')
						}}).
						then(function(response2){
							if(response2.data.e===0){
								$scope.permisos=response2.data.permisos;
								validarIngreso = function(page){
									var result = $scope.permisos.find( x => x.pagina === page );
									if(result !== undefined)
									{
										$scope.mostrarListaUsuarios = true;
										$scope.subtitle = "Lista de usuarios";
									}
									else
									{
										$scope.mostrarListaUsuarios = false;
										$scope.subtitle = "No puede acceder a esta categoría";
									}
								};
						
								validarIngreso($scope.getIdPagina);

								
								$http.get('/api/permisos/getpage/'+$scope.idPaginaEditar)
								.success(function(data){
									$scope.showSpinner = false;
									$scope.getIdPaginaEditar = data.page[0]._id;
				
									$http.get('/api/permisos/getpagesenabled',{
										ignoreLoadingBar: true,
										params:{
											token:$cookies.get('tokena'),
											tokensecret:$cookies.get('tokenSecreta')
										}}).
										then(function(data2){
											if(data2.data.e===0){
												$scope.permisos=data2.data.permisos;
												validarIngreso = function(page){
													var result = $scope.permisos.find( x => x.pagina === page );
													if(result !== undefined)
													{
														$scope.paginaEditar = true;
													}
													else
													{
														$scope.paginaEditar = false;
													}
												};
										
												validarIngreso($scope.getIdPaginaEditar);
				
												
											}
										},function(error){
										});
								});

							}
						},function(error){
						});
				});
			
			$scope.max = 20;
			$scope.start = 0;
			$scope.term = false;
			$scope.total = 0;
			$scope.users = {};
			$scope.currentPage = 1;
			$scope.privs = ['','Usuario','Administrador'];


			$scope.perfiles = [];
			$http.get('/api/permisos/getperfiles').
			success(function(data) {
				$scope.showSpinner=false;
				$scope.perfiles = data.perfiles;
			});

			$scope.encontrarPerfil = function(perfil){
				var array = $scope.perfiles;
				var result = array.find( x => x._id === perfil );
				if(result !== undefined)
				  return result.name;
				else
				  return "Sin privilegios";
			};


			
			var canceller;
			$scope.list = function(){
				canceller = $q.defer();
				var dat = {
					start : $scope.start,
					max : $scope.max,
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta')
				};
				
				if($scope.term)
					dat.term = $scope.term;
				
				$scope.showSpinner=true;
				$http.get('/api/search/users',{
					params:dat,
					timeout:canceller.promise}).
				success(function(data) {
					$scope.showSpinner=false;
					$scope.total = data.total;
					$scope.users = data.users;
				});
			};
			
			$scope.deleteID = false;
			$scope.enableID = false;
			$scope.prepareDelete = function(id){
				$scope.deleteID = id;
			};
			$scope.prepareActivate = function(id){
				$scope.enableID = id;
			};
			
			$scope.remove = function(){
				if($scope.deleteID){
					$http.delete('/api/users/disable/'+$scope.deleteID ,{params:{
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta')
					}}).
					then(function(response){
						if(response.data.e===0){
							toastr.success("Usuario eliminado",'', {progressBar:true});
							$scope.list();
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					},function(error){
						
					});
				}else
					toastr.error("Usuario invalido",'', {progressBar:true});
			};
			
			$scope.activate = function(){
				if($scope.enableID){
					$http.post('/api/users/enable/'+$scope.enableID ,{
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta')
					}).
					then(function(response){
						if(response.data.e===0){
							toastr.success("Usuario re-activado",'', {progressBar:true});
							$scope.list();
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					},function(error){
						
					});
				}else
					toastr.error("Usuario invalido",'', {progressBar:true});
			};
			
			
			$scope.list();
			$scope.performSearch = function(){
				$scope.showSpinner=false;
				canceller.resolve();
				if($scope.search.length>0)
					$scope.term = $scope.search;
				else
					$scope.term = false;
				$scope.list();
			};
			
			$scope.pageChange = function(currentPage){
				$scope.currentPage = currentPage;
				$scope.start = $scope.max * (currentPage-1);
				$scope.list();
			};
			
			
		}else{
			$location.path('/admin');
		}
}

function MainControllerUser($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

			$scope.idPagina = 6;
			$scope.permisos={};
			$http.get('/api/permisos/getpage/'+$scope.idPagina)
				.success(function(response){
					$scope.showSpinner = false;
					$scope.getIdPagina = response.page[0]._id;

					$http.get('/api/permisos/getpagesenabled',{
						ignoreLoadingBar: true,
						params:{
							token:$cookies.get('tokena'),
							tokensecret:$cookies.get('tokenSecreta')
						}}).
						then(function(response2){
							if(response2.data.e===0){
								$scope.permisos=response2.data.permisos;
								validarIngreso = function(page){
									var result = $scope.permisos.find( x => x.pagina === page );
									if(result !== undefined)
									{
										$scope.mostrarUsuario = true;
										$scope.subtitle = "Agregar usuario";
									}
									else
									{
										$scope.mostrarUsuario = false;
										$scope.subtitle = "No puede acceder a esta categoría";
									}
								};
						
								validarIngreso($scope.getIdPagina);
							}
						},function(error){
						});
				});
	

		
		$scope.form = {};
		$rootScope.title = $state.current.title;
		$scope.file = false;
		$scope.perfiles=[];
		
		$scope.subtitle = "Agregar usuario";
		
		$scope.list = function(){
			$scope.showSpinner = true;
			$http.get('/api/permisos/perfiles').
				success(function(data){
					$scope.showSpinner = false;
					$scope.perfiles = data.perfiles;
				});
		}
		$scope.list();


		$scope.send=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					user : $scope.form.user,
					priv: $scope.form.priv,
					mail: $scope.form.mail,
					password: $scope.form.password,
					perfil: $scope.form.perfil
				};
				
				$http.post('/api/users/createuser',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							$scope.form.user = '';
							$scope.form.priv = '';
							$scope.form.mail = '';
							$scope.form.password = '';
							$scope.form.perfil = '';
							toastr.success('Usuario agregado correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});

		};
			
		}else{
			$location.path('/admin');
		}
}

function MainControllerListProduct($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
			if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

				$scope.idPagina = 3;
				$scope.idPaginaEditar = 4;
				$scope.permisos={};
				$http.get('/api/permisos/getpage/'+$scope.idPagina)
					.success(function(response){
						$scope.showSpinner = false;
						$scope.getIdPagina = response.page[0]._id;

						$http.get('/api/permisos/getpagesenabled',{
							ignoreLoadingBar: true,
							params:{
								token:$cookies.get('tokena'),
								tokensecret:$cookies.get('tokenSecreta')
							}}).
							then(function(response2){
								if(response2.data.e===0){
									$scope.permisos=response2.data.permisos;
									validarIngreso = function(page){
										var result = $scope.permisos.find( x => x.pagina === page );
										if(result !== undefined)
										{
											$scope.mostrarListaProductos = true;
											$scope.subtitle = "Lista de productos";
										}
										else
										{
											$scope.mostrarListaProductos = false;
											$scope.subtitle = "No puede acceder a esta sección";
										}
									};
							
									validarIngreso($scope.getIdPagina);

									$http.get('/api/permisos/getpage/'+$scope.idPaginaEditar)
									.success(function(data){
										$scope.showSpinner = false;
										$scope.getIdPaginaEditar = data.page[0]._id;
				
										$http.get('/api/permisos/getpagesenabled',{
											ignoreLoadingBar: true,
											params:{
												token:$cookies.get('tokena'),
												tokensecret:$cookies.get('tokenSecreta')
											}}).
											then(function(data2){
												if(data2.data.e===0){
													$scope.permisos=response2.data.permisos;
													validarIngreso = function(page){
														var result = $scope.permisos.find( x => x.pagina === page );
														if(result !== undefined)
														{
															$scope.editarProductos = true;
														}
														else
														{
															$scope.editarProductos = false;
														}
													};
											
													validarIngreso($scope.getIdPaginaEditar);
				
													
												}
											},function(error){
											});
									});
						
								}
							},function(error){
							});
					});
		
	

				$scope.max = 20;
				$scope.start = 0;
				$scope.term = false;
				$scope.total = 0;
				$scope.items = [];
				$scope.currentPage = 1;
				$scope.statuses = {S:'Servicio',U:'Usado',N:'Nuevo',SN:'Semi nuevo'};
				$scope.types = {S:'Venta',A:'Subasta',L:'Lote'};
				$scope.actives = ['','Activa','Pausada','Finalizada','Eliminada'];
				$scope.statusfilter = 'N';
				$scope.typefilter = 'S';
				$scope.activefilter = '1';
				
				
				var canceller;
				$scope.list = function(){
					canceller = $q.defer();
					var dat = {
						start : $scope.start,
						max : $scope.max,
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta'),
						status: $scope.statusfilter,
						type: $scope.typefilter,
						active: $scope.activefilter
					};
					
					if($scope.term)
						dat.term = $scope.term;
					
					$scope.showSpinner=true;
					$http.get('/api/search/admin/item',{
						params:dat,
						timeout:canceller.promise}).
					success(function(data) {
						$scope.showSpinner=false;
						$scope.total = data.total;
						$scope.items = data.items;
					});
				};
				
				$scope.deleteID = false;
				$scope.prepareDelete = function(id){
					$scope.deleteID = id;
				};
				
				$scope.remove = function(){
					if($scope.deleteID){
						$http.delete('/api/items/delete/'+$scope.deleteID ,{params:{
							token : $cookies.get('tokena'),
							tokensecret : $cookies.get('tokenSecreta')
						}}).
						then(function(response){
							if(response.data.e===0){
								toastr.success("Producto eliminado",'', {progressBar:true});
								$scope.list();
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
						},function(error){
							
						});
					}else
						toastr.error("Producto invalido",'', {progressBar:true});
				};
				
				
				$scope.list();
				$scope.performSearch = function(){
					$scope.showSpinner=false;
					canceller.resolve();
					if($scope.search.length>0)
						$scope.term = $scope.search;
					else
						$scope.term = false;
					$scope.list();
				};
				
				$scope.pageChange = function(currentPage){
					$scope.currentPage = currentPage;
					$scope.start = $scope.max * (currentPage-1);
					$scope.list();
				};
			}else{
				$location.path('/admin');
			}
}

function ItemsEdit($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,NgMap,Upload) {
		$rootScope.title = $state.current.title;
		
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
			if ($cookies.get('tokena').length<9 || $cookies.get('tokenSecreta').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		$scope.categories=[];
		$scope.childs=[];
		$scope.form={};
		$scope.itemdata=[];
		$scope.files0=[];
		$scope.files=[];

		$http.get('/api/category/parents').
			success(function(data, status, headers, config) {
			  $scope.categories = data.categories;
		});
	
			$scope.getchilds = function(id) {
						$scope.states={};
						$http
						.get('/api/category/childs/id/'+id, {
							params: {
								//source: link
							}
						 })
						 .success(function (data,status) {
								//console.log(data)
								$scope.childs = data.categories;		
						 });
					   
			};
			$scope.remove = function(index) {
				$scope.files.splice(index, 1);
			};		
	 
		$http.get('/api/items/details/'+$stateParams.id,{
				params: {
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}	
				}).
			success(function(data, status, headers, config) {
			var i=0;
			  $scope.itemdata = data.item;
			  $scope.form.name = data.item.name;
			  $scope.form.descriptionShort = data.item.descriptionShort;
			  $scope.form.descriptionLong = data.item.descriptionLong;
			  $scope.form.price = data.item.price;
			  $scope.form.stock = data.item.stock;
			  $scope.form.parent = data.item.parentCategory;
			  $scope.form.type = data.item.type;
			  $scope.form.active = ""+data.item.active;
			  $scope.form.status = data.item.status;
			  $scope.form.location = data.item.location;
				$http
				.get('/api/category/childs/id/'+data.item.parentCategory, {
					params: {
						//source: link
					}
				 })
				 .success(function (data,status) {
						//console.log(data)
						$scope.childs = data.categories;		
				 });	
			  $scope.form.category = data.item.category;				 
			  for(;i<data.item.images.length;i++){
				  $scope.files0.push('uploads/250/'+data.item.images[i]);
			  }
			  //$scope.files = data.item.images;
		});
		
		var myMap,countmarket=false;
		$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh_QtBdAh9G2ZAczx6ZdAhJqISBI8QGUU";
		NgMap.getMap().then(function(map) {
			myMap = map;
			myMap.addListener('click', function(e) {
				if(countmarket)
					countmarket.setPosition(e.latLng);
				else{
						countmarket = new google.maps.Marker({
						position: e.latLng, 
						map: myMap,
						draggable:true
					});			
				}
			});		
		});
			
		$scope.uploadPhotos = function(){
			
			if($scope.files.length>0){
				var data = {
					itempics: $scope.files,
					token:$cookies.get('tokena'),
					tokensecret:$cookies.get('tokenSecreta')
				};
			
			
				Upload.upload({
					url: '/api/items/changepictures/'+$stateParams.id,
					arrayKey:'',
					method:'PUT',
					data: data
				}).then(function (response) {
					$scope.showSpinner=false;
					console.log(response);
					if(response.data.e==0){
						toastr.success('Item agregado correctamente','', {progressBar:true});
							$location.path('/shop/'+decodeURIComponent(response.data.url));						
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
							
						
					}, function (response) {
						console.log(response);
					}, function (evt) {
						/*$scope.progress = 
							Math.min(100, parseInt(100.0 * evt.loaded / evt.total));*/
					});
			}else
				toastr.error("Debes seleccionar al menos una foto",'', {progressBar:true});
		};
	
	
		$scope.send=function(){
			$scope.showSpinner=true;
			var data = {
				token : $cookies.get('tokena'),
				tokensecret : $cookies.get('tokenSecreta'),
				active: $scope.form.active,
				name : $scope.form.name,
				descriptionShort : $scope.form.descriptionShort,
				descriptionLong : $scope.form.descriptionLong,
				price : $scope.form.price,
				stock : $scope.form.stock,
				category : $scope.form.category,
				location : $scope.form.location,
				status : $scope.form.status,
				type : $scope.form.type,
				time: $scope.form.time,
				active: $scope.form.active
			};
				
			if(countmarket){
				data.lat = countmarket.position.lat();
				data.lng = countmarket.position.lng();
			}
			
			
			$http.put('/api/items/edit/'+$stateParams.id, data)
			   .then(function (response) {
					$scope.showSpinner=false;
					if(response.data.e==0){
						toastr.success('Articulo actualizado','', {progressBar:true});						
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});						
					}
				}, function (error) {
					$scope.showSpinner=false;
					toastr.error('Ups!','', {progressBar:true});
				});
		};
}


function MainControllerProduct($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

			$scope.idPagina = 4;
			$scope.permisos={};
			$http.get('/api/permisos/getpage/'+$scope.idPagina)
				.success(function(response){
					$scope.showSpinner = false;
					$scope.getIdPagina = response.page[0]._id;

					$http.get('/api/permisos/getpagesenabled',{
						ignoreLoadingBar: true,
						params:{
							token:$cookies.get('tokena'),
							tokensecret:$cookies.get('tokenSecreta')
						}}).
						then(function(response2){
							if(response2.data.e===0){
								$scope.permisos=response2.data.permisos;
								validarIngreso = function(page){
									var result = $scope.permisos.find( x => x.pagina === page );
									if(result !== undefined)
									{
										$scope.mostrarProducto = true;
										$scope.subtitle = "Agregar producto";
									}
									else
									{
										$scope.mostrarProducto = false;
										$scope.subtitle = "No puede acceder a esta categoría";
									}

								};
						
								validarIngreso($scope.getIdPagina);

							}
						},function(error){
						});
				});
	

		
			$scope.form = {};
			$rootScope.title = $state.current.title;
			$scope.file = false;
			$scope.categorys={};
			$http.get('/api/category/parents').
				success(function(data, status, headers, config) {
					$scope.categories = data.categories;
				});
			
			$scope.brands={};
			$http.get('/api/brands').
				success(function(data, status, headers, config) {
					$scope.brands = data.brands;
				});
				
			$scope.send=function(){
				var i=0;			
					$scope.showSpinner=true;
					var data = {
						token : $cookies.get('tokena'),
						tokensecret : $cookies.get('tokenSecreta'),
						name : $scope.form.name,
						descriptionshort: $scope.form.descriptionshort,
						descriptionlong: $scope.form.descriptionlong,
						itempics: $scope.files,
						category: $scope.form.category,
						size: $scope.form.size,
						brand: $scope.form.brand,
						meassure: $scope.form.medida,
						price: $scope.form.price,
						tax: $scope.form.tax,
						gtin: $scope.form.gtin
					};
					
					/*if(countmarket){
						data.lat = countmarket.position.lat();
						data.lng = countmarket.position.lng();
					}*/
					console.log(data);
					Upload.upload({
							url: '/api/items',
							arrayKey:'',
							data: data
						}).then(function (response) {
							$scope.showSpinner=false;
							console.log(response);
							if(response.data.e===0){
								toastr.success('Item agregado correctamente','', {progressBar:true});
								//$location.path('/shop/'+decodeURIComponent(response.data.url));						
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
								
							
						}, function (response) {
							console.log(response);
						}, function (evt) {
							/*$scope.progress = 
								Math.min(100, parseInt(100.0 * evt.loaded / evt.total));*/
						});
	
			};
			
		}else{
			$location.path('/admin');
		}
}

function LoginController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
			$location.path('/admin/main');
		}
		
		
		$rootScope.title = $state.current.title;
		$scope.send=function(){
		$scope.showSpinner=true;
		var formdata=new FormData();
		formdata.append("username",$scope.form.username);
		formdata.append("password",$scope.form.password);
		var data={
			username:$scope.form.username,
			password:$scope.form.password
		};
		var request = {
                    method: 'POST',
                    url: '/api/users/loginadmin',
                    data: data
                };

        $http(request)
            .success(function (d) {
				$scope.showSpinner=false;
				if(d.e===0){
                    $cookies.put('tokena',d.token);
					$cookies.put('tokenSecreta',d.tokenSecret);
					toastr.success('Logeo EXITOS','', {progressBar:true});
					$location.path('/admin/main');
				}else{
					toastr.error(d.mes,'', {progressBar:true});
				}
            })
           .error(function () {
        });
	};
}

function MainControllerFooter($rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookies, $window) {
	if($cookies.get('tokena') && $cookies.get('tokenSecreta')){
		$rootScope.title = $state.current.title;

		$scope.idPagina = 8;
		$scope.permisos={};
		$http.get('/api/permisos/getpage/'+$scope.idPagina)
			.success(function(response){
				$scope.showSpinner = false;
				$scope.getIdPagina = response.page[0]._id;

				$http.get('/api/permisos/getpagesenabled',{
					ignoreLoadingBar: true,
					params:{
						token:$cookies.get('tokena'),
						tokensecret:$cookies.get('tokenSecreta')
					}}).
					then(function(response2){
						if(response2.data.e===0){
							$scope.permisos=response2.data.permisos;
							validarIngreso = function(page){
								var result = $scope.permisos.find( x => x.pagina === page );
								if(result !== undefined)
								{
									$scope.mostrarFooter = true;
									$scope.subtitle = "Información del footer";
								}
								else
								{
									$scope.mostrarFooter = false;
									$scope.subtitle = "No puede acceder a esta categoría";
								}
							};
					
							validarIngreso($scope.getIdPagina);
						}
					},function(error){
					});
			});

		//$scope.subtitle = "Información del footer";

		$scope.form={};
		$http.get('/api/footer/contacto')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form._idcontacto = data.result._id;
				$scope.form.email = data.result.email;
				$scope.form.direccion = data.result.direccion;
				$scope.form.telefono = data.result.telefono;
			}
		});

		$http.get('/api/footer/social')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form._idredes = data.result._id;
				$scope.form.instagram = data.result.instagram;
				$scope.form.facebook = data.result.facebook;
				$scope.form.youtube = data.result.youtube;
				$scope.form.twitter = data.result.twitter;
			}
		});

		$http.get('/api/footer/terminos')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form._idterminos = data.result._id;
				$scope.form.tituloterminos = data.result.titulo;
				$scope.htmlVariable = data.result.texto;
			}
		});

		$http.get('/api/footer/privacidad')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form._idprivacidad = data.result._id;
				$scope.form.tituloprivacidad = data.result.titulo;
				$scope.htmlVariablePrivacidad = data.result.texto;
			}
		});

		$http.get('/api/footer/quienes')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form._idquienes = data.result._id;
				$scope.htmlVariable = data.result.texto;
			}
		});

		$scope.send=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					email : $scope.form.email,
					direccion : $scope.form.direccion,
					telefono : $scope.form.telefono,
					_id : $scope.form._idcontacto
				};

				
				$http.post('/api/footer/actualizarcontacto',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							toastr.success('Datos de contacto actualizados correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});
		};

		$scope.send1=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					instagram : $scope.form.instagram,
					facebook : $scope.form.facebook,
					youtube : $scope.form.youtube,
					twitter : $scope.form.twitter,
					_id : $scope.form._idredes
				};

				
				$http.post('/api/footer/actualizarsociales',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							toastr.success('Datos de redes sociales actualizados correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});
		};


		$scope.send2=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					texto : $scope.htmlVariable,
					id : $scope.form._idterminos,
					titulo : $scope.form.tituloterminos
				};

				
				$http.post('/api/footer/actualizarterminos',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							toastr.success('Datos actualizados correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});
		};

		$scope.send3=function(){
			var i=0;			
				$scope.showSpinner=true;
				var data = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					texto : $scope.htmlVariablePrivacidad,
					id : $scope.form._idprivacidad,
					titulo : $scope.form.tituloprivacidad
				};

				
				$http.post('/api/footer/actualizarprivacidad',data).then(function(response){
						$scope.showSpinner=false;
						console.log(response);
						if(response.data.e===0){
							toastr.success('Datos actualizados correctamente','', {progressBar:true});					
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					});
		};



	}
}

function addTaxController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window, Upload) {
		if($cookies.get('tokena') && $cookies.get('tokenSecreta')){

			$scope.idPagina = 8;
			$scope.permisos={};
			$http.get('/api/permisos/getpage/'+$scope.idPagina)
				.success(function(response){
					$scope.showSpinner = false;
					$scope.getIdPagina = response.page[0]._id;

					$http.get('/api/permisos/getpagesenabled',{
						ignoreLoadingBar: true,
						params:{
							token:$cookies.get('tokena'),
							tokensecret:$cookies.get('tokenSecreta')
						}}).
						then(function(response2){
							if(response2.data.e===0){
								$scope.permisos=response2.data.permisos;
								validarIngreso = function(page){
									var result = $scope.permisos.find( x => x.pagina === page );
									if(result !== undefined)
									{
										$scope.mostrarComision = true;
										$scope.subtitle = "Definir comisión";
									}
									else
									{
										$scope.mostrarComision = false;
										$scope.subtitle = "No puede acceder a esta categoría";
									}
								};
						
								validarIngreso($scope.getIdPagina);
							}
						},function(error){
						});
				});
	

			$rootScope.title = $state.current.title;
			$scope.form={};
			$http.get('/api/site/tax').
				success(function(data, status, headers, config) {
					if(data.e===0)
						$scope.form.tax = data.tax;
				});
				
			$scope.send = function(){
				var dat = {
					token : $cookies.get('tokena'),
					tokensecret : $cookies.get('tokenSecreta'),
					tax : $scope.form.tax
				};
				
				$scope.showSpinner=true;
				$http.post('/api/site/tax',dat).then(function(response){
					$scope.showSpinner=false;
					if(response.data.e===0){
						toastr.success('Comisión actualizada correctamente','', {progressBar:true});					
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});
					}
				});
				
			};
			
		}else{
			$location.path('/admin');
		}
}