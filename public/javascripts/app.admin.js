
// Declare app level module which depends on filters, and services
var kulbidAdmin = angular.module('kulbidAdmin', ['angularUtils.directives.dirPagination','bw.paging','ngFileUpload','angularSpinner','ngMap', 'textAngular','angular-loading-bar','timer', 'ngAnimate', 'ui.router', 'ngCookies', 'duScroll']);



kulbidAdmin.config(function($locationProvider) {
	$locationProvider.html5Mode({
	  enabled: true
	});
});


kulbidAdmin.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider.
		state('/admin', {
			url: '/admin',
			title: 'Kulbid | Ingresar',
			templateUrl: '/admin/views/index',
			controller: LoginController
			}).
		  state('/admin/main', {
			url: '/admin/main',
			title: 'Kulbid | Panel Administrativo',
			templateUrl: '/admin/views/main',
			controller: MainController
		  }).
		  state('/admin/main.category', {
			url: '/category',
			title: 'Kulbid | Agregar categoria',
			templateUrl: '/admin/views/viewcategory',
			controller: MainControllerCategory
		  }).
		  state('/admin/main.categoryedit', {
			url: '/category/:id',
			title: 'Kulbid | Editar categoria',
			templateUrl: '/admin/views/viewcategory',
			controller: MainControllerCategoryEdit
		  }).
		  state('/admin/main.products', {
			url: '/products',
			title: 'Kulbid | Agregar producto',
			templateUrl: '/admin/views/viewproducts',
			controller: MainControllerProduct
		  }).
		  state('/admin/main.user', {
			url: '/user',
			title: 'Kulbid | Agregar usuario',
			templateUrl: '/admin/views/viewuser',
			controller: MainControllerUser
		  }).
		  state('/admin/main.listusers',{
			url: '/listusers',
			title: 'Kulbid | Lista de usuarios',
			templateUrl: '/admin/views/listusers',
			controller: MainControllerListUser
		  }).
		  state('/admin/main.banner',{
			url: '/banner',
			title: 'Kulbid | Agregar banner',
			templateUrl: '/admin/views/viewbanner',
			controller: addBannerController
		  }).
		  state('/admin/main.bannerlist',{
			url: '/bannerlist',
			title: 'Kulbid | Lista de banners',
			templateUrl: '/admin/views/listbanner',
			controller: MainControllerListBanners
		  }).
		  state('/admin/main.categorylist', {
			url: '/categorylist',
			title: 'Kulbid | Listar categorias',
			templateUrl: '/admin/views/listcategory',
			controller: MainControllerListCategory
		  }).
		  state('/admin/main.listproduct', {
			url: '/listproduct',
			title: 'Kulbid | Listar productos',
			templateUrl: '/admin/views/listproducts',
			controller: MainControllerListProduct
		  }).
		  state('/admin/main.productsedit', {
			url: '/products/:id',
			title: 'Kulbid | Editar articulo',
			templateUrl: '/admin/views/itemsedit',
			controller: ItemsEdit
		  }).
		  state('/', {
			url: '/admin',
			title: 'Kulbid | Ingresar',
			templateUrl: '/admin/views/index',
			controller: LoginController
		  }).
		  state('/admin/main.tax',{
			url: '/taxed',
			title: 'Kulbid | Definir comisión',
			templateUrl: 'admin/views/viewtax',
			controller: addTaxController
		  }).
		  state('/admin/main.transactions',{
			url: '/transactions',
			title: 'Kulbid | Listado de transacciones',
			templateUrl: '/admin/views/listtransactions',
			controller: MainControllerTransactionList
			}).
			state('/admin/main.permisos',{
				url: '/permisos',
				title: 'Kulbid | Configuración de permisos',
				templateUrl: '/admin/views/permisos',
				controller: MainControllerPermisos
			}).
			state('/admin/main.perfiles',{
				url: '/perfiles',
				title: 'Kulbid | Lista de perfiles',
				templateUrl: '/admin/views/perfiles',
				controller: MainControllerPerfiles
			}).
			state('/admin/main.permisosedit',{
				url: '/permisos/:id',
				title: 'Kulbid | Editar permisos de perfil',
				templateUrl: '/admin/views/verpermisos',
				controller: MainControllerPermisosEdit
			}).
			state('/admin/main.footer',{
				url: '/footer',
				title: 'Kulbid | Footer',
				templateUrl: '/admin/views/footer',
				controller: MainControllerFooter
			});
});

kulbidAdmin.factory('Page', function() {
   var title = 'balance';
   return {
     title: function() { return title; },
     setTitle: function(newTitle) { title = newTitle; }
   };
});

kulbidAdmin.value('duScrollDuration', 2000).value('duScrollOffset', 125);
kulbidAdmin.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({color: '#ff9800',position:'fixed',top:'50%'});
}]);