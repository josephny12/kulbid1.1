var kulbid = angular.module('kulbid', ['angularUtils.directives.dirPagination','bw.paging','ngFileUpload','angularSpinner','textAngular','ngMap','angular-loading-bar','timer', 'ngAnimate', 'ui.router', 'ngCookies', 'duScroll']);


kulbid.config(function($locationProvider) {
	$locationProvider.html5Mode({
	  enabled: true
	});
});


kulbid.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider.
		  state('index', {
			abstract: true,
			title: 'Kulbid',
			templateUrl: 'views/index2',
			controller: index2
		  })
		  .state('index.cats',{
			url: '/',
			title: 'Kulbid',
			templateUrl: 'views/index2main',
			controller: index2main
		  })
		  .state('index.category', {
			url: '/category/:main',
			title: 'Kulbid',
			templateUrl: 'views/index2cats',
			controller: index2cats
		  }).
		  state('index.sub', {
			url: '/subcategory/:sub',
			title: 'Kulbid',
			templateUrl: 'views/index2subs',
			controller: index2subs
		  }).
		  state('/notify', {
			url: '/notify',
			title: 'Kulbid | Notificacion',
			templateUrl: 'views/spam'
		  }).
		  state('/register', {
			url: '/register',
			title: 'Kulbid | Registrate',
			templateUrl: 'views/register',
			controller: RegisterController
		  }).
		  state('/register-personal', {
			url: '/register-personal',
			title: 'Kulbid | Registrate',
			templateUrl: 'views/register_personal',
			controller: RegisterControllerPersonal
		  }).
		  state('/register-store', {
			url: '/register-store',
			title: 'Kulbid | Registrate',
			templateUrl: 'views/register_store',
			controller: RegisterControllerStore
		  }).
		  state('/items', {
			url: '/items',
			title: 'Kulbid | Subir articulo',
			templateUrl: 'views/items',
			controller: ItemsRegister
		  }).
		  state('/item/edit/:id', {
			url: '/item/edit/:id',
			title: 'Kulbid | Editar articulo',
			templateUrl: 'views/itemsedit',
			controller: ItemsEdit
		  }).
		  state('/uploadlote', {
			url: '/uploadlote',
			title: 'Kulbid | Subir lote',
			templateUrl: 'views/uploadlote',
			controller: Uploadlote
		  }).
		  state('/register/mail/:username/:token', {
			url: '/register/mail/:username/:token',
			title: 'Kulbid | Confirmar E-mail',
			templateUrl: 'views/validation',
			controller: ValidationController
		  }).
		  state('/shop/user', {
			url: '/shop/user/:username',
			templateUrl: 'views/myshop',
			title: 'Kulbid Shop',
			controller: MyShpoController
		  }).
		  state('/shop', {
			url: '/shop/:urlname',
			templateUrl: 'views/single',
			title: 'Kulbid',
			controller: SingleController
		  }).
		  state('/stores', {
			url: '/stores',
			templateUrl: 'views/searchstores',
			title: 'Kulbid',
			controller: SearchStoresController
		  }).
		  state('/nearme/:lat/:lng', {
			url: '/nearme/:lat/:lng',
			templateUrl: 'views/nearme',
			title: 'Kulbid',
			controller: SearchNearMeController
		  }).
		  state('/nearme/:lat/:lng/:num', {
			url: '/nearme/:lat/:lng/:num',
			templateUrl: 'views/nearme',
			title: 'Kulbid',
			controller: SearchNearMeController
		  }).
		  state('/search/:term/:num', {
			url: '/search/:term/:num',
			templateUrl: 'views/search',
			title: 'Kulbid',
			controller: SearchPageController
		  }).
		  state('/search/:term', {
			url: '/search/:term',
			templateUrl: 'views/search',
			title: 'Kulbid',
			controller: SearchPageController
		  }).
		  state('/logout', {
			url: '/logout',
			templateUrl: 'views/index2',
			title: 'Kulbid',
			controller: LogOutController
		  }).
		  state('/forgotpassword', {
			url: '/forgotpassword',
			templateUrl: 'views/forgotpassword',
			title: 'Kulbid',
			controller: ForgotPasswordController
			}).
			state('/forgotmail', {
				url: '/forgotmail',
				templateUrl: 'views/forgotmail',
				title: 'Kulbid | Recuperar contraseña con e-mail',
				controller: ForgotPasswordController
			}).
			state('/forgotuser', {
				url: '/forgotuser',
				templateUrl: 'views/forgotuser',
				title: 'Kulbid | Recuperar contraseña con usuario',
				controller: ForgotPasswordController
			}).
		  state('/forgotpassword/:token/:tokensecret', {
			url: '/forgotpassword/:token/:tokensecret',
			templateUrl: 'views/newpass',
			title: 'Kulbid',
			controller: RestarPasswordController
		  }).
		  state('myitems', {
			url: '/myitems',
			title: 'Kulbid | Mis artículos',
			templateUrl: 'views/myitems',
			controller: MyItems
		  }).
		  state('myitems.favorites', {
			url: '/myfavorites',
			title: 'Kulbid | Mis artículos favoritos',
			templateUrl: 'views/myfavorites',
			controller: MyFavorites
		  }).
		  state('myitems.cart', {
			url: '/mycart',
			title: 'Kulbid | Mi carrito de compras',
			templateUrl: 'views/mycart',
			controller: MyCart
		  }).
		  state('myitems.history', {
			url: '/myhistoyrylist',
			title: 'Kulbid | Mi historial',
			templateUrl: 'views/myhistorylist',
			controller: MyHistory
		  }).
		  state('myitems.sales', {
			url: '/mysaleslist',
			title: 'Kulbid | Mis Ventas',
			templateUrl: 'views/mysales',
			controller: MySales
		  }).
		  state('myitems.purchases', {
			url: '/mypurchaseslist',
			title: 'Kulbid | Mis Compras',
			templateUrl: 'views/mypurchases',
			controller: MyPurchases
		  }).
		  state('myitems.list', {
			url: '/myitemslist',
			title: 'Kulbid | Mis artículos',
			templateUrl: 'views/myitemslist',
			controller: MyItems
		  }).
		  state('myitems.lote', {
			url: '/myitemsupload',
			title: 'Kulbid | Subir Lote',
			templateUrl: 'views/uploadlote',
			controller: Uploadlote
		  }).
		  state('profile', {
			url: '/profile',
			title: 'Kulbid | Mi perfil',
			templateUrl: 'views/profile',
			controller: ProfileController
		  }).
		  state('profile.particular', {
			url: '/particular',
			title: 'Kulbid | Perfil',
			cache: false,
			templateUrl: 'views/profileinfo',
			controller: ProfileParticularController
		  }).
		  state('profile.addphone', {
			url: '/particularphone',
			title: 'Kulbid | Mis numeros de contacto',
			templateUrl: 'views/profileaddphone',
			controller: ProfileParticularPhoneController
		  }).
		  state('profile.addemail', {
			url: '/particularmail',
			title: 'Kulbid | Mis direcciones de E-mail',
			templateUrl: 'views/profileaddemail',
			controller: ProfileParticularEmailController
		  }).
		  state('profile.changepass', {
			url: '/particularchangepass',
			title: 'Kulbid | Cambiar contraseña',
			templateUrl: 'views/profilechangepass',
			controller: ProfileParticularPassChangeController
		  }).
		  state('profile.store', {
			url: '/profilestore',
			title: 'Kulbid | Mi información de tienda',
			templateUrl: 'views/profilestore',
			controller: ProfileStoreController
		  }).
		  state('profile.paypal', {
			url: '/profilepay',
			title: 'Kulbid | Mi información de Paypal',
			templateUrl: 'views/profileapaypal',
			controller: ProfilePaypalController
		  }).
		  state('/terms', {
			url: '/terms',
			title: 'Kulbid | Terminos y Condiciones',
			templateUrl: 'views/terminos',
			controller: TermsController
		  }).
		  state('/privacy', {
			url: '/privacy',
			title: 'Kulbid | Politica de privacidad',
			templateUrl: 'views/privacy',
			controller: TermsController
		  }).
		  state('/done/:id', {
			url: '/done/:id',
			title: 'Kulbid ',
			templateUrl: 'views/done',
			controller: DoneController
		  }).
		  state('/transaction/:id/payment/:idp', {
			url: '/transaction/:id/payment/:idp',
			title: 'Kulbid ',
			templateUrl: 'views/done',
			controller: DoneController
		  });
        
});

kulbid.factory('Page', function() {
   var title = 'Kulbid';
   return {
     title: function() { return title; },
     setTitle: function(newTitle) { title = newTitle; }
   };
});
kulbid.value('duScrollDuration', 2000).value('duScrollOffset', 125);
kulbid.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({color: '#ff9800',position:'fixed',top:'50%'});
}]);
kulbid.controller('testController', LoginController);
kulbid.controller('SearchController', SearchController);
kulbid.controller('MyCart', MyCart);
kulbid.controller('MyNotis', MyNotis);
kulbid.controller('MainControllerFooter', MainControllerFooter);