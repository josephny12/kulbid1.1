'use strict';

/* Directives */


kulbid.directive( 'elemReady', function( $parse ) {
   return {
       restrict: 'A',
       link: function( $scope, elem, attrs ) {    
          elem.ready(function(){
            $('.ng-cloak').css('display','inherit !important');
          })
       }
    }
})

kulbid.directive('appVersion', function() {
    return function(scope, elm, attrs) {
        
    };
  });
  
kulbid.directive('kbSlider', function () {
      return {
         restrict: 'A',
         scope:{
           kbSlider: "=" 
         },
        controller: ["$scope", "$attrs", "$element", "$compile",
            function($scope, $attrs, $element, $compile) {
                $scope.init = function() {
                    $scope.$watch('kbSlider', function(newValue, oldValue) {
                        $($element).slippry({pager:false});
        
                    });
                };
                $scope.init();
            }]
         /*link: function (scope, el, attrs) {
            console.log($scope.kbSlider);
            $(el).slippry({pager:false});
         }*/
      };
});

kulbid.directive('favorite', ['$compile','$http','$cookies','$cookieStore',
  function($compile,$http,$cookies,$cookieStore) {
    return {
      restrict: 'E',
      scope: {
		index: "=",
		cb: "=",
        item: "=item"
      },
      controller: ["$scope", "$attrs", "$element", "$compile",
        function($scope, $attrs, $element, $compile) {


          $scope.init = function() {
            $scope.$watch('item', function(newValue, oldValue) {
				$scope.shopClick = function(){
					if($scope.item.isFavorite!==undefined){
						if($scope.item.isFavorite){
							var data = {
								token: $cookies.get('token'),
								tokensecret: $cookies.get('tokenSecret')
							};
							$http.delete('/api/favorites/item/'+$scope.item._id,{params:data}).
							success(function(data) {
								if(data.e===0){
									$scope.item.isFavorite=false;
									toastr.success('Articulo eliminado de favoritos','', {progressBar:true});
									if($scope.cb)
										$scope.cb(false,$scope.index);
								}else
									toastr.error(data.mes,'', {progressBar:true});
									
							});
						}else{
							if($scope.item.active==1){
								var dat = {
									token: $cookies.get('token'),
									tokensecret: $cookies.get('tokenSecret')
								};
								$http.post('/api/favorites/item/'+$scope.item._id,dat).
								success(function(data) {
									if(data.e===0){
										$scope.item.isFavorite=true;
										toastr.success('Articulo agregado a favoritos','', {progressBar:true});
										if($scope.cb)
											$scope.cb(true,$scope.index);
									}else
										toastr.error(data.mes,'', {progressBar:true});
								});
							}else
								toastr.error("No puedes agregar un articulo expirado a favoritos",'', {progressBar:true});
						}
					}else{
						toastr.warning('Debes iniciar sesión para agregar articulos a favoritos','', {progressBar:true});
					}
				};

				var item = '<button class="btn btn-danger" ng-class="item.active!=1 ? \'disabled\' : \'\'" type="button" ng-hide="item.isFavorite" ng-click="shopClick()"><i class="glyphicon glyphicon-heart"></i></button>';
				item += '<button class="btn btn-danger boton-favorito-a" type="button" ng-show="item.isFavorite" ng-click="shopClick()"><i class="glyphicon glyphicon-heart-empty"></i></button>';
				var e = $compile(item)($scope);
				$element.html(e);

            }, true);

          };
		  
          $scope.init();

        }
      ]

    };
  }
]);

  
kulbid.directive('shoppingCart', ['$compile','$http','$cookies','$rootScope','$cookieStore',
  function($compile,$http,$cookies,$rootScope,$cookieStore) {
    return {
      restrict: 'E',
      scope: {
		index: "=",
		cb: "=",
        item: "=item"
      },
	  
      controller: ["$scope", "$attrs", "$element", "$compile","$rootScope",
        function($scope, $attrs, $element, $compile,$rootScope) {

          $scope.init = function() {
		  
            $scope.$watch('item', function(newValue, oldValue) {

				$scope.shopClick = function(){
					if($scope.item.type!='A'){
						if($scope.item.isInCart!==undefined){
							if($scope.item.isInCart){
								var data = {
									token: $cookies.get('token'),
									tokensecret: $cookies.get('tokenSecret')
								};
								$http.delete('/api/cart/'+$scope.item._id,{params:data}).
								success(function(data, status, headers, config) {
									if(data.e===0){
										$scope.item.isInCart=false;
										toastr.success('Articulo eliminado del carrito de compras','', {progressBar:true});
										if($scope.cb)
											$scope.cb(false,$scope.index);
									}else
										toastr.error(data.mes,'', {progressBar:true});
										
								});
							}else{
								if($scope.item.active==1){
									var dat = {
										token: $cookies.get('token'),
										tokensecret: $cookies.get('tokenSecret')
									};
									$http.post('/api/cart/'+$scope.item._id,dat).
									success(function(data, status, headers, config) {
										if(data.e===0){
											
											$http.get('/api/cart?token='+$cookies.get('token')+'&tokensecret='+$cookies.get('tokenSecret')).
											success(function(data, status, headers, config) { 
												$rootScope.listcar = data.items;
												console.log($rootScope.listcar);
											});
											
											$scope.item.isInCart=true;
											toastr.success('Articulo agregado al carrito de compras','', {progressBar:true});
											if($scope.cb)
												$scope.cb(true,$scope.index);
										}else
											toastr.error(data.mes,'', {progressBar:true});
									});
								}else
									toastr.error("No puedes agregar un articulo expirado al carrito de compras",'', {progressBar:true});
							}
						}else{
							toastr.warning('Debes iniciar sesión para agregar articulos al carrito de compras','', {progressBar:true});
						}
					}else
						toastr.warning('No puedes agregar subastas al carrito de compras','', {progressBar:true});
						
				
				
				};

				var item = '<button class="btn btn-info" ng-class="item.active!=1 ? \'disabled\' : \'\'" ng-hide="item.isInCart" type="button" ng-click="shopClick()"><i class="glyphicon glyphicon-shopping-cart"></i></button>';
				item += '<button class="btn btn-info btn-carrito-a" ng-show="item.isInCart" type="button" ng-click="shopClick()"><i class="fa fa-cart-arrow-down"></i></button>';
				var e = $compile(item)($scope);
				$element.html(e);

            }, true);

          }; 
		  
          $scope.init();

        }
      ]

    };
  }
]);    

kulbid.directive('squareProfile', ['$compile',
  function($compile) {
    return {
      restrict: 'E',
      scope: {
        item: "=item"
      },
	  
      controller: ["$scope", "$attrs", "$element", "$compile",
        function($scope, $attrs, $element, $compile) {

          $scope.init = function() {
		  
            $scope.$watch('item', function(newValue, oldValue) {
				var item = [],i=0;
				$scope.statuses = {U:'Usado',N:'Nuevo',S:'Servicio',SN:'Semi nuevo'};
				$scope.types = {S:'Venta',A:'Subasta',L:'Lote'};
				item[i++] = '<div class="col-xs-12 col-sm-6 col-md-3 shop-item"><a class="portfolio-link"><div class="caption"><div class="caption-content"><i class="fa fa-search-plus fa-3x"></i></div></div>';
				item[i++] = '<img class="img-responsive img-fixed img-cua" src="uploads/200/{{item.photo}}" alt=""></a>';
				item[i++] = '<h5 class="text-center">{{item.user}}</h5><ul class="lista-caracteristicas">';
				item[i++] = '</ul><a class="vermas" href="/shop/user/{{item.user}}">Ver tienda</a></div>';
				var e = $compile(item.join(''))($scope);
				$element.html(e);

            }, true);

          }; 
          $scope.init();

        }
      ]

    };
  }
]);

kulbid.directive('catItem', ['$compile',
  function($compile) {
    return {
      restrict: 'E',
      scope: {
        category: "=category",
        ref: "=ref"
      },
	  
      controller: ["$scope", "$attrs", "$element", "$compile",
        function($scope, $attrs, $element, $compile) {

          $scope.init = function() {
		  
            $scope.$watch('item', function(newValue, oldValue) {
				var item,i=0;
				item =  '<a ui-sref="{{ref}}" class=""><div class="catitem">';
				item += '<img src="uploads/200/{{category.image}}" class="img-responsive img-fixed img-cua-cat img-cir"/>';
				item += '<h5>{{category.name}}</h5></div></a>';
				var e = $compile(item)($scope);

				$element.html(e);

            }, true);

          }; 
          $scope.init();

        }
      ]

    };
  }
]);  

  
kulbid.directive('squareItem', ['$compile',
  function($compile) {
    return {
      restrict: 'E',
      scope: {
		favCallback: "=",
		favIndex: "=",
		cartCallback: "=",
		cartIndex: "=",
        item: "=item",
		mdSize: "="
      },
	  
      controller: ["$scope", "$attrs", "$element", "$compile",
        function($scope, $attrs, $element, $compile) {

          $scope.init = function() {
		  
            $scope.$watch('item', function(newValue, oldValue) {
				var item = [],i=0;
				$scope.statuses = {U:'Usado',N:'Nuevo',S:'Servicio',SN:'Semi nuevo'};
				$scope.types = {S:'Venta',A:'Subasta',L:'Lote'};
				item[i++] = '<div class="col-xs-12 col-sm-6 '+ ($scope.mdSize ? 'col-md-4' : 'col-md-3' )+' shop-item"><a href="/shop/{{item.url}}" class="portfolio-link"><div class="caption"><div class="caption-content"><i class="fa fa-search-plus fa-3x"></i></div></div>';
				item[i++] = '<img class="img-responsive img-fixed img-cua" src="uploads/200/{{item.images[0]}}" alt=""></a>';
				item[i++] = '<h5 class="text-center">{{item.name}}</h5><ul class="lista-caracteristicas">';
				item[i++] = '<a href="/shop/user/{{item.user.user}}"><li class="cuadro_usuario"><div class="col-md-3 sinpadding"><img style="height: 50px; width: 50px;" class="img-responsive img-circle w100" src="uploads/200/{{item.user.photo}}" alt=""></div><div style="col-md-9"><strong>{{item.user.user}}</strong></div></li></a>';
				item[i++] = '<li class="cuadro-gris ng-binding">Precio: {{item.price}}$</li>';
				item[i++] = '<li class="cuadro-gris ng-binding"> <div class="cuadrito_emergencia"><i class="glyphicon glyphicon-time icono-separe"></i><timer end-time="item.time">{{days}} dias, {{hours}} horas, {{minutes}} minutos restantes.</timer></div></li>';
				item[i++] = '<li class="cuadro-gris ng-binding">Cantidad: {{item.stock}}</li>';
				item[i++] = '<li class="cuadro-gris ng-binding">{{types[item.type]}}</li>';
				item[i++] = '<li class="cuadro-gris ng-binding">{{statuses[item.status]}}</li>';
                if($scope.item.canEdit)
                    item[i++] = '<li class="cuadro-gris ng-binding"><a class="btn btn-info" href="/item/edit/{{item._id}}"><i class="glyphicon glyphicon-pencil"></i></a></li>';
                else
                    item[i++] = '<li class="cuadro-gris ng-binding"><shopping-cart  '+($scope.cartCallback? 'cb="cartCallback"' :'')+' '+($scope.cartIndex != undefined? 'index="cartIndex"' :'')+'  item="item"/><favorite '+($scope.favCallback? 'cb="favCallback"' :'')+' '+($scope.favIndex != undefined? 'index="favIndex"' :'')+'  item="item"/></li>';
				item[i++] = '</ul><a class="vermas" href="/shop/{{item.url}}">Ver más</a></div>';
				var e = $compile(item.join(''))($scope);

				$element.html(e);

            }, true);

          }; 
          $scope.init();

        }
      ]

    };
  }
]); 

kulbid.directive('datePicker', function () {
        var today = Date.now();
        var max = today - 567648000000;
    
        var options = {monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        selectYears: 100,
        selectMonths: true,
        max: new Date(max),
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        format:'dd-mm-yyyy',
        container: 'body'};
    
        return {
         restrict: 'A',
         link: function (scope, el, attrs) {
              $(el).pickadate(options);
         }
      }
  });

kulbid.directive('zoom2', ['$compile',
  function($compile) {
    return {
      restrict: 'AC',
      scope: {
        tiny: "=",
        small: "=",
        big: "=",
        title: "="
      },
	  
      controller: ["$scope", "$attrs", "$element", "$compile",
        function($scope, $attrs, $element, $compile) {

          $scope.init = function() {

            console.log($scope);

			
            $scope.$watch('tiny + small + big + title', function(newValue, oldValue) {


              var str = ' <a href="' + $scope.big + '" class="cloud-zoom" rel="adjustX: 10, adjustY:-4">' +
                '<img style="width:100%;height:300px;object-fit: cover;" src="' + $scope.small + '"/></a>';
				console.log(str);
              var e = $compile(str)($scope);

              $element.html(e);

              $(".cloud-zoom, .cloud-zoom-gallery").CloudZoom();

            }, true);

          }; 
		  
          $scope.init();

        }
      ]

    };
  }
]);
kulbid.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});