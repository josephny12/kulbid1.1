function MyNotis($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$document,$window){
	$scope.notis = [];
	$scope.audio = new Audio("/assets/audio/solemn.ogg");
	$scope.audio.type= 'audio/ogg';
    $scope.audio.src= "/assets/audio/solemn.ogg";
	
	
	$scope.clicknotis = function(){
		$http.get('/api/notifications',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret'),
				start:0,
				max:5
			}}).then(function(response){
				if(response.data.e===0){
					$scope.notis = response.data.notifications;
				}
			},function(){});
	
	};
	
	if(typeof(EventSource) !== "undefined") {
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			var source = new EventSource('/events/'+$cookies.get('token')+'/'+$cookies.get('tokenSecret'));
			source.onmessage = function (event) {
				var data = JSON.parse(event.data);
				if(data.logout){
					$cookies.remove("token");
					$cookies.remove("tokenSecret");
					$window.location.href='/';
				}else{
					$scope.audio.play();
					toastr.success(data.mes,'', {progressBar:true});
					$scope.notis.unshift(data);
					if($scope.notis.length>=5)
						$scope.notis.slice(0,5);
				}
            };
		}
		
	}
}

function index2main($rootScope, $state, $scope, $http, $location, $state,$stateParams, $cookies, $cookieStore, $anchorScroll, $window, $document) {
	$scope.$parent.showSpinner=true;
	$scope.$parent.history1.active=false;
	$scope.$parent.history2.active=false;
	$scope.$parent.history3.active=false;
	//$scope.contacto={};
	$scope.subtitle = {};
	$scope.subtitle = "Hola";
	
	$rootScope.title = $state.current.title;
	
	$http.get('/api/category/parents')
	.success(function(data, status, headers, config) {
		$scope.$parent.showSpinner=false;
		$scope.$parent.history = [];
		$scope.categories = data.categories;
	});

	/*

	$http.get('/api/footer/contacto')
	.success(function(data) {
		if(data.e===0)
		{
			$scope.contacto = data.result;
		}
	});

	*/
}

function index2cats($rootScope, $state, $scope, $http, $location, $state,$stateParams, $cookies, $cookieStore, $anchorScroll, $window, $document) {
	$scope.$parent.showSpinner=true;
	
	
	$scope.$parent.history1.active=true;
	$scope.$parent.history2.active=true;
	$scope.$parent.history2.name=$state.params.main;
	$scope.$parent.history3.active=false;
	
	$rootScope.title = "Kulbid | "+$state.params.main;
	$http.get('/api/category/name/'+$state.params.main)
	.then(function(response){
		if(response.data.e==0){
			$http.get('/api/category/childs/id/'+response.data.category._id)
			.success(function(data, status, headers, config) {
				$scope.$parent.showSpinner=false;
				$scope.categories = data.categories;
			});
		}else{
			toastr.error(response.data.mes,'', {progressBar:true});
		}
	},function(error){});
}

function index2subs($rootScope, $state, $scope, $http, $location, $state,$stateParams, $cookies, $cookieStore, $anchorScroll, $window, $document) {
	$scope.$parent.showSpinner=true;
	$scope.$parent.history1.active=true;
	$scope.$parent.history3.active=true;
	$scope.$parent.history3.name=$state.params.sub;
	$scope.listempty=false;
	
	
	$rootScope.title = "Kulbid | "+$state.params.sub;
	$http.get('/api/category/name/'+$state.params.sub)
	.then(function(response_c){
		if(response_c.data.e==0){
			var dat = {
				category: response_c.data.category._id,
				max: 12,
			};
			if($cookies.get('token') && $cookies.get('tokenSecret')){
				dat.token = $cookies.get('token');
				dat.tokensecret = $cookies.get('tokenSecret');
			}
			$http.get('/api/search/item',{params:dat}).
			success(function(data, status, headers, config) {
				$scope.$parent.showSpinner=false;
				if(data.items.length>0){
					$scope.items = data.items;
				}else{
					$scope.listempty=true;
				}
			});	 
		}else{
			toastr.error(response.data.mes,'', {progressBar:true});
		}
	},function(error){});
}

function index2($rootScope, $state, $scope, $http, $location, $state,$stateParams, $cookies, $cookieStore, $anchorScroll, $window, $document) {
	$scope.showSpinner=false;
	$scope.banners = [];
	$scope.history1 = {active:false};
	$scope.history2 = {active:false,name:""};
	$scope.history3 = {active:false,name:""};
	$rootScope.title = $state.current.title;
	
	$scope.trigger = false;
	$scope.showSpinner=true;
	$http.get('/api/site/banners').
		success(function(data, status, headers, config) {
			$scope.banners = data.banners;
			$scope.trigger = true;
			$scope.showSpinner=false;
		});
}

//VALIDATION
function ValidationController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$document) {
	$rootScope.title = $state.current.title;
	
	$http.put('/api/users/mail/'+$stateParams.username+'/'+$stateParams.token).
		success(function(data, status, headers, config) {
			if(data.e==0){
				toastr.success('Correo confirmado correctamente','', {progressBar:true});
				$location.path('/');
			}else{
				$scope.message_error=data.mes;
			}
		});	
}
//LOGIN CONTROLLER
function LoginController($scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.send=function(){
		$scope.showSpinner=true;
		var formdata=new FormData();
		formdata.append("username",$scope.form.username);
		formdata.append("password",$scope.form.password);
		var data={
			username:$scope.form.username,
			password:$scope.form.password
			
		}
		var request = {
                    method: 'POST',
                    url: '/api/users/login',
                    data: data
                };

        $http(request)
            .success(function (d) {
				$scope.showSpinner=false;
				if(d.e==0){
                    $cookies.put('token',d.token);
					$cookies.put('tokenSecret',d.tokenSecret);
					window.location.reload();
				}else{
					toastr.error(d.mes,'', {progressBar:true});
				}
            })
           .error(function () {
        });
	};
}
//Pre-Register
function RegisterController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$document) {
		$rootScope.title = $state.current.title;
		topy_ = angular.element(document.getElementById('topy_'));
		$document.scrollToElementAnimated(topy_,[130]);
}
//REGISTER PERSONAL
function RegisterControllerPersonal($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,Upload) {
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			$location.path('/');
		}
		
	$scope.keypress = function(evnt){
		if(evnt.charCode==32)
			evnt.preventDefault();
	};		
	
	
	$scope.form = {}, $scope.shSend=true,  $scope.shSending = false;
	$scope.avisos=['Selecciona tu Pais'];

	$rootScope.title = $state.current.title;
	$scope.file = false;
	$scope.countries={};
		$http.get('/api/data/countries').
		success(function(data, status, headers, config) {
			$scope.countries = data.countries;
			$scope.form.country = data.countries[0]._id;
		});

	
	
	$scope.changePhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
			$scope.file = $file;
		};
	
	$scope.send=function(){
		$scope.showSpinner=true;
		
		var dat = {
			profilepic:$scope.file,
			username: $scope.form.username,
			name: $scope.form.name,
			lastname: $scope.form.lastname,
			password: $scope.form.password,
			type: 1,
			deliveryaddress: $scope.form.address,
			state: $scope.form.state,
			country: $scope.form.country,
			postal: $scope.form.postal,
			phone: $scope.form.phone,
			mail: $scope.form.mail,
			dob: $scope.form.dob
		};
		
		if($scope.form.city)
			dat.city = $scope.form.city;
		
		Upload.upload({
				url: '/api/users',
				arrayKey:'',
				method:'POST',
				data: dat
			}).then(function (response) {
				$scope.showSpinner=false;
				if(response.data.e===0){
					toastr.success("Registro exitoso, por favor confirma tu E-mail",'', {progressBar:true});
					$location.path('/notify');				
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
				}
			}, function (response) {
					
			}, function (evt) {
		});
	};
			$scope.getstate = function(id) {
			   	$scope.states={};
				$http
				.get('/api/data/states/'+id, {
					params: {
						//source: link
					}
				 })
				 .success(function (data,status) {
						//console.log(data)
						$scope.states = data.states;		
				 });
			   
			}
			//CITY
			$scope.getcity = function(id) {
				console.log(id);
			   	$scope.cities={};
				$http
				.get('/api/data/cities/'+id, {
					params: {
						//source: link
					}
				 })
				 .success(function (data,status) {
						//console.log(data)
						$scope.cities = data.cities;		
				 });
			   
			}

		$scope.number_p =function(event) {
			console.log(event);
			if (event.which !=8 && (event.which < 47 || event.which > 59)){
				if ((event.which < 47 || event.which > 59))
					{
						event.preventDefault();
						if ( ($(this).indexOf('.') != -1)) {
							event.preventDefault();
						}else if((event.which == 8) && ($(this).indexOf('.') != -1)){
							event.preventDefault();
						}
					}				
			}

			
		}
}
//REGISTER STORE
function RegisterControllerStore($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,Upload) {
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			$location.path('/');
		}
		
		
	$scope.keypress = function(evnt){
		if(evnt.charCode==32)
			evnt.preventDefault();
	};		
	$scope.form = {}
	$rootScope.title = $state.current.title;
	$scope.file = false;

	$scope.countries={};
		$http.get('/api/data/countries').
		success(function(data, status, headers, config) {
			$scope.countries = data.countries;
			$scope.form.country = data.countries[0]._id;
		});

	$scope.changePhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
		$scope.file = $file;
	};
	
	$scope.send=function(){
		$scope.showSpinner=true;
	
		var dat = {
			profilepic:$scope.file,
			username: $scope.form.username,
			name: $scope.form.name,
			lastname: $scope.form.lastname,
			password: $scope.form.password,
			storename: $scope.form.storename,
			storetype: $scope.form.storetype,
			storeaddress: $scope.form.storeaddress,
			type: 2,
			deliveryaddress: $scope.form.address,
			state: $scope.form.state,
			country: $scope.form.country,
			postal: $scope.form.postal,
			phone: $scope.form.phone,
			mail: $scope.form.mail,
			dob: $scope.form.dob
		};
		
		if($scope.form.city)
			dat.city = $scope.form.city;
		
		Upload.upload({
				url: '/api/users',
				arrayKey:'',
				method:'POST',
				data: dat
			}).then(function (response) {
				$scope.showSpinner=false;
				if(response.data.e===0){
					toastr.success("Registro exitoso, por favor confirma tu E-mail",'', {progressBar:true});
					$location.path('/notify');				
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
				}
			}, function (response) {
					
			}, function (evt) {
		});
		
		var request = {
                    method: 'POST',
                    url: '/api/users',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

        $http(request)
            .success(function (d) {
					$scope.showSpinner=false;
					console.log(d);
					if(d.e==0){
						//$cookies.put('token',d.token);
						//$cookies.put('tokenSecret',d.tokenSecret);
						toastr.success("Registro exitoso, por favor confirma tu E-mail",'', {progressBar:true});
						setTimeout(function(){
							$location.path('/');
						}, 5000);
					}else{
						//alert(d.mes);
						toastr.error(d.mes,'', {progressBar:true});
						
					}
            })
           .error(function () {
        });
	};
			$scope.getstate = function(id) {
			   	$scope.states={};
				$http
				.get('/api/data/states/'+id, {
					params: {
						//source: link
					}
				 })
				 .success(function (data,status) {
						//console.log(data)
						$scope.states = data.states;		
				 });
			   
			}
			//CITY
			$scope.getcity = function(id) {
				console.log(id);
			   	$scope.cities={};
				$http
				.get('/api/data/cities/'+id, {
					params: {
						//source: link
					}
				 })
				 .success(function (data,status) {
						//console.log(data)
						$scope.cities = data.cities;		
				 });
			   
			}

		$scope.number_p =function(event) {
			console.log(event);
			if (event.which !=8 && (event.which < 47 || event.which > 59)){
				if ((event.which < 47 || event.which > 59))
					{
						event.preventDefault();
						if ( ($(this).indexOf('.') != -1)) {
							event.preventDefault();
						}else if((event.which == 8) && ($(this).indexOf('.') != -1)){
							event.preventDefault();
						}
					}				
			}

			
		}
}

function ItemsEdit($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,NgMap,Upload) {
		$rootScope.title = $state.current.title;
		
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		
		
		
		$scope.days = {
			S : [
				{value: 7, name: "7 días"},
				{value: 15, name: "15 días"},
				{value: 30, name: "30 días"}
			],
			L : [
				{value: 7, name: "7 días"},
				{value: 15, name: "15 días"},
				{value: 30, name: "30 días"}
			],
			A : [
				{value: 1, name: "1 día"},
				{value: 3, name: "3 días"},
				{value: 5, name: "5 días"},
				{value: 7, name: "7 días"}
			]
		
		};
			
			
		$scope.typeChanged = function(){
			if($scope.form.type=='A')
				$scope.form.stock = 1;
		}	
		
		$scope.validateStock = function(){
			if($scope.form.type=='A')
				$scope.form.stock = 1;
		}
		
		
		$http.get('/api/site/tax',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}}).then(function(response){
					$scope.taxP=response.data.tax;
			},function(){});
		$http.get('/api/users/canpublish',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}}).then(function(response){
				
					$scope.chekbox=response.data;
				
			},function(){});
		
		
		$scope.categories=[];
		$scope.childs=[];
		$scope.form={};
		$scope.itemdata=[];
		$scope.files0=[];
		$scope.files=[];
		$scope.form.url={};

		$http.get('/api/category/parents').
			success(function(data, status, headers, config) {
			  $scope.categories = data.categories;
		});
	
		$scope.getchilds = function(id) {
					$scope.states={};
					$http
					.get('/api/category/childs/id/'+id, {
						params: {
							//source: link
						}
					 })
					 .success(function (data,status) {
							//console.log(data)
							$scope.childs = data.categories;		
					 });
				   
		};
		$scope.remove = function(index) {
			$scope.files.splice(index, 1);
		};		
	 
		var countmarket=false;
		$http.get('/api/items/details/'+$stateParams.id,{
		params: {
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}	
		}).
		success(function(data, status, headers, config) {
			var i=0;
			$scope.itemdata = data.item;
			$scope.form.name = data.item.name;
			$scope.form.descriptionShort = data.item.descriptionShort;
			$scope.form.descriptionLong = data.item.descriptionLong;
			$scope.form.price = data.item.price;
			$scope.form.stock = data.item.stock;
			$scope.form.parent = data.item.parentCategory;
			$scope.form.type = data.item.type;
			$scope.form.active = ""+data.item.active;
			$scope.form.status = data.item.status;
			$scope.form.url = data.item.url;
			$scope.position = Array.isArray(data.item.position) ? data.item.position.coordinates : false;
			
			if($scope.myMap!==undefined && Array.isArray($scope.position)){
				countmarket = new google.maps.Marker({
					position: new google.maps.LatLng($scope.position[1], $scope.position[0]), 
					map: $scope.myMap,
					draggable:true
				});
				$scope.myMap.setCenter(new google.maps.LatLng($scope.position[1], $scope.position[0]));
			};
			
			$http.get('/api/category/childs/id/'+data.item.parentCategory)
			 .success(function (data,status) {
					$scope.childs = data.categories;
					$scope.form.category = $scope.itemdata.category;		
			 });			 
			  for(;i<data.item.images.length;i++){
				  $scope.files0.push('uploads/250/'+data.item.images[i]);
			  }
			  //$scope.files = data.item.images;
	});
		
		$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh_QtBdAh9G2ZAczx6ZdAhJqISBI8QGUU";
		NgMap.getMap().then(function(map) {
			$scope.myMap = map;
			
			if($scope.position){
				countmarket = new google.maps.Marker({
					position: new google.maps.LatLng($scope.position[1], $scope.position[0]), 
					map: $scope.myMap,
					draggable:true
				});	
				$scope.myMap.setCenter(new google.maps.LatLng($scope.position[1], $scope.position[0]));
			}
			
			$scope.myMap.addListener('click', function(e) {
				if(countmarket)
					countmarket.setPosition(e.latLng);
				else{
					countmarket = new google.maps.Marker({
						position: e.latLng, 
						map: $scope.myMap,
						draggable:true
					});			
				}
			});		
		});
		//Enviar información básica
	
	
		$scope.uploadPhotos = function(){
			
			if($scope.files.length>0){
				var data = {
					itempics: $scope.files,
					token:$cookies.get('token'),
					tokensecret:$cookies.get('tokenSecret')
				};
			
			
				Upload.upload({
					url: '/api/items/changepictures/'+$stateParams.id,
					arrayKey:'',
					method:'PUT',
					data: data
				}).then(function (response) {
					$scope.showSpinner=false;
					console.log(response);
					if(response.data.e==0){
						toastr.success('Imagenes actualizadas correctamente','', {progressBar:true});
							$location.path('/shop/'+decodeURIComponent(response.data.url));						
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
							
						
					}, function (response) {
						console.log(response);
					}, function (evt) {
						/*$scope.progress = 
							Math.min(100, parseInt(100.0 * evt.loaded / evt.total));*/
					});
			}else
				toastr.error("Debes seleccionar al menos una foto",'', {progressBar:true});
		};
	
	
		$scope.send=function(){
			$scope.showSpinner=true;
			var data = {
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				active: $scope.form.active,
				name : $scope.form.name,
				descriptionShort : $scope.form.descriptionShort,
				descriptionLong : $scope.form.descriptionLong,
				price : $scope.form.price,
				stock : $scope.form.stock,
				category : $scope.form.category,
				status : $scope.form.status,
				type : $scope.form.type,
				time: $scope.form.time
			};
				
			if(countmarket){
				data.lat = countmarket.position.lat();
				data.lng = countmarket.position.lng();
			}
			
			
			$http.put('/api/items/edit/'+$stateParams.id, data)
			   .then(function (response) {
					$scope.showSpinner=false;
					if(response.data.e===0){
						toastr.success('Articulo actualizado','', {progressBar:true});	
						console.log($scope.form.url);
						$location.path('/shop/'+decodeURIComponent($scope.form.url));	
					}else{
						toastr.error(response.data.mes,'', {progressBar:true});						
					}
				}, function (error) {
					$scope.showSpinner=false;
					toastr.error('Ups!','', {progressBar:true});
				});
		};
}

//INSERT ITEM
function ItemsRegister($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,NgMap,Upload) {
		$rootScope.title = $state.current.title;
		$scope.chekbox=[];
		
		$scope.days = {
			S : [
				{value: 7, name: "7 días"},
				{value: 15, name: "15 días"},
				{value: 30, name: "30 días"}
			],
			L : [
				{value: 7, name: "7 días"},
				{value: 15, name: "15 días"},
				{value: 30, name: "30 días"}
			],
			A : [
				{value: 1, name: "1 día"},
				{value: 3, name: "3 días"},
				{value: 5, name: "5 días"},
				{value: 7, name: "7 días"}
			]
		
		};
			
			
		$scope.typeChanged = function(){
			if($scope.form.type=='A')
				$scope.form.stock = 1;
		}	
		
		$scope.validateStock = function(){
			if($scope.form.type=='A')
				$scope.form.stock = 1;
		}
			
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
			$scope.categories=[];
			$scope.childs=[];
			$scope.form={};
			
	 $scope.max0 = function(inter){
		console.log($scope.form.price);
		inter = parseFloat(inter);
		console.log(inter);
		if(inter>0){
			$scope.form.price=inter;
			return true;
		}else{
			$scope.form.price='';
			return false;
		}		 
	 };
	 
	 $scope.max1 = function(inter){
		inter = parseInt(inter);
		if(inter>0){
			$scope.form.stock=inter;
			return true;
		}else{
			$scope.form.stock='';
			return false;
		}		 
	 };
	 
		$http.get('/api/site/tax',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}}).then(function(response){
					$scope.taxP=response.data.tax;
					console.log(response.data);
				
			},function(){});
		$http.get('/api/users/canpublish',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}}).then(function(response){
				
					$scope.chekbox=response.data;
				
			},function(){});
				
			$http.get('/api/category/parents').
			success(function(data, status, headers, config) {
				  $scope.categories = data.categories;
			});
	
			$scope.getchilds = function(id) {
						$scope.states={};
						$http
						.get('/api/category/childs/id/'+id, {
							params: {
								//source: link
							}
						 })
						 .success(function (data,status) {
								//console.log(data)
								$scope.childs = data.categories;		
						 });
					   
					}
			  $scope.remove = function(index) {
				$scope.files.splice(index, 1);
			  };
			  
			  
		var myMap,countmarket=false;
		$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh_QtBdAh9G2ZAczx6ZdAhJqISBI8QGUU";
		
		$scope.searchPosition = function(position){
			myMap.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
		}
		
		NgMap.getMap().then(function(map) {
			myMap = map;
			
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition($scope.searchPosition);
			}
			
			myMap.addListener('click', function(e) {
				if(countmarket)
					countmarket.setPosition(e.latLng);
				else{
					countmarket = new google.maps.Marker({
						position: e.latLng, 
						map: myMap,
						draggable:true
					});			
				}
			});		
		});
		
		$scope.send=function(){
			if($scope.files){
				var fl=$scope.files.length,i=0;
				
				if($scope.files.length>0){
				
					$scope.showSpinner=true;
					var data = {
						token : $cookies.get('token'),
						tokensecret : $cookies.get('tokenSecret'),
						name : $scope.form.name,
						descriptionShort : $scope.form.descriptionShort,
						descriptionLong : $scope.form.descriptionLong,
						price : $scope.form.price,
						stock : $scope.form.stock,
						category : $scope.form.category,
						status : $scope.form.status,
						type : $scope.form.type,
						time: $scope.form.time,
						itempics: $scope.files
					}
					
					if(countmarket){
						data.lat = countmarket.position.lat();
						data.lng = countmarket.position.lng();
					}
					
					Upload.upload({
							url: '/api/items',
							arrayKey:'',
							data: data
						}).then(function (response) {
							$scope.showSpinner=false;
							console.log(response);
							if(response.data.e==0){
								toastr.success('Item agregado correctamente','', {progressBar:true});
								$location.path('/shop/'+decodeURIComponent(response.data.url));						
							}else{
								toastr.error(response.data.mes,'', {progressBar:true});
							}
								
							
						}, function (response) {
							console.log(response);
						}, function (evt) {
						});

				}else{
					toastr.warning("Debes insertar al menos una imagen",'', {progressBar:true});
				}				
			}else{
				toastr.warning("Debes insertar al menos una imagen",'', {progressBar:true});
			}

		};
}
//UPLOAD LOTE
function Uploadlote($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;


		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}

	$scope.bulk = false;
	$scope.showSpinner = false;
	$scope.send = function(){
		var file = document.getElementById("bulk").files;
		var formdata=new FormData();
		formdata.append("bulk",file[0],file[0].name);
		formdata.append("token",$cookies.get('token'));
		formdata.append("tokensecret",$cookies.get('tokenSecret'));
		
		var request = {
                    method: 'POST',
                    url: '/api/items/uploadbulk',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

		$scope.showSpinner=true;
        // SEND THE FILES.
        $http(request)
			.then(function (d) { 
					$scope.showSpinner=false;
					//alert(d.mes);
					if(d.data.e==0){
						toastr.success("Lote subido correctamente, procesando",'', {progressBar:true});
					}else
						toastr.error(d.data.mes,'', {progressBar:true});
            },function (e){
				console.log(e);
			}, function (u){
				console.log(u);
			});
	};
}
//MI ITEMS
function MyShpoController($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.numberpages = [];
		$rootScope.title = $state.current.title;
		$scope.poitnscalidadcss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnscomunicacioncss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnsdisponiblecss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		
		$scope.poitnscalidadcss_c=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnscomunicacioncss_c=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnsdisponiblecss_c=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.profile={};
		$scope.comments=[];
		
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;
		$scope.getComments = function(){
			var dt = {
				max: 99
			};
				
			if($cookies.get('token') && $cookies.get('tokenSecret')){
				dt.token = $cookies.get('token');
				dt.tokensecret = $cookies.get('tokenSecret');
			}
				
			$http.get('/api/comments/store/'+$stateParams.username, {params:dt}).
				success(function(data, status, headers, config) {
					$scope.comments=data.comments;
					//console.log(data);
				});
		};
		$scope.getComments();
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max
			};
			
			if($cookies.get('token')){
				dat.token = $cookies.get('token');
				dat.tokensecret = $cookies.get('tokenSecret');
			}	
			$scope.showSpinner=true;
			$http.get('/api/items/user/shop/'+$stateParams.username,{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.items;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
		$http.get('/api/users/profile/'+$stateParams.username).
		success(function(data, status, headers, config) {
			$scope.showSpinner=false;
			$scope.profile = data.user;
			var ratecommunication=data.user.rate.seller.communication;
			var rateavailability=data.user.rate.seller.avialability;
			var ratequality=data.user.rate.seller.quality;
			if(data.user.rate.sellerSum>0){
				$scope.poitnscalidadcss[rateavailability]='puntos valor'+parseInt(ratequality+1)+' puntosObs';
				$scope.poitnscomunicacioncss[ratecommunication]='puntos valor'+parseInt(ratecommunication+1)+' puntosObs';
				$scope.poitnsdisponiblecss[rateavailability]='puntos valor'+parseInt(rateavailability+1)+' puntosObs';					
			}		
			var ratecommunication_c=data.user.rate.buyer.communication;
			var rateavailability_c=data.user.rate.buyer.avialability;
			var ratequality_c=data.user.rate.buyer.quality;
			if(data.user.rate.buyerSum>0){
				$scope.poitnscalidadcss_c[rateavailability_c]='puntos valor'+parseInt(ratequality_c+1)+' puntosObs';
				$scope.poitnscomunicacioncss_c[ratecommunication_c]='puntos valor'+parseInt(ratecommunication_c+1)+' puntosObs';
				$scope.poitnsdisponiblecss_c[rateavailability_c]='puntos valor'+parseInt(rateavailability_c+1)+' puntosObs';				
			}

		});

			$scope.sendQuestion = function () {
				
				$scope.showSpinner=true;
				var dat = { 
					replyto: $scope.replyTo,
					comment: $scope.form.comment,
					token: $cookies.get('token'),
					tokensecret: $cookies.get('tokenSecret') 
				};
				
				$http.post('/api/comments/store/'+$stateParams.username,dat).
					success(function(data, status, headers, config) {
						$('#closeModal').click();
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Comentario agregado','', {progressBar:true});
							$scope.getComments();}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
				//alert("Ops!");
			}
}


function MyItems($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;
		$scope.numberpages = [];

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}

		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/items',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.items;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
		
}
function MyFavorites($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/favorites/items',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.items;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
		$scope.swit = function(boo,ind){
			if(boo==false){
				delete $scope.items[ind];				
				var newItems =[], i=0;
				for(;i < $scope.items.length;i++)
					if($scope.items[i])
						newItems.push($scope.items[i]);
				$scope.items = newItems;
			}
		}
}
function MyHistory($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret')
			};
					
			$scope.showSpinner=true;
			$http.get('/api/history/items',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.items;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
}
function MySales($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/transactions/mysales',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.transactions;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
}
function MyPurchases($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {

		$rootScope.title = $state.current.title;
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		
		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/transactions/mypurchases',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$scope.items = data.transactions;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
}
function MyCart($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$rootScope.listcar = [];
		$rootScope.title = $state.current.title;
		$scope.numberpages = [];
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$location.path('/');
		}
		$scope.items=[];
		
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/cart',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				$rootScope.listcar = data.items;
				$scope.items = data.items;
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		
		$scope.buyAll = function(){
			$http.post('/api/cart/buyall',{token:$cookies.get('token'),tokensecret:$cookies.get('tokenSecret')})
			.then(function(response){
				if(response.data.e==0){
					toastr.success("Transacción realizada",'', {progressBar:true});
					$location.path('/myitems/mypurchaseslist');
				}else{
					toastr.error(response.data.mes,'', {progressBar:true});
				}	
			});
		}
		
		$scope.clickcart = function (){
			$http.get('/api/cart?token='+$cookies.get('token')+'&tokensecret='+$cookies.get('tokenSecret')+'&max='+5,{ignoreLoadingBar: true}).
			success(function(data, status, headers, config) {
				$scope.items = data.items;

			});
		}
		
		$scope.switcart = function (boo, ind){
			if(boo==false){
				delete $scope.items[ind];				
				var newItems =[], i=0;
				for(;i < $scope.items.length;i++)
					if($scope.items[i])
						newItems.push($scope.items[i]);
				$scope.items = newItems;
			}			
		}
		
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
}
//SEARCH
function SearchController($scope, $timeout, $http, $location, $stateParams, $cookies, $cookieStore, $window) {
		
		$scope.listsearchs = [],$scope.listempty=false,$scope.mostra_lista=false,$scope.moresearch=[],$scope.term="";
		var timeout;
		
		$scope.hideDelay = function(show){
			if(show && $scope.listsearchs.length>0){
				$scope.mostra_lista=true;
				if(timeout){
					$timeout.cancel(timeout);
					timeout=undefined;
				}
				clearTimeout(timeout);
			}else{
				timeout = $timeout(function(){
					$scope.mostra_lista=false;
				}, 200);
			}
		};
		
		$scope.out = function(){
			$http.get('/api/logout/'+$cookies.get('token')+'/'+$cookies.get('tokenSecret'))
			.then(function(){
				$cookies.remove("token");
				$cookies.remove("tokenSecret");
				$window.location.href='/';
			},function(){
				$cookies.remove("token");
				$cookies.remove("tokenSecret");
				$window.location.href='/';
			});
		};
		
		$scope.searchPosition = function(position){
			$scope.$apply(function(){
				$location.path('/nearme/'+position.coords.latitude+'/'+position.coords.longitude);
			});
		};
		
		$scope.nearMe = function(){
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition($scope.searchPosition);
			} else {
				toastr.error('Tu navegador no soporta esta función','', {progressBar:true});
			}
		};
		
		$scope.search=function(){
			$scope.showSpinner=true;
			if($scope.term.length>2){
				$http.get('/api/search/item?term='+$scope.term+'&max=5').
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.items.length>0){
							$scope.mostra_lista=true;
						}else{
							$scope.listempty=true;
						}
						$scope.listsearchs = data.items;
						
					});
			}else
				$scope.listsearchs = [];
		};
		$scope.searchsingle=function(){
			if($scope.term.length>2){
				$location.path('/search/'+$scope.term);
			}else{
				toastr.warning('Tu busqueda es muy corta','', {progressBar:true});
			}
		};
}
//SINGLE ITEM
function SingleController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$timeout) {
			$scope.showSpinner=true;
			$scope.item = [];
			$scope.particular = [];
		    $scope.thumb = [];
			$scope.form={};
		    $scope.ZoomItems = [];
			$scope.SelectedItem={};
			$scope.comments=[];
			$scope.commentsreplay=[];
			$scope.ID={};
			$scope.urlname={};
			$scope.statuses = {S:'Servicio',U:'Usado',N:'Nuevo',SN:'Semi nuevo'};
			$scope.types = {S:'Venta',A:'Subasta',L:'Lote'};
			$scope.modalTitle = "Haz una pregunta";
			$scope.replyTo = false;
			today=Date.now();
			$scope.statustime=true;
			$scope.quantity=1;
			var typeinter = '';
			//encodeURIComponent(uri)
			var dat = {};
			if($cookies.get('token') && $cookies.get('tokenSecret')){
				dat.token = $cookies.get('token');
				dat.tokensecret = $cookies.get('tokenSecret');
			}
			
			
			$scope.getComments = function(){
					var dt = {
						max: 99
					};
				
					if($cookies.get('token') && $cookies.get('tokenSecret')){
						dt.token = $cookies.get('token');
						dt.tokensecret = $cookies.get('tokenSecret');
					}
				
					$http.get('/api/comments/item/'+$scope.ID, {params:dt}).
						success(function(data, status, headers, config) {
						$scope.comments=data.comments;
						//console.log(data);
					});
			};
			$scope.to = false;
			$scope.callAtTimeout = function() {
				$http.get('/api/transactions/highestbid/'+$scope.ID,{ignoreLoadingBar: true}).
					success(function(data, status, headers, config) {
						$scope.price=data.bid;
					});
				$scope.to = $timeout( function(){ $scope.callAtTimeout(); }, 5000);
			}
			
			$scope.$on('$destroy', function(){
			  $timeout.cancel($scope.to);
			});
			
			$http.get('/api/items/shop/'+encodeURIComponent($stateParams.urlname),{params:dat}).
				success(function(data, status, headers, config) {
				
					$rootScope.title = $state.current.title+" | "+data.item.name;
					$scope.urlname=data.item.url;
					$scope.ID=data.item._id;
					$scope.showSpinner=false;
					$scope.item=data.item;
					$scope.poitnscalidadcss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
					$scope.poitnscomunicacioncss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
					$scope.poitnsdisponiblecss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
					$scope.particular=data.item.user;
					$scope.descriptionLong=data.item['descriptionLong'];
					$scope.type=data.item['type'];
					typeinter=data.item['type'];
					if(data.item.time>today){
						$scope.statustime=true;
					}
					$scope.thumb=data.item.images;
					$scope.price=data.item.price;
					var ratecommunication=data.item.user.rate.seller.communication;
					var rateavailability=data.item.user.rate.seller.avialability;
					var ratequality=data.item.user.rate.seller.quality;
					
					if(data.item.user.rate.sellerSum>0){
						$scope.poitnscalidadcss[rateavailability]='puntos valor'+parseInt(ratequality+1)+' puntosObs';
						$scope.poitnscomunicacioncss[ratecommunication]='puntos valor'+parseInt(ratecommunication+1)+' puntosObs';
						$scope.poitnsdisponiblecss[rateavailability]='puntos valor'+parseInt(rateavailability+1)+' puntosObs';
					}
					
					if(typeinter=='A'){
						$scope.to = $timeout( function(){ $scope.callAtTimeout(); }, 5000);				
					}
					for(var i = 0 ; i < $scope.thumb.length ; i++)
						$scope.ZoomItems.push({
							id: i,
							big: "uploads/1000/"+$scope.thumb[i],
							tiny: "uploads/500/"+$scope.thumb[i],
							small: "uploads/500/"+$scope.thumb[i],
							title: "display title",
						});
					$scope.SelectedItem = $scope.ZoomItems[0];
					$scope.getComments();
				});
				
				
			$scope.resetModal = function(){
				$scope.replyTo = false;
				$scope.modalTitle = "Haz una pregunta";
			};
			
			$scope.reply = function(parent){
				$scope.replyTo = parent;
				$scope.modalTitle = "Responder";
			};
			
			$scope.ThumbnailClicked = function(index){
				$scope.SelectedItem = $scope.ZoomItems[index];
			};		
			
			$scope.sendQuestion = function () {
				
				$scope.showSpinner=true;
				var dat = { 
					replyto: $scope.replyTo,
					comment: $scope.form.comment,
					token: $cookies.get('token'),
					tokensecret: $cookies.get('tokenSecret') 
				};
				
				$http.post('/api/comments/item/'+$scope.ID,dat).
					success(function(data, status, headers, config) {
						$('#closeModal').click();
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Comentario agregado','', {progressBar:true});
							$scope.getComments();}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
				//alert("Ops!");
			}
			//ENVIAR PUJA
			$scope.sendbid = function() {
				if($cookies.get('token') && $cookies.get('tokenSecret'))
					$http.post('/api/transactions/bid/'+$scope.ID,{type:$scope.form.type,bid:$scope.form.bid,tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')  }).
					success(function(data, status, headers, config) {
						$('#mybid span').click();
						if(data.e==0){
							toastr.success('Puja Exitosa','', {progressBar:true});
						}else{
							toastr.warning(data.mes,'', {progressBar:true});
						}
						
					});
				else
					toastr.warning("Debes iniciar sesión para pujar este producto",'', {progressBar:true});
					
				
				
			}
			//COMPRA
			$scope.buy = function() {	
				if($cookies.get('tokenSecret') && $cookies.get('token'))
					$http.post('/api/transactions/buy/'+$scope.ID,{quantity:$scope.quantity,tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}).
						success(function(data, status, headers, config) {
							if(data.e==0){
								$('#mybuy span').click();
								toastr.success('Compra Exitosa','', {progressBar:true});
								$timeout(function() {
									$location.path('/done/'+data.id);
								}, 2000);
							}else{
								toastr.warning(data.mes,'', {progressBar:true});
							}
							
						});
				else
					toastr.warning("Debes iniciar sesión para comprar ese producto",'', {progressBar:true});
			}

}



function SearchStoresController($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.title_search=$stateParams.term,$scope.listempty=false,$stateParams.numberpage,$scope.main_cat;
		$scope.numberpages = [];
		$scope.items = [];
		$scope.categories = [];
		$scope.category=false;
		var dat = {
				category: null,
				token: $cookies.get('token'),
				tokensecret: $cookies.get('tokenSecret')
			};
		
		
		$rootScope.title = $state.current.title+" | "+$stateParams.term;

		$scope.showSpinner=true;
					
		$http.get('/api/category/parents').
			success(function(data, status, headers, config) {
				  $scope.categories = data.categories;
		});
		
		
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;
		if($stateParams.numberpage){
			$scope.pageChange($stateParams.numberpage);
		}
		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
			if($scope.category)	
				dat.category=$scope.category;
			$scope.showSpinner=true;
			$http.get('/api/search/stores',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				if(data.users.length>0){
					$scope.items = data.users;
				}else{
					$scope.items = [];
					$scope.listempty=true;
				}
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};

		$scope.call_child = function (id_parent,name_cat){
				$scope.showSpinner=true;
				$http.get('/api/category/childs/id/'+id_parent).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						$scope.categories = [];
						$scope.main_cat = name_cat;
						$scope.categories = data.categories;
					});	
		}	
		
		$scope.clear = function(){
			$scope.showSpinner=true;
			$scope.main_cat = null;
			$scope.categories=[];
			$scope.category = false;
			$http.get('/api/category/parents').
				success(function(data, status, headers, config) {
					  $scope.categories = data.categories;
			});
			dat.category = null;
			$scope.list();
		}
		$scope.filtercat = function(id_cat){
			$scope.showSpinner=true;
			$scope.items = [];
			$scope.category = id_cat;
			$scope.list();
		}
		
}


function SearchNearMeController($q, NgMap, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.title_search=$stateParams.term,$scope.listempty=false,$stateParams.numberpage,$scope.main_cat;
		$scope.numberpages = [];
		$scope.items = [];
		
		$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh_QtBdAh9G2ZAczx6ZdAhJqISBI8QGUU";
		NgMap.getMap().then(function(map) {
			$scope.myMap = map;
			$scope.myMap.setCenter(new google.maps.LatLng(parseFloat($stateParams.lat), parseFloat($stateParams.lng)));
			new google.maps.Marker({
				position: {lat:parseFloat($stateParams.lat),lng:parseFloat($stateParams.lng)}, 
				map: $scope.myMap,
			});
		});
		
		$rootScope.title = $state.current.title+" | Cerca de ti";
		$scope.showSpinner=true;
		
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
			var dat = {
				lat: $stateParams.lat,
				lng: $stateParams.lng,
				start : $scope.start,
				max : $scope.max,
				token : $cookies.get('token'),
				tokensecret : $cookies.get('tokenSecret'),
				term: $stateParams.term
			};
					
			$scope.showSpinner=true;
			$http.get('/api/search/item',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				if(data.items.length>0){
					$scope.items = data.items;
				}else{
					$scope.items=[];
					$scope.listempty=true;
				}
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
		if($stateParams.numberpage){
			$scope.pageChange($stateParams.numberpage);
		}
}



function SearchPageController($q, $rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.title_search=$stateParams.term,$scope.listempty=false,$stateParams.numberpage,$scope.main_cat;
		$scope.numberpages = [];
		$scope.categories = [];
		$scope.order = "d|-1";
		$scope.typesearch = [
			{name:'Venta',type:'S'},
			{name:'Lote',type:'L'},
			{name:'Subasta',type:'A'}
		];
		$scope.typestatus = [
			{name:'Nuevo',type:'N'},
			{name:'Usado',type:'U'},
			{name:'Servicio',type:'S'},
			{name:'Seminuevo',type:'SN'},
		];
		$scope.orders = [
			{name:'Más reciente',type:'d|-1'},
			{name:'Más antiguo',type:'d|1'},
			{name:'Relevancia',type:'r|-1'},
			{name:'Precio menor',type:'p|1'},
			{name:'Precio mayor',type:'p|-1'},
		];
		
		
		
		
		
		$rootScope.title = $state.current.title+" | "+$stateParams.term;
		
		$scope.total = 0;
		$scope.max = 12;
		$scope.start = 0;
		$scope.currentPage = 1;

		var dat = {
				order: $scope.order.split("|")[0],
				orientation: $scope.order.split("|")[1],
				term: $stateParams.term,
				maxprice: 0,
				minprice: 0,
				type: null,
				start : $scope.start,
				max : $scope.max,
				status: null,
				category: null,
				token: $cookies.get('token'),
				tokensecret: $cookies.get('tokenSecret')
			};
		$scope.items=[];
		var canceller;
		$scope.list = function(){
			if(canceller)
				canceller.resolve();
			canceller = $q.defer();
					
			$scope.showSpinner=true;
			$http.get('/api/search/item',{
				params:dat,
				timeout:canceller.promise}).
			success(function(data) {
				if(data.items && data.items.length>0){
					$scope.items = data.items;
				}else{
					$scope.items = [];
					$scope.listempty=true;
				}
				$scope.total = data.total;
				$scope.showSpinner=false;
			});
		};
		$scope.list();
		
		
		
		
		$scope.filterorder = function(order){
			$scope.order = order;
			dat.order = $scope.order.split("|")[0];
			dat.orientation = $scope.order.split("|")[1];
			$scope.list();
		};
		
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$rootScope.title = $state.current.title+" | "+$stateParams.term+" | Página "+currentPage;
			$scope.list();
		};
		$scope.showSpinner=true;
					
		$http.get('/api/category/parents').
				success(function(data, status, headers, config) {
					  $scope.categories = data.categories;
			});

		$scope.call_child = function (id_parent,name_cat){
				$scope.showSpinner=true;
				$http.get('/api/category/childs/id/'+id_parent).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						$scope.categories = [];
						$scope.main_cat = name_cat;
						$scope.categories = data.categories;
					});	
		}			
			
			
		$scope.filterprice = function(){
			console.log($scope.minprice);
			$scope.showSpinner=true;
			$scope.range_price=$scope.minprice+' - '+$scope.maxprice;
			dat.maxprice = $scope.maxprice;
			dat.minprice = $scope.minprice;
			$scope.list();
		};
		
		$scope.filtertype = function(type,name){
			$scope.showSpinner=true;
			$scope.type_fil=name;
			$scope.items = [];
			dat.type = type;
			$scope.list();
		}
		
		$scope.filterstatus = function(status,name){
			$scope.showSpinner=true;
			$scope.status_used=name;
			$scope.items = [];
			$scope.list();
		}	
		
		$scope.clear = function(f){
			if(f=='cat'){
				$scope.showSpinner=true;
				$scope.main_cat = null;
				$scope.categories=[];
				$http.get('/api/category/parents').
					success(function(data, status, headers, config) {
						  $scope.categories = data.categories;
				});
				dat.category = null;
								
			}
			if(f=='range'){
				$scope.showSpinner=true;
				$scope.range_price=null;
				$scope.items = [];
				$scope.maxprice=null;
				$scope.minprice=null;				
				dat.maxprice = null;			
				dat.minprice = null;			
			}
			if(f=='subcat'){
				$scope.showSpinner=true;
				$scope.sub_cat = null;
				$scope.categories=[];
				$http.get('/api/category/parents').
					success(function(data, status, headers, config) {
						  $scope.categories = data.categories;
				});
				dat.category = null;			
			}
			if(f=='use'){
				$scope.showSpinner=true;
				$scope.status_used=null;
				$scope.items = [];
				dat.status = null;
					
			}
			if(f=='type'){
				$scope.showSpinner=true;
				$scope.type_fil=null;
				$scope.items = [];
				dat.type = null;		
			}
			$scope.list();
		}
		$scope.filtercat = function(id_cat,name){
			$scope.sub_cat = name;
			$scope.showSpinner=true;
			$scope.items = [];
			dat.category = id_cat;
			$scope.list();
		}
		if($stateParams.numberpage){
			$scope.pageChange($stateParams.numberpage);
		}

		$('.number-f').keypress(function(event) {
			if (event.which !=8 && (event.which < 47 || event.which > 59)){
				if (event.which != 46 && (event.which < 47 || event.which > 59))
					{
						event.preventDefault();
						if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
							event.preventDefault();
						}else if((event.which == 8) && ($(this).indexOf('.') != -1)){
							event.preventDefault();
						}
					}				
			}

			
		});		
}
function ChangePassController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$scope.send=function(){
				$scope.showSpinner=true;
				$http.put('/api/users/password', { oldpassword:$scope.form.oldpassword, newpassword:$scope.form.newpassword,token:$cookies.get('token'),tokensecret:$cookies.get('tokenSecret') })
			   .then(function (response) {
					console.log('all is good', response.data);
					$scope.showSpinner=false;
					if(response.data.e==0){
						$cookies.put('token',response.data.token);
						$cookies.put('tokenSecret',response.data.tokenSecret);
						toastr.success('Haz cambiado tu contraseña','', {progressBar:true});						
					}else{
						$scope.showSpinner=false;
						toastr.error(response.data.e,'', {progressBar:true});						
					}
				}, function (error) {
					$scope.showSpinner=false;
					toastr.error('Ups!','', {progressBar:true});
				});				
			}			
		}
}
function ChangeEmailController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$scope.showSpinner=true;
		if($cookies.get('token') && $cookies.get('tokenSecret')){
			if ($cookies.get('token').length<9 || $cookies.get('tokenSecret').length<36) {
			   $location.path('/');
			}
		}else{
			$scope.send=function(){
				$scope.showSpinner=true;
				$http.put('/api/users/mail', { oldmail:$scope.form.oldmail, newmail:$scope.form.newmail,token:$cookies.get('token'),tokensecret:$cookies.get('tokenSecret') }).then(function (response) {
					console.log('all is good', response.data);
					$scope.showSpinner=false;
					if(response.data.e==0){
						$cookies.put('token',response.data.token);
						$cookies.put('tokenSecret',response.data.tokenSecret);
						toastr.success('Haz cambiado tu Email','', {progressBar:true});						
					}else{
						$scope.showSpinner=false;
						toastr.error(response.data.e,'', {progressBar:true});						
					}
				}, function (error) {
					$scope.showSpinner=false;
					toastr.error('Ups!','', {progressBar:true});
				});				
			}
		}
}
function ProfileController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$rootScope.title = $state.current.title;
		$scope.showSpinner=true;

}

function MainControllerFooter($rootScope, $state, $scope, $http, $location, $stateParams, $cookies, $cookies, $window) {
		$rootScope.title = $state.current.title;
		$scope.contacto={};
		$scope.redes={};

		$http.get('/api/footer/contacto')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.contacto = data.result;
			}
		});

		$http.get('/api/footer/social')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.redes = data.result;
			}
		});
}

function ProfileParticularController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore, Upload, $templateCache) {

		$rootScope.title = $state.current.title;
		$scope.countries = [];
		$scope.states = [];
		$scope.cities = [];
		$scope.details = [];
		$scope.showSpinner=true;
		$scope.file = false;
		$scope.titleform = 'Informacion Basica';
				
		//GET COUNTRY

		
		$http.get('/api/data/countries').
		success(function(data, status, headers, config) {
			$scope.countries = data.countries;
			$scope.showSpinner=false;
			$scope.form.country = data.countries[0]._id;
			
		$http.get('/api/users', {
			params: {
				tokensecret:$cookies.get('tokenSecret'),
				token:$cookies.get('token')
			}
		 }).success(function (data,status) {
		     if (typeof data.user != 'undefined'){
				$scope.details = data.user;	
				$scope.form.deliveryaddress = data.user.deliveryaddress;	
				$scope.form.country = data.user.country;	
				$scope.form.state = data.user.state;	
				$scope.form.city = data.user.city;

				
					$http.get('/api/data/states/'+$scope.details.country, {
						params: {}
						})
					 .success(function (data,status) {
							$scope.states = data.states;	
								
									$http.get('/api/data/cities/'+$scope.details.state, {
										params: {}
										})
									 .success(function (data,status) {
											$scope.cities = data.cities;
											$scope.form.city = $scope.details.city;		
									});		
																
					});
		     }
				
			});
		});
		
		//GET STATE
		$scope.getstate=function(id){
			$http.get('/api/data/states/'+id, {
				params: {}
				})
			 .success(function (data,status) {
					$scope.states = data.states;		
			});			
		}
		//GET CITY
		$scope.getcity=function(id){
		$http.get('/api/data/cities/'+id, {
			params: {}
		 }).success(function (data,status) {
				$scope.cities = data.cities;		
			});
		}
		//GET DETAILS


		
		$scope.send=function(){
				$http.put('/api/users/particular/',{
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					name:$scope.form.name,
					lastname:$scope.form.lastname,
					deliveryaddress:$scope.form.address,
					country:$scope.form.country,
					state:$scope.form.state,
					city:$scope.form.city,
					postal:$scope.form.postal,
					dob:$scope.form.DoB,
					}).
					success(function(data, status, headers, config) {
						$('#closeModal').click();
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Datos actualizados','', {progressBar:true});
							$templateCache.remove('views/profileinfo')}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});			
		}
		
		$scope.uploadPhoto = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event){
			if($file!=null){
				$scope.file = $file;
				$scope.showSpinner=true;
				
				var data = {
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					profilepic:$scope.file
				}
				
				Upload.upload({
						url: '/api/users/profilepic',
						arrayKey:'',
						method:'PUT',
						data: data
					}).then(function (response) {
						$scope.showSpinner=false;
						if(response.data.e==0){
							toastr.success('Imagen de perfil actualizada correctamente','', {progressBar:true});				
						}else{
							toastr.error(response.data.mes,'', {progressBar:true});
						}
					}, function (response) {
					
					}, function (evt) {
					});
			}
		}

}
function ProfileParticularPhoneController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$rootScope.title = $state.current.title;
		$scope.mails = [];
		$scope.mailsNew = [];
		$scope.showSpinner=true;
		
		$scope.titleform = 'Numeros de Telefono';
		$scope.getMails = function (){
				$scope.showSpinner=true;
				$scope.mails = [];
				$scope.mailsNew = [];
				$http.get('/api/users/phone',{
					params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}
					}).
					success(function(data, status, headers, config) {
						$scope.mails=data.phones;
						$scope.showSpinner=false;
						angular.copy($scope.mails,$scope.mailsNew);
				});	
		}
			$http.get('/api/users/phone',{
				params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}
				}).
				success(function(data, status, headers, config) {
					$scope.showSpinner=false;
					$scope.mails=data.phones;
					angular.copy($scope.mails,$scope.mailsNew);
				});		
		$scope.delete=function(mail){
				$http.delete('/api/users/phone',{
					params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token'),oldphone:mail}
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Telefono eliminado','', {progressBar:true});
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});		
		}

		$scope.update=function(newmail,i){
			var index = $scope.mailsNew.indexOf(newmail);
				$http.put('/api/users/phone',{
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					oldphone:$scope.mails[i],
					newphone:newmail
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Telefono actualizado','', {progressBar:true});
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
		}

		$scope.add=function(){
				$http.post('/api/users/phone/',{
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					newphone:$scope.form.newmail
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Telefono agregado','', {progressBar:true});
							$('#closeModal').click();
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
		}

		$scope.number_p =function(event) {
			console.log(event);
			if (event.which !=8 && (event.which < 47 || event.which > 59)){
				if ((event.which < 47 || event.which > 59))
					{
						event.preventDefault();
						if ( ($(this).indexOf('.') != -1)) {
							event.preventDefault();
						}else if((event.which == 8) && ($(this).indexOf('.') != -1)){
							event.preventDefault();
						}
					}				
			}

			
		}
}
function ProfileParticularEmailController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$rootScope.title = $state.current.title;
		$scope.mails = [];
		$scope.mailsNew = [];
		$scope.showSpinner=true;
		
		$scope.titleform = 'Direccion de Correo';
		$scope.getMails = function (){
				$scope.showSpinner=true;
				$scope.mails = [];
				$scope.mailsNew = [];
				$http.get('/api/users/mail',{
					params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}
					}).
					success(function(data, status, headers, config) {
						$scope.mails=data.mails;
						$scope.showSpinner=false;
						angular.copy($scope.mails,$scope.mailsNew);
				});	
		}
			$http.get('/api/users/mail',{
				params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}
				}).
				success(function(data, status, headers, config) {
					$scope.showSpinner=false;
					$scope.mails=data.mails;
					angular.copy($scope.mails,$scope.mailsNew);
				});		
		$scope.delete=function(mail){
				$http.delete('/api/users/mail/',{
					params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token'),oldmail:mail}
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Email borrado','', {progressBar:true});
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});		
		}

		$scope.update=function(newmail,i){
			var index = $scope.mailsNew.indexOf(newmail);
				$http.put('/api/users/mail/',{
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					oldmail:$scope.mails[i].mail,
					newmail:newmail
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Datos actualizados','', {progressBar:true});
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
		}

		$scope.add=function(){
				$http.post('/api/users/mail/',{
					tokensecret:$cookies.get('tokenSecret'),
					token:$cookies.get('token'),
					newmail:$scope.form.newmail
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Email agregado','', {progressBar:true});
							$('#closeModal').click();
							$scope.getMails();
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});
		}

}
function ProfileParticularPassChangeController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore) {
		$rootScope.title = $state.current.title;
		$scope.titleform = 'Cambiar Contraseña';
		$scope.showSpinner=false;
		$scope.send=function(){
			if($scope.form.newpassword==$scope.form.renewpassword){
				$scope.showSpinner=true;
				$http.put('/api/users/password/',{
						tokensecret:$cookies.get('tokenSecret'),
						token:$cookies.get('token'),
						oldpassword:$scope.form.oldpassword,
						newpassword:$scope.form.newpassword
					}).
					success(function(data, status, headers, config) {
						$('#closeModal').click();
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Datos actualizados','', {progressBar:true});
							$cookies.put('token',data.token);
							$cookies.put('tokenSecret',data.tokenSecret);
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});	
			}else{
				toastr.error('Contraseñas no coinciden','', {progressBar:true})
			}		
		}
}

function ForgotPasswordController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {

		$rootScope.title = $state.current.title;
		$scope.sendRequestPass = function(){
				$http.post('/api/users/forgotpassword/',{
					user:$scope.form.user,
					mail:$scope.form.mail
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Email enviado','', {progressBar:true});
							$location.path('/');
						}else
							toastr.error(data.mes,'', {progressBar:true});
					});
		}

		$scope.sendRequestPassMail = function(){
			$http.post('/api/users/forgotemail/',{
				mail:$scope.form.mail
				}).
				success(function(data, status, headers, config) {
					$scope.showSpinner=false;
					if(data.e==0){
						toastr.success('Email enviado','', {progressBar:true});
						$location.path('/');
					}else
						toastr.error(data.mes,'', {progressBar:true});
				});
		}

		$scope.sendRequestPassUser = function(){
			$http.post('/api/users/forgotuser/',{
				user:$scope.form.user
				}).
				success(function(data, status, headers, config) {
					$scope.showSpinner=false;
					if(data.e==0){
						toastr.success('Email enviado','', {progressBar:true});
						$location.path('/');
					}else
						toastr.error(data.mes,'', {progressBar:true});
				});
	}
}
function RestarPasswordController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {

		$scope.sendPass = function(){
			if($scope.form.pass==$scope.form.repass){
					$scope.showSpinner=true;
					$http.put('/api/users/forgotpassword/',{
						token:$stateParams.token,
						tokensecret:$stateParams.tokensecret,
						newpassword:$scope.form.repass
						}).
						success(function(data, status, headers, config) {
							$scope.showSpinner=false;
							if(data.e==0){
								toastr.success('Contraseña actualizada','', {progressBar:true});
								$location.path('/');
							}else
								toastr.error(data.mes,'', {progressBar:true});
						});
				}else{
					toastr.error('Contraseñas no coinciden','', {progressBar:true});
				}
		}
}
function LogOutController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		$cookies.remove("token");
		$cookies.remove("tokenSecret");
		$window.location.href='/';
}
function TermsController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		$scope.form={};
		$http.get('/api/footer/terminos')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form.texto = data.result.texto;
				$scope.subtitle = data.result.titulo;
				$scope.form.e = data.e;
			}
		});

		$http.get('/api/footer/privacidad')
		.success(function(data) {
			if(data.e===0)
			{
				$scope.form.texto1 = data.result.texto;
				$scope.subtitle1 = data.result.titulo;
			}
		});

}
function DoneController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
	
		//COMPRADOR
		$scope.poitnscalidadcss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnscomunicacioncss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnsdisponiblecss=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		var ratecommunication,rateavailability,ratequality;

		//VENDEDOR
		$scope.poitnscalidadcss_s=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnscomunicacioncss_s=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.poitnsdisponiblecss_s=['puntos valor1','puntos valor2','puntos valor3','puntos valor4','puntos valor5'];
		$scope.sellermail=[];
		$scope.sellerphones=[];
		$scope.comments
		
		$scope.paytransations={};
		$scope.getransations={};
		
		$scope.buyermail=[];
		$scope.buyerphones=[];
		var ratecommunication_s,rateavailability_s,ratequality_s;
		$scope.profile={};
		$rootScope.title = "Kulbid | Transacción";

		
		$http.get('api/transactions/'+$stateParams.id,{params:{tokensecret:$cookies.get('tokenSecret'),token:$cookies.get('token')}}).
				success(function(data, status, headers, config) {
					if(data.e==0){
						$rootScope.title = "Kulbid | Transacción "+data.transaction.item.name;
						$scope.paytransations=data.transaction;
						$scope.profile=data.transaction.item;						
						$scope.buyer=data.transaction.buyer;
						$scope.buyermail=data.transaction.buyer.mails;						
						$scope.buyerphones=data.transaction.buyer.phones;
						$scope.buyeraddress = data.transaction.buyer.deliveryAddress;
						$scope.seller=data.transaction.seller;						
						$scope.sellermail=data.transaction.seller.mails;						
						$scope.sellerphones=data.transaction.seller.phones;		
						
						ratecommunication=data.transaction.buyer.rate.buyer.communication;
						rateavailability=data.transaction.buyer.rate.buyer.avialability;
						ratequality=data.transaction.buyer.rate.buyer.quality;
						
						ratecommunication_s=data.transaction.seller.rate.seller.communication;
						rateavailability_s=data.transaction.seller.rate.seller.avialability;
						ratequality_s=data.transaction.seller.rate.seller.quality;
						
						
						
						if(data.user.rate.buyerSum>0){
							$scope.poitnscalidadcss[parseInt(ratequality)]='puntos valor'+parseInt(ratequality+1)+' puntosObs';
							$scope.poitnscomunicacioncss[parseInt(ratecommunication)]='puntos valor'+parseInt(ratecommunication+1)+' puntosObs';
							$scope.poitnsdisponiblecss[parseInt(rateavailability)]='puntos valor'+parseInt(rateavailability+1)+' puntosObs';
						}
						
						if(data.user.rate.sellerSum>0){
							$scope.poitnscalidadcss_s[parseInt(ratequality_s)]='puntos valor'+parseInt(ratequality_s+1)+' puntosObs';
							$scope.poitnscomunicacioncss_s[parseInt(ratecommunication_s)]='puntos valor'+parseInt(ratecommunication_s+1)+' puntosObs';
							$scope.poitnsdisponiblecss_s[parseInt(rateavailability_s)]='puntos valor'+parseInt(rateavailability_s+1)+' puntosObs';
						}
					}else{
						toastr.warning(data.mes,'', {progressBar:true});
					}
										
				});
		$scope.start=0;
		$scope.max=10;
		$scope.currentPage=1;
		
		$scope.listreports = function(){
			$http.get('/api/report/transaction/'+$stateParams.id,{
				params:{
					start: $scope.start,
					max: $scope.max,
					token:$cookies.get('token'),
					tokensecret:$cookies.get('tokenSecret')
				}
			}).then(function(response){
				$scope.total = response.data.total;
				$scope.comments=response.data.reports;
			},function(){});
		};
		$scope.listreports();		
		
		$scope.pageChange = function(currentPage){
			$scope.currentPage = currentPage;
			$scope.start = $scope.max * (currentPage-1);
			$scope.listreports();
		};

		
		$scope.close = function(){
			$http.post('api/transactions/close/'+$stateParams.id,{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}).
			success(function(data, status, headers, config) {
				$scope.showSpinner=false;
				if(data.e==0){
					toastr.success('Reclamos cerrados','', {progressBar:true});
				}else
					toastr.error(data.mes,'', {progressBar:true});
			});	
		};	
				
		$scope.getinfo = function(){
			$scope.showSpinner=false;
			$http.get('/api/report/transaction/'+$stateParams.id,{
				ignoreLoadingBar: true,
				params:{
					token:$cookies.get('token'),
					tokensecret:$cookies.get('tokenSecret')
				}}).then(function(response){
					if(response.data.e===0){
						$scope.getransations=response.data;
					}else{
						toastr.warning(data.mes,'', {progressBar:true});
					}
				},function(){});	
		}
				
		$scope.send = function(){
			$scope.showSpinner=true;
			$http.get('/api/transactions/pay/'+$stateParams.id,{
				params:{
					token:$cookies.get('token'),
					tokensecret:$cookies.get('tokenSecret')
				}}).then(function(response){
					if(response.data.e===0){
						$window.location.href=response.data.url;
					}else{
						$scope.showSpinner=false;
						toastr.warning(response.data.mes,'', {progressBar:true});
					}
				},function(){});	
		}
		
		$scope.sendComent = function(){
				console.log($scope.form.quality);
				$http.post('api/transactions/rate/'+$stateParams.id,{
						token:$cookies.get('token'),
						tokensecret:$cookies.get('tokenSecret'),
						status:$scope.form.status,
						comment:$scope.form.comment,
						avialability:$scope.form.avialability,
						communication:$scope.form.communication,
						quality:$scope.form.quality
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Calificacion Enviada','', {progressBar:true});
							$location.path('/myitems');
							}
						else
							toastr.error(data.mes,'', {progressBar:true});
					});			
		}
		$scope.sendReg = function(){
				$http.post('/api/report/transaction/'+$stateParams.id,{
						token:$cookies.get('token'),
						tokensecret:$cookies.get('tokenSecret'),
						comment:$scope.form.comment
					}).
					success(function(data, status, headers, config) {
						$scope.showSpinner=false;
						if(data.e==0){
							toastr.success('Reclamo Enviado','', {progressBar:true});
							$('#closeModal').click();
							
							$scope.listreports();
								
						}else
							toastr.error(data.mes,'', {progressBar:true});
					});		
		}
				// transactions/rate/id
				//status,comment,
}

function ProfileStoreController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		$rootScope.title = $state.current.title;
		$scope.titleform = 'Informacion de Tienda';
		// api/users/store
		$scope.send = function(){
		
					$http.put('/api/users/store',{
						token:$cookies.get('token'),
						tokensecret:$cookies.get('tokenSecret'),
						storename:$scope.form.name,
						storeaddress:$scope.form.address,
						storetype:$scope.form.type
						}).
						success(function(data, status, headers, config) {
							$scope.showSpinner=false;
							if(data.e==0){
								toastr.success('Datos actualizados','', {progressBar:true});
								setTimeout(function(){
									$location.path('/');
								}, 5000);
								}
							else
								toastr.error(data.mes,'', {progressBar:true});
						});
				
		}
}

function ProfilePaypalController($rootScope, $state, $scope, $http, $location, $stateParams,$cookies,$cookieStore,$window) {
		$rootScope.title = $state.current.title;
		$scope.titleform = 'Informacion de Paypal';
		$scope.datapay={};
		$scope.form={};
		// api/users/store
		$http.get('/api/users/paypal',{
			ignoreLoadingBar: true,
			params:{
				token:$cookies.get('token'),
				tokensecret:$cookies.get('tokenSecret')
			}}).then(function(response){
				if(response.data.e===0){
					$scope.datapay=response.data.user;
					if(typeof $scope.datapay != 'undefined'){
					    $scope.form.namepay=$scope.datapay.name;
					    $scope.form.lastnamepay=$scope.datapay.lastName;
					    $scope.form.emailpay=$scope.datapay.payMail;
					}
				}
			},function(){});
	
		$scope.showSpinner=false;
		$scope.send_p = function(){
		
					$http.post('/api/users/paypal',{
						token:$cookies.get('token'),
						tokensecret:$cookies.get('tokenSecret'),
						name:$scope.form.namepay,
						lastname:$scope.form.lastnamepay,
						paymail:$scope.form.emailpay
						}).
						success(function(data, status, headers, config) {
							$scope.showSpinner=false;
							if(data.e==0){
								toastr.success('Datos de Paypal Agregados','', {progressBar:true});
							}else
								toastr.error(data.mes,'', {progressBar:true});
						});
				
		}
		$scope.send = function(){
		
					$http.put('/api/users/paypal',{
						token:$cookies.get('token'),
						tokensecret:$cookies.get('tokenSecret'),
						name:$scope.form.namepay,
						lastname:$scope.form.lastnamepay,
						mail:$scope.form.emailpay
						}).
						success(function(data, status, headers, config) {
							$scope.showSpinner=false;
							if(data.e==0){
								toastr.success('Datos de Paypal actualizados','', {progressBar:true});
								setTimeout(function(){
									$location.path('/');
								}, 5000);
								}
							else
								toastr.error(data.mes,'', {progressBar:true});
						});
				
		}

	$scope.code = $window.var1;
	if($scope.code != undefined)
	{
		console.log($scope.code);
		$http.put('/api/users/obtenerperfil/'+$scope.code,{
			token:$cookies.get('token'),
			tokensecret:$cookies.get('tokenSecret')
		}).
			success(function(data){
				if(data.e==0){
					toastr.success('Datos de Paypal actualizados','', {progressBar:true});
					$scope.userinfo = data.userinfo;
					console.log($scope.userinfo);
					$scope.obtenerMail();
				}
			})
	}

	

	$scope.obtenerLink = function(){
		$http.get('/api/users/obtenerlink').
		success(function(result) {
			$scope.subtitle = "Inicie sesión con su cuenta Paypal";
			$scope.mostrarMail = false;
			$scope.link = result.link;
			$scope.img = "https://www.paypalobjects.com/webstatic/en_US/developer/docs/lipp/loginwithpaypalbutton.png";  
			console.log($scope.img);
		});
	}

	$scope.obtenerMail = function(){
		$http.put('/api/users/obtenermail',{
			token:$cookies.get('token'),
			tokensecret:$cookies.get('tokenSecret')
		}).
			success(function(data) {
				if(data.e==0){
					$scope.mostrarMail = true;
					$scope.id = data.result._id;
					console.log($scope.id);
					$scope.mail = data.result.paypal;
					$scope.subtitle = "Tu cuenta Paypal asociada es ";
					$scope.remove = function(){
						if($scope.id){
							$http.post('/api/users/deletepaypal/'+$scope.id,{
								token1 : $cookies.get('token'),
								tokensecret1 : $cookies.get('tokenSecret')
							}).
							then(function(response){
								if(response.data.e===0){
									toastr.success("Cuenta eliminada",'', {progressBar:true});
									$scope.obtenerLink();
								}else{
									toastr.error(response.data.mes,'', {progressBar:true});
								}
							},function(error){
								
							});
						}else
							toastr.error("Perfil inválido",'', {progressBar:true});
					};
				} else {
					$scope.obtenerLink();
				}
		});
	}

	$scope.obtenerMail();



}