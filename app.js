//mongod --dbpath /var/MEAN/Kulbid/data --port 27018
var express = require('express');
var compression = require('compression');
var minify = require('express-minify');
var CronJob = require('cron').CronJob;
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoskin = require('mongoskin');
var db = mongoskin.db('mongodb://localhost:27017/kulbid');
var ObjectID = require('mongoskin').ObjectID;
var mailer = require('./routes/utils/mailer');
var utils = require('./routes/utils/utils');
var notification = require('./routes/utils/notification');
var Paypal = require('paypal-adaptive');
var favicon = require('serve-favicon');
var passport = require('passport');

var paypalSdk = new Paypal({
    userId:    'josebaperezrodrigo_api1.hotmail.com',
    password:  '4GYT7LAQEE5BXJLH',
    signature: 'AAD3KozP8NzbIO4HlwmOVxfLKd60ArD2xNH-lkZBMfCqBp4LEiiV71L1',
    appId: 'APP-49E3692753674460U'
});    

//db.collection("categories").remove({});

/*db.collection("categories").find({}).toArray(function(err, res){
	if(res.length > 0){
		for(let key in res){
			let image = res[key].image.split("_")[1];
			console.log(image);
			db.collection("categories").update({_id: new ObjectID(res[key]._id)}, {"$set": { image : image}});
		}
	}
});*/

/*db.collection("users").update({"mails.mail":"gabomartz@gmail.com"},{"$set":{user:"gabo"}}, function(err, user){
	console.log(user);
});*/

//db.collection("maxBids").remove({});

/*setInterval(function() {
    db.collection('paymentsToCheck').find({}).toArray(function(err,transs){
        if(err){
            console.log(err);
        }else if(transs.length>0){
            var handleLoop = function(trms){
                db.collection('debtPayments').findOne({payKey:trms.payKey},function(err,trns){
                    if(err) console.log(err);
                    else if(trns){
                        var params = {
                            payKey : trns.payKey
                        };
                        paypalSdk.paymentDetails(params, function (err, response) {
                            if (err) {
                                console.log(err);
                            }else {
                                switch(response.status){
                                    case "COMPLETED":
                                        utils.getCompleteUser(db, trns.user, function(user){
                                            if(user){
                                                var buyerMails = [];
                                                    
                                                for(var i = 0 ; i < user.mails.length ; i++){
                                                    if(user.mails[i].confirmed)
                                                        buyerMails.push(user.mails[i].mail);
                                                }
                                                
                                                var mailOptions = mailer.debtPayed(user.particular.name);
                                                    mailOptions.to = buyerMails.join(',');
                                                    mailOptions.send();
                                                    
                                                notification.debtPayed(db, trns.user);
                                            }
                                        });
                                        db.collection('debtPayments').update({_id:new ObjectID(trns._id)},{"$set":{completed:true,status:"S"}});
                                        db.collection('transactions').update({_id:{"$in":trns.appliesTo}},{"$set":{kulbidPayed:true}});
                                        break;
                                    case "ERROR":
                                        utils.getCompleteUser(db, trns.user, function(user){
                                            if(user){
                                                var buyerMails = [];
                                                    
                                                for(var i = 0 ; i < user.mails.length ; i++){
                                                    if(user.mails[i].confirmed)
                                                        buyerMails.push(user.mails[i].mail);
                                                }
                                                
                                                var mailOptions = mailer.debtPaymentError(user.particular.name);
                                                    mailOptions.to = buyerMails.join(',');
                                                    mailOptions.send();
                                                    
                                                notification.debtPaymentError(db, trns.user);
                                            }
                                        });
                                        db.collection('paymentsToCheck').remove({_id:trms._id});
                                        db.collection('debtPayments').update({_id:new ObjectID(trns._id)},{$set:{completed:true,status:"E"}});
                                        break;
                                    case "CANCELED":
                                        utils.getCompleteUser(db, trns.user, function(user){
                                            if(user){
                                                var buyerMails = [];
                                                    
                                                for(var i = 0 ; i < user.mails.length ; i++){
                                                    if(user.mails[i].confirmed)
                                                        buyerMails.push(user.mails[i].mail);
                                                }
                                                
                                                var mailOptions = mailer.debtPaymentError(user.particular.name);
                                                    mailOptions.to = buyerMails.join(',');
                                                    mailOptions.send();
                                                    
                                                notification.debtPaymentError(db, trns.user);
                                            }
                                        });
                                        db.collection('paymentsToCheck').remove({_id:trms._id});
                                        db.collection('debtPayments').update({_id:new ObjectID(trns._id)},{$set:{completed:true,status:"C"}});
                                        break;
                                }
                                
                            }
                        });
                    }
                    
                    
                });
            };
            
            for(var i = 0 ; i < transs.length ; i++)
                handleLoop(transs[i]);
        }
    });
}, 5000);*/


setInterval(function(){
    db.collection('transactionsToCheck').find({}).toArray(function(err,transs){
        if(err){
            console.log(err);
        }else if(transs.length>0){
            var handleLoop = function(trns){
                var transaction = new ObjectID(trns.transaction),
                    //paystatus =  trns.paystatus,
                    paykey = trns.payKey,
                    params = {
                        payKey: trns.payKey
                    };
                paypalSdk.paymentDetails(params, function (err, response) {
                    if (err) {
                        console.log(err);
						console.log(response);
						db.collection('transactionsToCheck').remove({_id:new ObjectID(trns._id)});
						db.collection('transactions').update(
							{_id:transaction,"paypalTransactions.payKey":paykey},
							{"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"E"}},
							function(err){
								if(err) console.log(err);
							}
						);
						db.collection('transactions').findOne({_id:transaction},function(err,trans){
							if(err) console.log(err);
							
							utils.completeTransaction(db,trans,function(trns){
								var buyerMails = [];
									
								for(var i = 0 ; i < trns.buyer.mails.length ; i++){
									if(trns.buyer.mails[i].confirmed)
										buyerMails.push(trns.buyer.mails[i].mail);
								}
									
								var mailOptions2 = mailer.paymentError(
									trns.buyer.particular.name,
									trns.item.name,
									transaction);
									mailOptions2.to = buyerMails.join(',');
									mailer.sendMail(mailOptions2);
									
								notification.paymentError(db, trns.item.name, transaction, trns.buyer._id);
							});
							
						});
						
                    }else {
                        switch(response.status){
							case "CREATED":
								if(trns.time<(Date.now()-72*60*60*1000)){
									db.collection('transactionsToCheck').remove({_id:new ObjectID(trns._id)});
								}
								break;
                            case "COMPLETED":
                                db.collection('transactions').update(
                                    {_id:transaction,"paypalTransactions.payKey":paykey},
                                    {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"S"}},
                                    function(err){
                                        if(err) console.log("update error",err);
                                    }
                                );
                                
                                db.collection('transactionsToCheck').remove({_id:new ObjectID(trns._id)});
                                
                                db.collection('transactions').update(
                                    {_id:transaction},
                                    {"$set":{kulbidPayed:true,paymentType:'P',payed:true}},
                                    function(err){
                                        if(err) console.log(err);
                                    }
                                );
                                db.collection('transactions').findOne({_id:transaction},function(err,trans){
                                    if(err) console.log(err);
                                    
                                    utils.completeTransaction(db,trans,function(trns){
                                        var buyerMails = [],
                                            sellerMails = [];
                                            
                                        for(var i = 0 ; i < trns.buyer.mails.length ; i++){
                                            if(trns.buyer.mails[i].confirmed)
                                                buyerMails.push(trns.buyer.mails[i].mail);
                                        }
                                        
                                        for(i = 0 ; i < trns.seller.mails.length ; i++){
                                            if(trns.seller.mails[i].confirmed)
                                                sellerMails.push(trns.seller.mails[i].mail);
                                        }
                                        
                                        var mailOptions = mailer.productPayed(
                                            trns.seller.particular.name,
                                            trns.item.name,
                                            transaction,
                                            trns.buyer.user);
                                            mailOptions.to = sellerMails.join(',');
                                            mailer.sendMail(mailOptions);
                                            
                                        var mailOptions2 = mailer.paymentProcesed(
                                            trns.buyer.particular.name,
                                            trns.item.name,
                                            transaction);
                                            mailOptions2.to = buyerMails.join(',');
                                            mailer.sendMail(mailOptions2);
                                            
                                        notification.paymentProcesed(db, trns.item.name, transaction, trns.buyer._id);
                                        notification.productPayed(db, trns.buyer.user, trns.item.name, transaction, trns.seller._id);
                                    });
                                    
                                });
                                
                                break;
                            case "ERROR":
								db.collection('transactionsToCheck').remove({_id:new ObjectID(trns._id)});
                                db.collection('transactions').update(
                                    {_id:transaction,"paypalTransactions.payKey":paykey},
                                    {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"E"}},
                                    function(err){
                                        if(err) console.log(err);
                                    }
                                );
                                db.collection('transactions').findOne({_id:transaction},function(err,trans){
                                    if(err) console.log(err);
                                    
                                    utils.completeTransaction(db,trans,function(trns){
                                        var buyerMails = [];
                                            
                                        for(var i = 0 ; i < trns.buyer.mails.length ; i++){
                                            if(trns.buyer.mails[i].confirmed)
                                                buyerMails.push(trns.buyer.mails[i].mail);
                                        }
                                            
                                        var mailOptions2 = mailer.paymentError(
                                            trns.buyer.particular.name,
                                            trns.item.name,
                                            transaction);
                                            mailOptions2.to = buyerMails.join(',');
                                            mailer.sendMail(mailOptions2);
                                            
                                        notification.paymentError(db, trns.item.name, transaction, trns.buyer._id);
                                    });
                                    
                                });
                                
                                break;
                            case "CANCELED":
								db.collection('transactionsToCheck').remove({_id:new ObjectID(trns._id)});
                                db.collection('transactions').update(
                                    {_id:transaction,"paypalTransactions.payKey":paykey},
                                    {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"F"}},
                                    function(err){
                                        if(err) console.log(err);
                                    }
                                );
                                db.collection('transactions').findOne({_id:transaction},function(err,trans){
                                    if(err) console.log(err);
                                    
                                    utils.completeTransaction(db,trans,function(trns){
                                        var buyerMails = [];
                                            
                                        for(var i = 0 ; i < trns.buyer.mails.length ; i++){
                                            if(trns.buyer.mails[i].confirmed)
                                                buyerMails.push(trns.buyer.mails[i].mail);
                                        }
                                            
                                        /*var mailOptions2 = mailer.paymentError(
                                            trns.buyer.particular.name,
                                            trns.item.name,
                                            transaction);
                                            mailOptions2.to = buyerMails.join(',');
                                            mailer.sendMail(mailOptions2);
                                            
                                        notification.paymentError(db, trns.item.name, transaction, trns.buyer._id);*/
                                    });
                                    
                                });
                                
                                break;
                        }
                        
                    }
                });
            };
            
            for(var i = 0 ; i < transs.length ; i++)
                handleLoop(transs[i]);
        }
    });
}, 5000);


var client_index = require('./routes/client/index');
var admin_index = require('./routes/admin/index');
var uploads = require('./routes/uploads/uploads');
var api_notifications = require('./routes/api/notifications');
var api_transactions = require('./routes/api/transaction');
var api_categories = require('./routes/api/category');
var api_favorites = require('./routes/api/favorites');
var api_comments = require('./routes/api/comments');
var api_history = require('./routes/api/history');
var api_report = require('./routes/api/report');
var api_search = require('./routes/api/search');
var api_users = require('./routes/api/user');
var api_items = require('./routes/api/items');
var api_cart = require('./routes/api/cart');
var api_data = require('./routes/api/data');
var api_site = require('./routes/api/site');
var api_pages = require('./routes/api/permisos');
var api_footer = require('./routes/api/footer');
var app = express();

new CronJob('* * 6 * * *',function(){
  
  db.collection('users').find({active:1}).toArray(function(err,users){
    if(err){
      //console.log(err);
    }else if(users.length>0){
      var handleLoop = function(user){
        utils.getUserRate(db,user._id,function(rate){
          var sum = (rate.seller.communication + rate.seller.avialability + rate.seller.quality)/3;
          db.collection('items').update({user:user._id,active:1},{"$set":{rate:sum}});
        });
      };
      var i=0;
      for(;i < users.length ; i++)
        handleLoop(users[i]);
    }
  });
  
}, null, true, 'America/Mexico_City');

new CronJob('01 * * * * *', function() {
	db.collection('items').find({time:{"$lt":Date.now()},active:1}).toArray(function(err,items){
		if(err){
			//console.log(err);  
		}else if(items){
			var ids = [],i=0;
			var handleLoop = function(item){
				db.collection('users').findOne({_id:new ObjectID(item.user)},function(err,user){
					if(err){
						//console.log(err);
					}else if(user){
						var mails = user.mails, l = mails.length, mm = [];
						for(var i = 0; i < l ; i++)
							if(mails[i].confirmed)
								mm.push(mails[i].mail);
							mm = mm.join(',');
						var mailStock = mailer.productDone(user.particular.name, item.name);
							mailStock.to = mm;
							mailer.sendMail(mailStock);
						notification.productDone(db,item.name,user._id);
							  
						if(item.type=="A"){
							utils.getHighestBid(db, item._id, function(bid){
								if(bid){
									utils.getCompleteUser(db, new ObjectID(bid.user._id), function(buyer){
										if(buyer){
											db.collection('tax').findOne({"$query":{},"$orderby":{_id:-1}},function(err,tax){
												if(err) console.log(err);
												tax = tax.tax || 5;
												tax = tax / 100;
												tax = Math.round(((bid.bid) * tax) * 100) / 100;
												var data = {
													item: item._id,
													time: Date.now(),
													buyer: new ObjectID(bid.user._id),
													seller: new ObjectID(item.user),
													ammount: 1,
													price: bid.bid,
													kulbidTax: tax + 4,
                                                    payed: false,
													kulbidPayed: false,
                                                    paymentType: "A",
                                                    closed: false,
                                                    paypalTransactions: [],
													rates: {
														buyer: {
															status: 'P'
														},
														seller: {
															status: 'P'
														}
													}
												};
												db.collection('transactions').insert(data,function(err){
													if(err){
														//console.log(err);
													}else{
														var buyerMails = [];					
														for(var j = 0 ; j < buyer.mails.length ; j++)
															if(buyer.mails[j].confirmed)
																buyerMails[j] = buyer.mails[j].mail;
																		
														var mailPurchase = mailer.newPurchase(buyer.particular.name, item.name, bid.bid, 1, user);
															mailPurchase.to = buyerMails.join(',');
															mailer.sendMail(mailPurchase);
														notification.newPurchase(db,item.name,buyer._id);
																		   
														var mailSale = mailer.newSale(user.particular.name, item.name, bid.bid, 1, buyer, tax);
															mailSale.to = mm;
															mailer.sendMail(mailSale);
														notification.newSale(db,item.name,user._id);
													}
												});
											});
										}
									});
								}
							});
						}	
					}
				});
			};
	  
	  
      for(;i<items.length;i++){
        ids[i] = new ObjectID(items[i]._id);
        handleLoop(items[i]);
      }
      db.collection('items').update({_id:{"$in":ids}}, {"$set":{active:3}}, {multi: true}, function(err){
        if(err){
          //console.log(err);  
        }
      });
    }
  });
}, null, true, 'America/Mexico_City');

//console.log(__dirname+'/minCache');
app.use(compression());
app.use(minify({
  cache: __dirname+'/minCache/',
  uglifyJS: undefined,
  cssmin: undefined,
  onerror: undefined,
  js_match: /javascript/,
  css_match: /css/,
  sass_match: /scss/,
  less_match: /less/,
  stylus_match: /stylus/,
  json_match: /json/,
}));
app.use(function(req,res,next){
    req.db = db;
    res._uglifyMangle = false;
    next();
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(express.compress());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());




app.use(express.static(__dirname + '/public/'));
app.use(favicon(path.join(__dirname,'public','images','kulbid.ico')));
app.use('/api/notifications',api_notifications);
app.use('/api/transactions',api_transactions);
app.use('/api/favorites',api_favorites);
app.use('/api/category',api_categories);
app.use('/api/comments',api_comments);
app.use('/api/history',api_history);
app.use('/api/report',api_report);
app.use('/api/search',api_search);
app.use('/api/items',api_items);
app.use('/api/users',api_users);
app.use('/api/data',api_data);
app.use('/api/cart',api_cart);
app.use('/api/site',api_site);
app.use('/api/permisos', api_pages);
app.use('/api/footer', api_footer);
app.use('/uploads', uploads);
app.use('/admin', admin_index);


var openConnections = [];

function expell(token, tokenSecret, res){
    console.log(token+" "+tokenSecret);
    for (var j = 0 ; j < openConnections.length ; j++) {
        console.log(openConnections[j]);
        for(var o = 0 ; o < openConnections[j].sets.length ; o++){
            console.log(openConnections[j].sets[o]);
            if (openConnections[j].sets[o].token == token &&
                openConnections[j].sets[o].tokenSecret == tokenSecret) {
                console.log(openConnections[j].sets[o].res.length);
                for(var k = 0 ; k < openConnections[j].sets[o].res.length ; k++){
                    openConnections[j].sets[o].res[k].write('id: ' + Date.now() + '\n');
                    openConnections[j].sets[o].res[k].write('data:' + JSON.stringify({logout:true}) +   '\n\n');
                    openConnections[j].sets[o].res[k].flush();
                }
                console.log("REMOVED!");
                openConnections[j].sets.splice(o,1);
                if(openConnections[j].sets.length === 0)
                    openConnections.splice(j,1);
                return;
            }
        }
    }
    res.end();
}

function sendMessage(sets,mes){
    for(var k = 0 ; k < sets.length ; k++)
        for(var i = 0 ; i < sets[k].res.length ; i++){
            sets[k].res[i].write('id: ' + mes.id + '\n');
            sets[k].res[i].write('data:' + JSON.stringify(mes) +   '\n\n');
            sets[k].res[i].flush();
        }
}

function prepareMessages(resp){
        var user = resp.user,
            sets = resp.sets;   
        db.collection("notifications").find({user:user,status:0}).toArray(function(err,arr){
            if(err){
              console.log(err);
            }else if(arr.length>0){
                
                for(var i = 0 ; i < arr.length ; i++){
                    var mes = {
                        id  : arr[i]._id,
                        mes : arr[i].mes,
						status : 1,
						time: arr[i].time,
                        url : arr[i].url
                    };
                    sendMessage(sets,mes);
                }
                db.collection("notifications").update({user:user,status:0},{"$set":{status:1}},{multi: true});
            }
        });
        
    }
	
setInterval(function() {

    for(var j = 0 ; j < openConnections.length ; j++){
        prepareMessages(openConnections[j]);
    }
	
    db.collection("notifications").find({time:{"$lt":(Date.now()-10*60*1000)},status:0},{"$set":{status:1}},{multi: true});
}, 1000);


app.use('/api/logout/:token/:tokensecret',function(req,res){
        var db = req.db,
            token = req.params.token,
            tokenSecret = req.params.tokensecret;
        db.collection("users").update({"bitacora.token": token, "bitacora.tokenSecret": tokenSecret},
            {"$set":{"bitacora.$.active":false}},
            function(){
                expell(token, tokenSecret, res);
            });
    });

app.use('/events/:token/:tokenSecret',function(req,res){
    var token = req.params.token,
        tokenSecret = req.params.tokenSecret,
        db = req.db;
    
    utils.checkHash(db, token, tokenSecret, function(user){
        if(user){
          
            var userOID = new ObjectID(user._id);
            req.socket.setTimeout(Number.MAX_VALUE);
            res.writeHead(200,
              {"Content-Type":"text/event-stream",
              "Cache-Control":"no-cache",
              "Connection":"keep-alive"});
            res.write("\n");
            res.flush();
            var notFound = true;
            for (var j = 0 ; j < openConnections.length ; j++) {
                if (openConnections[j].user.equals(userOID)) {
                    for(var o = 0 ; o < openConnections[j].sets.length ; o++){
                        if(openConnections[j].sets[o].token == token &&
                           openConnections[j].sets[o].tokenSecret == tokenSecret){
                            openConnections[j].sets[o].res.push(res);
                            notFound = false;
                            break;
                        }
                    }
                    break;
                }
            }
            if(notFound){
                openConnections.push(
                    {
                        user:   userOID,
                        sets:[
                            {
                                token: token,
                                tokenSecret: tokenSecret,
                                res: [res]
                            }
                        ]
                        
                    }                         
                );
            }
            
            req.on("close", function() {
                for (var j = 0 ; j < openConnections.length ; j++) {
                    if (openConnections[j].user.equals(userOID)) {
                        for(var p = 0 ; p < openConnections[j].sets.length ; p++){
                            if(openConnections[j].sets[p].token == token && openConnections[j].sets[p].tokenSecret == tokenSecret){
                                for(var k = 0 ; k < openConnections[j].sets[p].res.length ; k++){
                                    if(openConnections[j].sets[p].res[k] == res){
                                        openConnections[j].sets[p].res.splice(k,1);
                                        if(openConnections[j].sets[p].res.length === 0){
                                            openConnections[j].sets.splice(p,1);
                                            if(openConnections[j].sets.length === 0){
                                                openConnections.splice(j,1);
                                            }
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            });
        }else{
            res.json({e:1,mes:"Error"});
        }
    });
});


app.use('/', client_index);
/*app.use(function(req,res){
	res.render('index');
});*/
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    /*app.locals.pretty = true;
    app.set('view options', { pretty: true });*/
    app.use(function(err, req, res, next) {
      ////console.log(err.stack);
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
      next();
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
    next();
});


module.exports = app;
