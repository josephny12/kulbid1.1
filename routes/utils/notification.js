var ObjectID = require('mongoskin').ObjectID;

var newCommentStore = function(db, user, userID){
    
    db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Han comentado tu perfil de tienda',
        url: '/shop/user/'+user,
        status: 0,
        time: Date.now()
    });
    
};

var newReplyStore = function(db, storeName, userID){
    
    db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Han respondido tu comentario en la tienda '+storeName,
        url: '/shop/user/'+storeName,
        status: 0,
        time: Date.now()
    });
    
};


var overBid = function(db, itemName, userID, itemURL){
    
    db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Ha habido una sobrepuja en '+itemName,
        url: '/shop/'+itemURL,
        status: 0,
        time: Date.now()
    });
    
};

var newCommentItem = function(db, itemName, itemURL, userID){
    db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Han comentado tu publicación '+itemName,
        url: '/shop/'+itemURL,
        time: Date.now(),
        status: 0
    }); 
};

var newReplyItem = function(db, itemName, itemURL, userID){
    db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Han respondido tu comentario en la publicación '+itemName,
        url: '/shop/'+itemURL,
        time: Date.now(),
        status: 0
    }); 
};

var outOfStock = function(db, itemName, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Se ha agotado el inventario de '+itemName,
        url: '/myitems/mysaleslist',
        time: Date.now(),
        status: 0
    }); 
};

var newPurchase = function(db, itemName, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Felicitades por tu compra de '+itemName,
        url: '/myitems/mypurchaseslist',
        time: Date.now(),
        status: 0
    }); 
};

var newSale = function(db, itemName, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Felicitades por tu venta de '+itemName,
        url: '/myitems/mypurchaseslist',
        time: Date.now(),
        status: 0
    }); 
};

var productDone = function(db, itemName, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Tu publicación '+itemName+' ha finalizado',
        url: '/myitems/myitemslist',
        time: Date.now(),
        status: 0
    }); 
};

var productPayed = function(db, buyerName, itemName, transactionID, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'El usuario '+buyerName+' ha pagado el producto '+itemName,
        url: '/done/'+transactionID,
        time: Date.now(),
        status: 0
    }); 
};


var itemRated = function(db, itemName, transactionID, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Tu contraparte te ha calificado por la transacción de '+itemName,
        url: '/done/'+transactionID,
        time: Date.now(),
        status: 0
    }); 
};

var paymentProcesed = function(db, itemName, transactionID, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Se ha procesado tu pago de '+itemName,
        url: '/done/'+transactionID,
        time: Date.now(),
        status: 0
    }); 
};

var paymentError = function(db, itemName, transactionID, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Tu pago de ha sido cancelado '+itemName,
        url: '/done/'+transactionID,
        time: Date.now(),
        status: 0
    }); 
};

var debtPaymentError = function(db, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Ha ocurrido un error con el pago de tu deuda',
        url: '/profile/debt',
        time: Date.now(),
        status: 0
    }); 
};

var debtPayed = function(db, userID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Se ha saldado tu deuda',
        url: '/profile/debt',
        time: Date.now(),
        status: 0
    }); 
};

var newReport = function(db, userID, transactionID){
	db.collection("notifications").insert({
        user: new ObjectID(userID),
        mes: 'Se ha reportado una de tus transacciones',
        url: '/done/'+transactionID,
        time: Date.now(),
        status: 0
    }); 
};

var exports = {
	newCommentStore:newCommentStore,
    newReplyStore: newReplyStore,
	newReplyItem: newReplyItem,
    newCommentItem: newCommentItem,
	outOfStock: outOfStock,
	newPurchase:newPurchase,
    productDone:productDone,
	newSale:newSale,
    productPayed:productPayed,
    paymentProcesed:paymentProcesed,
    paymentError:paymentError,
    debtPaymentError:debtPaymentError,
    debtPayed:debtPayed,
    newReport:newReport,
	overBid:overBid,
	itemRated:itemRated
};

module.exports = exports;