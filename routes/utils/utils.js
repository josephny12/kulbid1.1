var ObjectID = require('mongoskin').ObjectID;


function pullAndPush(collection,query,pull,psh,cb){
	collection.update(query,pull.statement,pull.options,function(err){
		if(err){
			console.log(err);
			cb(false);
		}
		collection.update(query,psh.statement,psh.options,function(err){
			if(err){
				console.log(err);
				cb(false);
			}
			cb(true);
		});
	});
}

function isInFavAndCart(db,user,item,cb){
     if(ObjectID.isValid(user)){
        db.collection("favorites").findOne({user:new ObjectID(user), "items.item" : new ObjectID(item)},function(err,fav){
            var isFav=fav?true:false;
            db.collection("cart").findOne({user:new ObjectID(user), "items.item" : new ObjectID(item)},function(err,cart){
				var isInCart=cart?true:false;
				cb(isFav,isInCart);
			});  
        });
     }else
		cb(false,false);
}

function itemsInFavAndCart(db,user,items,cb){
	var il = items.length, round = 1, i=0;
        
        function handleLoop(k){
            isInFavAndCart(db,user,items[k]._id,function(isFav,isInCart){
				items[k].isFavorite = isFav;
				items[k].isInCart = isInCart;
				if(round == il)
					cb(items);
				round++;
            });
        }

	if(il>0)
		for(;i<il;i++)
			handleLoop(i);
	else
		cb(items); 
}

function getUserAvg(db, user, cb){
	getUserRate(db,user,function(rates){
		var buyerAvg = (rates.buyer.communication + rates.buyer.avialability + rates.buyer.quality) / 3,
			sellerAvg = (rates.seller.communication + rates.seller.avialability + rates.seller.quality) / 3;
		cb(buyerAvg, sellerAvg);
	});
};

function getUserRate(db,user,cb){
     if(ObjectID.isValid(user)){
        db.collection("transactions").find({buyer:new ObjectID(user)}).toArray(function(err,buyerRates){
               if(err)
                    console.log(err);
                        
                var buyerRate = {
                    communication: 0,
                    avialability: 0,
                    quality: 0    
                };
                if(buyerRates.length>0){
                        var brl = buyerRates.length, i =0, buytot = 0;
                        for(;i<brl;i++){
                            var rate = buyerRates[i].rates.seller.rate, status = buyerRates[i].rates.seller.status;
                            switch(status){
                                case 'S':
                                        buyerRate.communication += parseInt(rate.communication);
                                        buyerRate.avialability += parseInt(rate.avialability);
                                        buyerRate.quality += parseInt(rate.quality);
										buytot++;
                                        break;
                                case 'C':
                                        buyerRate.communication += parseInt(buyerRate.communication)/(i+1);
                                        buyerRate.avialability += parseInt(buyerRate.avialability)/(i+1);
                                        buyerRate.quality += parseInt(buyerRate.quality)/(i+1); 
										buytot++;
                                        break;
                                case 'F':
                                        buyerRate.communication += 0;
                                        buyerRate.avialability += 0;
                                        buyerRate.quality += 0; 
										buytot++;
                                        break;
                            }
                        }
                        if(buytot>0){
                            buyerRate.communication = Math.round(buyerRate.communication/buytot);
                            buyerRate.avialability = Math.round(buyerRate.avialability/buytot);
                            buyerRate.quality = Math.round(buyerRate.quality/buytot); 
                        }
                 }
                db.collection("transactions").find({seller:new ObjectID(user)}).toArray(function(err,sellerRates){
                        if(err)
                        console.log(err);
                        var sellerRate = {
                            communication: 0,
                            avialability: 0,
                            quality: 0    
                        };
                        if(sellerRates.length>0){
                                var srl = sellerRates.length, i =0, seltot = 0;
                                for(;i<srl;i++){
									console.log();
                                    var rate = sellerRates[i].rates.buyer.rate, status = sellerRates[i].rates.buyer.status;
                                    switch(status){
                                        case 'S':
                                                sellerRate.communication += parseInt(rate.communication);
                                                sellerRate.avialability += parseInt(rate.avialability);
                                                sellerRate.quality += parseInt(rate.quality);
												seltot++;
                                                break;
                                        case 'C':
                                                sellerRate.communication += parseInt(sellerRate.communication)/(i+1);
                                                sellerRate.avialability += parseInt(sellerRate.avialability)/(i+1);
                                                sellerRate.quality += parseInt(sellerRate.quality)/(i+1); 
												seltot++;
                                                break;
                                        case 'F':
                                                sellerRate.communication += 0;
                                                sellerRate.avialability += 0;
                                                sellerRate.quality += 0; 
												seltot++;
                                                break;
                                    }
                                }
                                
                                if(seltot>0){
                                    sellerRate.communication = Math.round(sellerRate.communication/seltot);
                                    sellerRate.avialability = Math.round(sellerRate.avialability/seltot);
                                    sellerRate.quality = Math.round(sellerRate.quality/seltot); 
                                }  
                        }
                        cb({buyer :buyerRate,
							buyerSum: buyerRates.length,
							seller:sellerRate,
							sellerSum: sellerRates.length});
                });
                
        });
     }else
        cb({
            buyer :{
                communication: 0,
                avialability: 0,
                quality: 0},
            seller:{
                communication: 0,
                avialability: 0,
                quality: 0}
        });
        
        
}

function checkHash(db,token,tokenSecret,cb){
		if(token && tokenSecret){
			db.collection("users").findOne({"bitacora.token":token,"bitacora.tokenSecret":tokenSecret,"bitacora.active":true},function(err,user){
				if (err) throw err;
				if(user){
					cb(user);
				}else
					cb(false);
			});
		}else
			cb(false);
	}

function giveCategoryToItem(db,item,cb){
        db.collection("categories").findOne({_id:new ObjectID(item.category)},function(err,cat){
                if(cat)
                        item.categoryName = cat.name;
                else
                        item.categoryName = false;
                cb(item);
        });
}

function giveCategoryToItems(db,items,cb){
    var il = items.length, round = 1, i=0;
        
        function handleLoop(k){
            giveCategoryToItem(db,items[k],function(item){
		items[k] = item;
		if(round == il)
			cb(items);
		round++;
            });
        }

	if(il>0)
		for(;i<il;i++)
			handleLoop(i);
	else
		cb(items);   
}

function getFullItem(db,id,cb){
        db.collection("items").findOne({_id:new ObjectID(id)},function(err,item){
			if(item){
                getFullUser(db,item.user,function(user){
                    if(user)
                        item.user = user;
                    giveCategoryToItem(db,item,function(itm){
						if(itm.type=="A"){
							getHighestBid(db,id,function(bid){
								if(bid)
									itm.price = bid.bid;
								cb(itm);
							});
						}else
							cb(itm);        
                    });
                });
			}else
				cb(false);
		});
}

function getFullItems(db,ids,cb){
	var il = ids.length, round = 1, i=0, items = [];
	
	function handleLoop(k){
		getFullItem(db, ids[k], function(item){
			items.push(item);
			if(round == il)
				cb(items);
			else
				round++;
		});
		
		
	}
	
	
	if(il>0)
		for(; i < il ; i++)
			handleLoop(i);
	else
		cb(false);
	
}

function completeTransaction(db, transaction, cb){
    getFullItem(db, transaction.item, function(item){
        if(item)
            transaction.item = item;
        getCompleteUser(db,transaction.buyer,function(buyer){
            if(buyer)
                transaction.buyer = buyer;
            getCompleteUser(db,transaction.seller,function(seller){
                if(seller)
                    transaction.seller = seller;
				transaction.processing = false;	
				if(transaction.paypalTransactions && Array.isArray(transaction.paypalTransactions))
					for(var i = 0; i < transaction.paypalTransactions.length ; i++){
						if(transaction.paypalTransactions.completed){
							transaction.processing = true;
							break;
						}
					}	
				db.collection("reports").count({to:new ObjectID(transaction._id)},function(err,count){
					transaction.reports = count;
					cb(transaction);
				});
            });
        });    
    });
} 

function getCompleteUser(db,id,cb){
		db.collection("users").findOne({_id:new ObjectID(id)},function(err,user){
			if(user){
				delete user.bitacora;
				delete user.postal;
				delete user.priv;
				delete user.password;
				delete user.particular.DoB;
				delete user.particular.address;
				delete user.password;
				
				user.paypal = user.paypal !== undefined;
				
				if(user.photo.length<5){
					if(user.type==1)
						user.photo = "particular.jpg";
					else
						user.photo = "stores.jpg";
				}
				
                giveLocationToUser(db,user,function(data){
                    getUserRate(db,data._id,function(rate){
                        data.rate = rate;     
                        cb(data);
                    });
                });
			}else
				cb(false);
		});
	}


function getFullUser(db,id,cb){
		db.collection("users").findOne({_id:new ObjectID(id)},function(err,user){
			if(user){
				delete user.bitacora;
				delete user.mails;
				delete user.phones;
				delete user.postal;
				delete user.paypal;
				delete user.priv;
				delete user.password;
				delete user.particular.DoB;
				delete user.particular.address;
				delete user.password;
				
				user.paypal = user.paypal !== undefined;
				
				if(user.photo.length<5){
					if(user.type==1)
						user.photo = "particular.jpg";
					else
						user.photo = "stores.jpg";
				}
				
                giveLocationToUser(db,user,function(data){
                    getUserRate(db,data._id,function(rate){
                        data.rate = rate;     
                        cb(data);
                    });
                });
			}else
				cb(false);
		});
	}

function getMiniUser(db,id,cb){
	db.collection("users").findOne({_id:new ObjectID(id)},function(err,user){
		if(user){
			
			if(user.photo.length<5){
			    if(user.type==1)
				    user.photo = "particular.jpg";
				else
					user.photo = "stores.jpg";
			}
			
			var data = {
				_id : id,
				user: user.user,
				photo:user.photo,
				type:user.type,
				particular:{
                                        name: user.particular.name
                                }
			};
                        
			if(user.type==2){
				data.store = user.store;
			}
			cb(data);
		}else
			cb(false);
		});
	}	
	
function giveUsersToItems(items,db,cb){
	var il = items.length, round = 1, i=0;
	function callBack(user,id){
		if(user){
			var j = 0;
			for(;j<il;j++){
				if(items[j]._id == id)
					items[j].user = user;
			}
		}
		if(round == il)
			cb(items);
		round++;
	}
        
        function handleLoop(k){
            getMiniUser(db,items[k].user,function(user){
		callBack(user,items[k]._id);
            });
        }

	if(il>0)
		for(;i<il;i++)
			handleLoop(i);
	else
		cb(items);
}	
	
function findEmail(db,email,cb){
	db.collection("users").findOne({"mails.mail":email},function(err,user){
			if(user){
				cb(user);
			}else
				cb(false);
	});
}

function findUsername(db,username,cb){
	db.collection("users").findOne({"$or": [{"user":username}, {"mails.mail": username}]},function(err,user){
		if(user){
			cb(user);
		}else
			cb(false);
	});
}

function giveLocationToUser(db,user,cb){
        var rounds = 0, i = 0;
        function callBack(){
             i++;   
             if(rounds == i)
               cb(user);
        }
        
        if(user.country && ObjectID.isValid(user.country)){
             rounds++;
                
        }else
                user.countryName = "";
        
        if(user.state && ObjectID.isValid(user.country)){
             rounds++;
        }else
                user.stateName = "";
        
        if(user.city && ObjectID.isValid(user.country)){
             rounds++;
        }else
                user.cityName = "";
        
        if(user.country && ObjectID.isValid(user.country)){
            db.collection('countries').findOne({_id:new ObjectID(user.country)},function(err,cc){
                if(cc){
                   user.countryName = cc.name;
                }else
                   user.countryName = "Desconocido";
                callBack();
            });
        }
        
        if(user.state && ObjectID.isValid(user.state)){
            db.collection('states').findOne({_id:new ObjectID(user.state)},function(err,ss){
                if(ss){
                   user.stateName = ss.name;
                }else
                   user.stateName = "Desconocido";
                callBack();
            });
        }
        
        if(user.city && ObjectID.isValid(user.city)){
               db.collection('cities').findOne({_id:new ObjectID(user.city)},function(err,ci){
                if(ci){
                   user.cityName = ci.name;
                }else
                   user.cityName = "Desconocido";
                callBack();
            }); 
        }
        
        if(rounds===0)
                cb(user);
}


function getHighestBid(db, item, cb){
	db.collection("bids").findOne({"$query":{item:new ObjectID(item)},"$orderby":{bid:-1}},function(err,bid){
			if(err)
				cb(false);
			else if(bid){
				getMiniUser(db, bid.user, function(user){
					if(user)
						bid.user = user;
					cb(bid);	
				});
			}else
				cb(false);
	});
	
	
}

function getDebt(db, userID, cb){
	db.collection('transactions').find({seller:new ObjectID(userID),
		kulbidPayed:false,
		"rates.buyer.status":{"$ne":"P"},
		"rates.seller.status":{"$ne":"P"}
	}).toArray(function(err,rows){
		if(err){
			cb(false);
		}else if(rows.length>0){
			var owed = [], total = 0, rounds = 1;
			
			var handleLoop = function(k){
				var trans = rows[k];
				completeTransaction(db, trans, function(trns){
					total += trns.kulbidTax;
					owed.push({
						_id: trns._id,
						item: trns.item.name,
						buyer: trns.buyer.user
					});
					if(rounds == rows.length)
						cb({total:total, list:owed});
					rounds++;
				});
				
			};
			if(rows.length>0)
				for(var i = 0 ; i < rows.length ; i++)
					handleLoop(i);
			
		}else
			cb({total:0,list:[]});
	});
}


var exports = {
    isInFavAndCart:	isInFavAndCart,
	itemsInFavAndCart: itemsInFavAndCart,
	checkHash:	checkHash,
	pullAndPush: pullAndPush,
	findUsername:	findUsername,
	findEmail:		findEmail,
	getHighestBid: getHighestBid,
    getMiniUser:	getMiniUser,
    getFullUser:	getFullUser,
	getCompleteUser: getCompleteUser,
    getFullItem:	getFullItem,
	getFullItems: getFullItems,
    getUserRate:	getUserRate,
    giveCategoryToItem: giveCategoryToItem,
    giveCategoryToItems:giveCategoryToItems,
	giveUsersToItems:   giveUsersToItems,
    giveLocationToUser: giveLocationToUser,
	completeTransaction:completeTransaction,
	getDebt:getDebt,
	getUserAvg:getUserAvg
};

module.exports = exports;
