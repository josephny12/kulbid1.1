var mailer = require('nodemailer');
var transporter = mailer.createTransport({
	service: 'Zoho',
	host: this.service,
    port: 587,
	secure: false,
	ignoreTLS: true,
	requireTLS: false, 
    auth: {
        user: 'notify@kulbid.com',
        pass: 'Notify123'
    }
});
transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});

var wrapBody = function(body, header){
		header = header || "";
		return '<div style="font-family:sans-serif"><div style="width:100%;margin:0 auto"><header style="margin-bottom:5px; width:100%;height:25px;background:#009ee2"></header><h1 style="text-align: center">'+header+'<img align="middle"src="https://kulbid.com/uploads/180/logo1.png"/></h1><section style="width: 100%;text-align: justify; padding: 1em;">'+body+'</section><table style="width: 100%;"><tr><td style="width: 33%; vertical-align: middle; text-align: center;"><img align="middle" src="https://kulbid.com/uploads/icon_mail.png"/><a style="font-size:18px; font-weight: bold; color:#000; text-decoration: none;" href="mailto:info@kulbid.com">info@kulbid.com</a></td><td style="width: 33%; vertical-align: middle; text-align: center;"><img align="middle" src="https://kulbid.com/uploads/icon_phone.png"/><span style="font-size:18px; font-weight: bold;">+52 55 8421 7422</span></td><td style="width: 33%; vertical-align: middle; text-align: center;"><img align="middle" src="https://kulbid.com/uploads/icon_world.png"/><a style="font-size:18px; font-weight: bold; color:#000; text-decoration: none;" href="https://kulbid.com">kulbid.com</a></td></tr></table><footer style="margin-top:5px; width:100%;height:25px;background:#009ee2"></footer></div></div>';
	};	
	
var forgotMail = function(name,token,tokenSecret){
	return {
	//	from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Recupera tu contraseña', // Subject line
		text: 'Saludos '+name+'\nTe enviamos este mensaje para que puedas recuperar tu contraseña.\nSi quieres restablecer la contraseña y volver a acceder a tu cuenta, pulsa en la dirección siguiente o pégala en tu navegador.\n https://kulbid.com/forgotpassword/'+token+'/'+tokenSecret+'\nSi no necesitas recuperar tu contraseña, ignora este mensaje.', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br>Te enviamos este mensaje para que puedas recuperar tu contraseña.<br>Si quieres restablecer la contraseña y volver a acceder a tu cuenta, pulsa en la dirección siguiente o pégala en tu navegador.<br><a href="https://kulbid.com/forgotpassword/'+token+'/'+tokenSecret+'">https://kulbid.com/forgotpassword/'+token+'/'+tokenSecret+'</a><br>Si no necesitas recuperar tu contraseña, ignora este mensaje.') // html body					
	};
};

var productDone = function(name, item){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Tu publicación '+item+' ha finalizado', // Subject line
		text: 'Saludos '+name+'\n Te informamos que la publicación del articulo '+item+' ha finalizado', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br> Te informamos que la publicación del articulo <b>'+item+'</b> ha finalizado.') // html body					
	};
};

var outOfStock = function(name, item){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Se ha agotado el inventario de '+item, // Subject line
		text: 'Saludos '+name+'\n Te informamos que el articulo '+item+' se ha quedado sin inventario, si deseas continuar vendiendo este articulo deberas incrementar el stock.', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br> Te informamos que el articulo <b>'+item+'</b> se ha quedado sin inventario, si deseas continuar vendiendo este articulo deberas incrementar el stock.') // html body					
	};
};

var newReplyStore = function(name, storeName, commentName, commentPhoto, comment){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Han respondido tu comentario en la tienda '+storeName, // Subject line
		text: 'Saludos '+name+'\n El usario '+commentName+' ha dicho:\n \t'+comment+' \npuedes responder accediendo al siguiente enlace\nhttps://kulbid.com/shop/user/'+storeName, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usario <b>'+commentName+'</b> ha dicho:</p><br><blockquote style="padding-left: 100px;"><div style="width: 100%; display: flex; flex-direction: row; align-items: center;"><img src="https://kulbid.com/uploads/100/'+commentPhoto+'" style="width: 100px; height: 100px; border-radius: 50%; object-fit: cover; margin: 0 1em 0 0;"><div class="mensaje_text"><p><strong>'+commentName+'</strong></p><p>'+comment+'</p></div></div></blockquote></section><br><a href="https://kulbid.com/shop/user/'+storeName+'">Responder</a>') // html body					
	};
};

var newReply = function(name, item, itemurl, commentName, commentPhoto, comment){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Han respondido tu comentario en la publicación '+item, // Subject line
		text: 'Saludos '+name+'\n El usario '+commentName+' ha dicho:\n \t'+comment+' \npuedes responder accediendo al siguiente enlace\nhttps://kulbid.com/shop/'+itemurl, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usario <b>'+commentName+'</b> ha dicho:</p><br><blockquote style="padding-left: 100px;"><div style="width: 100%; display: flex; flex-direction: row; align-items: center;"><img src="https://kulbid.com/uploads/100/'+commentPhoto+'" style="width: 100px; height: 100px; border-radius: 50%; object-fit: cover; margin: 0 1em 0 0;"><div class="mensaje_text"><p><strong>'+commentName+'</strong></p><p>'+comment+'</p></div></div></blockquote></section><br><a href="https://kulbid.com/shop/'+itemurl+'">Responder</a>') // html body					
	};
};

var newCommentStore = function(name, storeName, commentName, commentPhoto, comment){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Han comentado tu perfil de tienda', // Subject line
		text: 'Saludos '+name+'\n El usario '+commentName+' ha dicho:\n \t'+comment+' puedes responder accediendo al siguiente enlace\nhttps://kulbid.com/shop/user/'+storeName, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usario <b>'+commentName+'</b> ha dicho:</p><br><blockquote style="padding-left: 100px;"><div style="width: 100%; display: flex; flex-direction: row; align-items: center;"><img src="https://kulbid.com/uploads/100/'+commentPhoto+'" style="width: 100px; height: 100px; border-radius: 50%; object-fit: cover; margin: 0 1em 0 0;"><div class="mensaje_text"><p><strong>'+commentName+'</strong></p><p>'+comment+'</p></div></div></blockquote></section><br><a href="https://kulbid.com/shop/user/'+storeName+'">Responder</a>') // html body					
	};
};

var overBid = function(name, item, itemurl){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Alguien ha sobrepujado '+item, 
		text: 'Saludos '+name+'\n Alguien ha sobrepujado el articulo por el que pujaste, Sí estas interesado en seguir pujando puedes clica en\nhttps://kulbid.com/shop/'+itemurl, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Alguien ha sobrepujado el articulo por el que pujaste, Sí estas interesado en seguir pujando puedes clica en</section><br><a href="https://kulbid.com/shop/'+itemurl+'">'+item+'</a>') // html body					
	};
};

var newComment = function(name, item, itemurl, commentName, commentPhoto, comment){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Han comentado tu publicación '+item, // Subject line
		text: 'Saludos '+name+'\n El usario '+commentName+' ha dicho:\n \t'+comment+' puedes responder accediendo al siguiente enlace\nhttps://kulbid.com/shop/'+itemurl, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usario <b>'+commentName+'</b> ha dicho:</p><br><blockquote style="padding-left: 100px;"><div style="width: 100%; display: flex; flex-direction: row; align-items: center;"><img src="https://kulbid.com/uploads/100/'+commentPhoto+'" style="width: 100px; height: 100px; border-radius: 50%; object-fit: cover; margin: 0 1em 0 0;"><div class="mensaje_text"><p><strong>'+commentName+'</strong></p><p>'+comment+'</p></div></div></blockquote></section><br><a href="https://kulbid.com/shop/'+itemurl+'">Responder</a>') // html body					
	};
};

var newReport = function(itemName, name, transactionID, commenteer, comment, as, commentPhoto){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject:'Se ha reportado la transación del articulo'+itemName,
		text: 'Saludos '+name+'\nSe ha abierto un reporte para la venta del articulo '+itemName+', y el usuario '+commenteer+' en calidad de '+as+' ha dicho:\n '+comment+' \nhttps://kulbid.com/done/'+transactionID+'#report', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br>Se ha abierto un reporte para la venta del articulo <b>'+itemName+'</b>, y el usuario <b>'+commenteer+'</b> en calidad de '+as+' ha dicho:<br><blockquote style="padding-left: 100px;"><div style="width: 100%; display: flex; flex-direction: row; align-items: center;"><img src="https://kulbid.com/uploads/100/'+commentPhoto+'" style="width: 100px; height: 100px; border-radius: 50%; object-fit: cover; margin: 0 1em 0 0;"><div class="mensaje_text"><p>'+comment+'</p></div></div></blockquote><a href="https://kulbid.com/done/'+transactionID+'">Kulbid</a>') // html body					
	};
};

var productPayed = function(name, itemName, transactionID, buyerName){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'El usuario '+buyerName+' ha pagado '+itemName, // Subject line
		text: 'Saludos '+name+'\n El usario '+buyerName+' ha cancelado el monto acordado, puedes revisar los detalles del pago en el siguiente enlace\nhttps://kulbid.com/done/'+transactionID, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usario <b>'+buyerName+'</b> ha cancelado el monto acordado, puedes revisar los detalles del pago en el siguiente enlace</p></section><br><a href="https://kulbid.com/done/'+transactionID+'">Responder</a>') // html body					
	};
};

var paymentProcesed = function(name, itemName, transactionID){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Pago de '+itemName+' procesado', // Subject line
		text: 'Saludos '+name+'\n se ha procesado el pago de paypal que has realizado para '+itemName+' puedes revisar los detalles del pago en el siguiente enlace\nhttps://kulbid.com/done/'+transactionID, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Se ha procesado el pago de paypal que has realizado para <b>'+itemName+'</b> puedes revisar los detalles del pago en el siguiente enlace</p></section><br><a href="https://kulbid.com/done/'+transactionID+'">Responder</a>') // html body					
	};
};

var itemRated = function(name, itemName, transactionID, status){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Tu contraparte califico la transacción de '+itemName, // Subject line
		text: 'Saludos '+name+'\n Tu contraparte en la transacción de '+itemName+' la ha calificado como '+status+'\nhttps://kulbid.com/done/'+transactionID, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Tu contraparte en la transacción de <b>'+itemName+'</b> la ha calificado como <b>'+status+'</b></p></section><br><a href="https://kulbid.com/done/'+transactionID+'">Transacción</a>') // html body					
	};
};

var paymentError = function(name, itemName, transactionID){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Pago de '+itemName+' cancelado', // Subject line
		text: 'Saludos '+name+'\n Ha habido un error al procesar el pago de '+itemName+' \nhttps://kulbid.com/done/'+transactionID, // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Ha habido un error al procesar el pago de '+itemName+'</p></section><br><a href="https://kulbid.com/done/'+transactionID+'">Responder</a>') // html body					
	};
};

var debtPaymentError = function(name){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Pago de deuda ha sido cancelado', // Subject line
		text: 'Saludos '+name+'\n Ha habido un error al procesar el pago de tu deuda \nhttps://kulbid.com/profile/debt', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Ha habido un error al procesar el pago de tu deuda</p></section><br><a href="https://kulbid.com/profile/debt">Responder</a>') // html body					
	};
};

var debtPayed = function(name){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Pago de deuda ha sido compleado', // Subject line
		text: 'Saludos '+name+'\n Se ha completado el pago de su deuda \nhttps://kulbid.com/profile/debt', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>Se ha completado el pago de su deuda</p></section><br><a href="https://kulbid.com/profile/debt">Responder</a>') // html body					
	};
};

var noPayPal = function(name, buyerName, itemName){
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: buyerName+' intenta pagar '+itemName, // Subject line
		text: 'Saludos '+name+'\n El usuario '+buyerName+' esta intentando pagar su compra de '+itemName+', pero eso no es posible hasta que afilies una cuenta de paypal, puedes afiliar una ahora mismo siguiendo el siguiente enlace \nhttps://kulbid.com/profile/profilepay', // plaintext body
		html: wrapBody('<h3>Saludos '+name+'</h3><br><section style="width: 100%;text-align: justify; padding: 1em;"><p>El usuario <b>'+buyerName+'</b> esta intentando pagar su compra de <b>'+itemName+'</b>, pero eso no es posible hasta que afilies una cuenta de paypal, puedes afiliar una ahora mismo siguiendo el siguiente enlace</p></section><br><a href="https://kulbid.com/profile/profilepay">Afiliar cuenta de paypal</a>') // html body					
	};
};

var newSale = function(name, item, price, quantity, buyer, tax){
	/*var mailText = [], mailHTML = [],phoneText = [], phoneHTML = [];
	mailHTML.push("<ul>");
	for(;mi<ml;mi++){
		if(mails[mi].confirmed){
			mailText[mi] = mails[mi].mail+"\n";
			mailHTML.push("<li>"+mails[mi].mail+"</li>");
		}		
	}
	mailHTML.push("</ul>");
	
	phoneHTML.push("<ul>");
	for(;pi<pl;pi++){
		phoneText[pi] = phones[pi]+"\n";
		phoneHTML.push("<li>"+phones[pi]+"</li>");		
	}
	phoneHTML.push("</ul>");*/
	
	var particularInfoText = "", particularInfoHTML = "", storeInfoHTML = "",storeInfoText = "", storeTypes = {SA: 'Sociedad Anonima', SACV: 'Sociedad Anonima de Capital Variable', SRL: 'Sociedad de Responsabilidad Limitada', SNC: 'Sociedad de Nombre Colectivo', SCS: 'Sociedad de Comandite Simple'};
	
	particularInfoText += "Nombre: "+buyer.particular.name+" "+buyer.particular.lastName+"\nDirección de envio: "+buyer.deliveryAddress+"\n";
	
	particularInfoHTML += "<b>Nombre:</b> "+buyer.particular.name+" "+buyer.particular.lastName+"<br><b>Dirección de envio:</b> "+buyer.deliveryAddress+"<br>";
	
	if(buyer.store){
		storeInfoText += "\nInformación de la Tienda: \n";
		storeInfoText += "Nombre: "+buyer.store.name+" "+storeTypes[buyer.store.type]+"\n";
		storeInfoText += "Dirección: "+buyer.store.address+"\n";
		storeInfoHTML += "<br>Información de la Tienda: <br>";
		storeInfoHTML += "<b>Nombre</b>: "+buyer.store.name+" "+storeTypes[buyer.store.type]+"<br>";
		storeInfoHTML += "<b>Dirección</b>: "+buyer.store.address+"<br>";
	}
	
	
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Felicitades por tu venta de '+item,
		text: 'Saludos '+name+"\n Felicidades por tu venta de "+quantity+" "+item+" por un total de "+price+"$ con una comisión para Kulbid de "+tax+"$\n Información de comprador: \n"+particularInfoText+" "+storeInfoText+"\nRecuerda tratar con amabilidad al comprador y enviar su pedido máximo un día después del recibir el aviso de cobro.\n No envíes nunca el artículo antes de recibir la notificación de pago pues puedes ser objeto de una estafa y no estarás cubierto por la garantía de la plataforma de pagos.",
		html: wrapBody('<h3>Saludos '+name+'</h3><br> <p>Felicidades por tu venta de <b>'+quantity+'</b> <b>'+item+'</b> por un total de <b>'+price+'$</b> con una comisión para Kulbid de <b>'+tax+'$</b><br> Información de contacto: <br>'+particularInfoHTML+" "+storeInfoHTML+"<br>Recuerda tratar con amabilidad al comprador y enviar su pedido máximo un día después del recibir el aviso de cobro.<br> No envíes nunca el artículo antes de recibir la notificación de pago pues puedes ser objeto de una estafa y no estarás cubierto por la garantía de la plataforma de pagos.")
	};
};

	
var newPurchase = function(name, item, price, quantity, seller){
	/*var mails = seller.mails, phones = seller.phones, ml = mails.length, mailText = [], mailHTML = [], mi = 0, pi = 0, pl = phones.length, phoneText = [], phoneHTML = [];
	mailHTML.push("<ul>");
	for(;mi<ml;mi++){
		if(mails[mi].confirmed){
			mailText[mi] = mails[mi].mail+"\n";
			mailHTML.push("<li>"+mails[mi].mail+"</li>");
		}		
	}
	mailHTML.push("</ul>");
	
	phoneHTML.push("<ul>");
	for(;pi<pl;pi++){
		phoneText[pi] = phones[pi]+"\n";
		phoneHTML.push("<li>"+phones[pi]+"</li>");		
	}
	phoneHTML.push("</ul>");*/
	
	var particularInfoText = "", particularInfoHTML = "", storeInfoHTML = "",storeInfoText = "", storeTypes = {SA: 'Sociedad Anonima', SACV: 'Sociedad Anonima de Capital Variable', SRL: 'Sociedad de Responsabilidad Limitada', SNC: 'Sociedad de Nombre Colectivo', SCS: 'Sociedad de Comandite Simple'};
	
	particularInfoText += "Nombre: "+seller.particular.name+" "+seller.particular.lastName+"\n";
	
	particularInfoHTML += "<b>Nombre:</b> "+seller.particular.name+" "+seller.particular.lastName+"<br>";
	
	if(seller.store){
		storeInfoText += "\nInformación de la Tienda: \n";
		storeInfoText += "Nombre: "+seller.store.name+" "+storeTypes[seller.store.type]+"\n";
		storeInfoText += "Dirección: "+seller.store.address+"\n";
		storeInfoHTML += "<br>Información de la Tienda: <br>";
		storeInfoHTML += "<b>Nombre</b>: "+seller.store.name+" "+storeTypes[seller.store.type]+"<br>";
		storeInfoHTML += "<b>Dirección</b>: "+seller.store.address+"<br>";
	}
	
	return {
//		from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
		subject: 'Felicitades por tu compra de '+item,
		text: 'Saludos '+name+"\n Felicidades por tu compra de "+quantity+" "+item+" por un total de "+price+"$, puedes ponerte en contacto con "+seller.user+" mediante los siguientes medios:\n Información de contacto: \n"+particularInfoText+" "+storeInfoText+"\nTe aconsejamos que no completes ningún pago fuera de la plataforma de Kulbid pues puede ser una estafa y no estará cubierta por las protecciones al comprador.",
		html: wrapBody('<h3>Saludos '+name+'</h3><br> <p>Felicidades por tu compra de <b>'+quantity+'</b> <b>'+item+'</b> por un total de <b>'+price+'$</b>, puedes ponerte en contacto con <b>'+seller.user+'</b> mediante los siguientes medios:<br>Información de contacto: <br>'+particularInfoHTML+" "+storeInfoHTML+"<br>Te aconsejamos que no completes ningún pago fuera de la plataforma de Kulbid pues puede ser una estafa y no estará cubierta por las protecciones al comprador.")
	};
};

var confirmMail = function(name, username, mailToken){
		return {
//			from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
			subject: 'Confirma tu E-mail', // Subject line
			text: 'Saludos '+name+'\nEs necesario que confirmes tu email, para hacerlo haz click en el siguiente enlace:\n https://kulbid.com/register/mail/'+username+'/'+mailToken, // plaintext body
			html: wrapBody('<h3>Saludos '+name+'</h3><br>Es necesario que confirmes tu email, para hacerlo haz click en el siguiente enlace:<br><a href="https://kulbid.com/register/mail/'+username+'/'+mailToken+'">Kulbid</a>') // html body					
		};
	};

var welcomeMail = function(){
		return {
//			from: '"Registro Kulbid" <registro@kulbid.com>',
from: '"Registro Kulbid" <notify@kulbid.com>',
			subject:'Bienvenido a Kulbid',
			text:'Gracias por formar parte de la comunidad de Kulbid.\nSi necesitas ayuda para navegar por la web puedes ingresar en el apartado de ayuda.\nEsperemos que disfrutes en tu navegación por nuestra página web y app.\nGracias.',
			html:wrapBody('Gracias por formar parte de la comunidad de Kulbid.<br>Si necesitas ayuda para navegar por la web puedes ingresar en el apartado de ayuda.<br>Esperemos que disfrutes en tu navegación por nuestra página web y app.<br><a href="https://kulbid.com">Kulbid</a>','Bienvenido a')
		};
	};

var sendMail = 	function(mailOptions) {
		mailOptions.encoding = 'utf-8';
		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				return console.log(error);
			}
			console.log('Message sent: ' + info.response);
		});
	};
	
var exports = {
	wrapBody: wrapBody,
	outOfStock : outOfStock,
	confirmMail: confirmMail,
	newCommentStore: newCommentStore,
	newReplyStore: newReplyStore,
	forgotMail: forgotMail,
	welcomeMail: welcomeMail,
	sendMail: sendMail,
	newComment: newComment,
	newReport:newReport,
	newReply: newReply,
	productDone: productDone,
	newPurchase: newPurchase,
	newSale: newSale,
	paymentProcesed:paymentProcesed,
	productPayed:productPayed,
	paymentError:paymentError,
	debtPaymentError:debtPaymentError,
	debtPayed:debtPayed,
	overBid:overBid,
	itemRated: itemRated,
	noPayPal:noPayPal
};

module.exports = exports;
