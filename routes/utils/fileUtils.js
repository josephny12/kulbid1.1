var fs = require('fs');
var request = require('request');
function saveFile(files,mime,callback){
		var validMimes = [];
		switch(mime){
			case "image":
				validMimes	=	["image/jpeg","image/png"];
				break;
			default:
				return false;
				break;
		}
		
		var fl = files.length, i = 0, validFiles = [], failed = false, file, name, cicle = 0;
		function saveCallback(){
            cicle++;       
			if(failed){
               	deleteFile("routes/uploads/husk/"+arguments[0]);
				return;
			}

			if(!arguments[0]){
				var vl = validFiles.length,j=0;
				failed = true;
				callback(false);
				for(;j<vl;j++)
					deleteFile("routes/uploads/husk/"+validFiles[j]);
				return;
			}
			validFiles.push(arguments[0]);
        	if(fl == cicle)
				callback(validFiles);
		}
		if(fl){
            for(;i<fl;i++){
        		file = files[i];
				if(validMimes.indexOf(file.mimetype)>=0){
				name = Date.now()+file.filename;
					moveFile(file.path,"routes/uploads/husk/"+name,name,saveCallback);
				}else{
					saveCallback(false)
					break;
				}
			}
		}else{
			callback(false);
		}
	}
	//anexo
	var normalize = (function() {
  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç-", 
      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc ",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }      
      return ret.join( '' ).replace( /[^-A-Za-z0-9]+/g, '.' ).toLowerCase();
  }
 
})();
function moveFile(oldPath, newPath, name, callback){
	var source = fs.createReadStream(oldPath);
	var dest = fs.createWriteStream(newPath);
	function onEnd(){
		callback(name);
		deleteFile(oldPath);
	}
	
	function onError(err){
		console.log(err);
		callback(false);
		deleteFile(oldPath);
	}
	

	source.pipe(dest);
	source.on('end', onEnd);
	source.on('error', onError);
}

function deleteFile(path){
	console.log("El path es: " + path);
	fs.exists(path,function(exists){
		if(exists){
			fs.unlink(path, (err) => {
				if (err) {
					console.log("Falló la eliminación");
				} else {
					console.log("Se eliminó la imagen");
				}
			});
		}else{
		}
	
	});
	
	}

function getExtension(fileName){
     return fileName.split('.').pop().split(/\#|\?/)[0];
}

function getName(fileName){
     return fileName.split('/').pop().split(/\#|\?/)[0];
}

function downloadFile(uri, filename, name, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);
    request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){callback(name);});
  });
};

function downloadAndSave(files,mime,callback) {
        
        
    var validMimes = [];
	switch(mime){
		case "image":
			validMimes	=	["jpg","jpeg","png"];
			break;
		default:
			return false;
			break;
	}
		
	var fl = files.length, i = 0, validFiles = [], file, name, cicle = 0;
	
    
    function saveCallback(){
        cicle++;
		if(arguments[0]){
        	validFiles.push(arguments[0]);
		}
		if(fl == cicle)
			callback(validFiles);
	}    
    	
	if(fl){
        for(;i<fl;i++){
			file = files[i];
            var ext = getExtension(file);
			if(validMimes.indexOf(ext)>=0){
				name = Date.now()+encodeURIComponent(getName(file));
				downloadFile(file,"routes/uploads/husk/"+name,name,saveCallback);
			}else{
				saveCallback(false)
				break;
			}
		}
	}else{
		callback(false);
	}
}

//downloadAndSave(['http://wp.patheos.com.s3.amazonaws.com/blogs/faithwalkers/files/2013/03/bigstock-Test-word-on-white-keyboard-27134336.jpg'],'image',function(){console.log('hola');});





var exports = {
	saveFile: saveFile,
	deleteFile: deleteFile,
	moveFile:	moveFile,
	downloadAndSave: downloadAndSave

}

module.exports = exports;
