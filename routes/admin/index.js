var express = require('express');
var utils = require('../utils/utils');
var router = express.Router();

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

router.get('/views/:name', function(req, res) {
    if(req.cookies.token && req.cookies.tokenSecret){
        utils.checkHash(req.db,req.cookies.token,req.cookies.tokenSecret,function(user){
                if(user){
                    delete user.bitacora;
                    res.render('admin/parciales/'+req.params.name,{user: user,logged:true});
                }
                else{
                    res.render('admin/parciales/'+req.params.name,{logged:false,expired:true});
                }
            });
    }else
         res.render('admin/parciales/'+req.params.name,{logged:false,expired:false});
});

router.get('/*', function(req, res) {
    if(req.cookies.token && req.cookies.tokenSecret){
        utils.checkHash(req.db,req.cookies.token,req.cookies.tokenSecret,function(user){
                if(user){
                    delete user.bitacora;
                    res.render('admin/index',{user: user,logged:true});
                }
                else{
                    res.render('admin/index',{logged:false,expired:true});
                }
            });
    }else
         res.render('admin/index',{logged:false,expired:false});
});



module.exports = router;
