var express = require('express');
var sharp = require('sharp');
var fs = require('fs');
var router = express.Router();

router.get('/', function(req, res) {
	res.status(404).send('Not found');
});

router.get('/:name',function(req,res){
	var path = __dirname+'/husk/'+req.params.name;
	fs.exists(path, function(exists){
		if(exists){
			sharp(path)
				.toBuffer()
				.then(function(data) {
					res.setHeader("Cache-Control", "public, max-age=31536000");
					res.setHeader("Expires", new Date(Date.now() +31536000000).toUTCString());
					res.write(data);
					res.end();
				}).catch(function(e) {
					console.log(e);
					res.status(404).send('Not found');
				});
		}else{
			res.status(404).send('Not found');
		}
	});
});

router.get('/:maxwidth/:name',function(req,res){
	var path = __dirname+'/husk/'+req.params.name;
	fs.exists(path, function(exists){
		if(exists){
			sharp(path)
				.resize(parseInt(req.params.maxwidth),parseInt(req.params.maxwidth))
				.max()
				.withoutEnlargement()
				.toBuffer()
				.then(function(data) {
					res.setHeader("Cache-Control", "public, max-age=31536000");
					res.setHeader("Expires", new Date(Date.now() + 31536000000).toUTCString());
					res.write(data);
					res.end();
				}).catch(function(e) {
					console.log(e);
					res.status(404).send('Not found');
				});
		}else{
			res.status(404).send('Not found');
		}
	});
});


module.exports = router;
