var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var mailer = require('../utils/mailer');
var notification = require('../utils/notification');
var ObjectID = require('mongoskin').ObjectID;


router.route('/transaction/:id')
    .post(function(req,res){
      var comment = req.body.comment,
          id = req.params.id,
          token = req.body.token,
          tokenSecret = req.body.tokensecret,
          db = req.db;
      if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
               if (user) {
                  db.collection("transactions").findOne({_id:id},function(err, transaction){
                     if(err)
                        res.json({e:2,mes:"Error al recuperar transacción"});
                     else if(transaction){
                        if(transaction.closed){
                          res.json({e:3,mes:"La disputa de este articulo ha sido cerrada por un administrador"});
                        }else{
                          var userID = new ObjectID(user._id),
                              buyer = new ObjectID(transaction.buyer),
                              seller = new ObjectID(transaction.seller),
                              from = false,
                              notiTO = [];
                              
                          if(userID.equals(buyer)){
                             notiTO.push(seller);
                             from = "B";
                          }else if(userID.equals(seller)){
                             notiTO.push(buyer);
                             from = "S";
                          }else if(user.priv==2){
                             notiTO.push(seller);
                             notiTO.push(buyer);
                             from = "A";
                          }
                          
                          if(from!==false){
                             utils.completeTransaction(db, transaction, function(transaction){
                                var data = {
                                   user: userID,
                                   userIs: from,
                                   to: id,
                                   comment: comment,
                                   time: Date.now()
                                };
                                
                                db.collection("reports").insert(data,function(err,result){
                                   
                                   var sellerMails = [],
                                      buyerMails = [];
                                   
                                   for(var i = 0 ; i < transaction.seller.mails.length ; i++)
                                      if(transaction.seller.mails[i].confirmed)
                                         sellerMails.push(transaction.seller.mails[i].mail);
                                         
                                   for(i = 0 ; i < transaction.buyer.mails.length ; i++)
                                      if(transaction.buyer.mails[i].confirmed)
                                         buyerMails.push(transaction.buyer.mails[i].mail);
                                         
                                   for(i = 0 ; i < notiTO.length ; i++)
                                      notification.newReport(db, notiTO[i], id);
                                      
                                   
                                   switch(from){
                                      case "B":
                                         var mb = mailer.newReport(transaction.item.name,
                                            transaction.seller.particular.name,
                                            id,
                                            user.user,
                                            comment,
                                            "COMPRADOR",
                                            user.photo);
                                            mb.to = sellerMails.join(',');
                                         mailer.sendMail(mb);
                                         break;
                                      case "S":
                                         var ms = mailer.newReport(transaction.item.name,
                                            transaction.buyer.particular.name,
                                            id,
                                            user.user,
                                            comment,
                                            "VENDEDOR",
                                            user.photo);
                                            ms.to = buyerMails.join(',');
                                         mailer.sendMail(ms);
                                         break;
                                      case "A":
                                         var mss = mailer.newReport(transaction.item.name,
                                            transaction.buyer.particular.name,
                                            id,
                                            user.user,
                                            comment,
                                            "ADMINISTRADOR",
                                            user.photo);
                                            mss.to = buyerMails.join(',');
                                         mailer.sendMail(mss);
                                         var mbb = mailer.newReport(transaction.item.name,
                                            transaction.seller.particular.name,
                                            id,
                                            user.user,
                                            comment,
                                            "ADMINISTRADOR",
                                            user.photo);
                                            mbb.to = sellerMails.join(',');
                                         mailer.sendMail(mbb);
                                         break;
                                   }
                                   
                                   
                                   if(err)
                                      res.json({e:4,mes:"Error al guardar reporte"});
                                   else
                                      res.json({e:0,id:result.insertedIds[0]});
                                });
                                
                             });
                          }else
                             res.json({e:3,mes:"No tienes permiso para realizar esta acción"});
                        }
                        
                     }else
                        res.json({e:2,mes:"Transacción no encontrada"});
                  });
                }else{
                    res.json({e:1,mes:"Token invalido"});
                }
            });
        }else{
            res.json({e:5,mes:"ID de articulo invalido"});
        }
    }).get(function(req,res){
        var id = req.params.id,
          token = req.query.token,
          start = req.query.start || 0,
          max = req.query.max || 10,
          tokenSecret = req.query.tokensecret,
          db = req.db;
        start = parseInt(start);
        max = parseInt(max);
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user){
                  db.collection("transactions").findOne({_id:id},function(err, transaction){
                     if(err)
                        res.json({e:2,mes:"Error al recuperar transacción"});
                     else if(transaction){
                        
                        var userID = new ObjectID(user._id),
                            buyer = new ObjectID(transaction.buyer),
                            seller = new ObjectID(transaction.seller),
                            from = false;
                            
                        if(userID.equals(buyer)){
                           from = true;
                        }else if(userID.equals(seller)){
                           from = true;
                        }else if(user.priv==2){
                           from = true;
                        }
                        if(from){
                           db.collection("reports").find({"$query":{to:id},"$orderby":{time:-1}}).skip(start).limit(max).toArray(function(err, reports){
                              if(err)
                                 res.json({e:3,mes:"Error al recuperar reportes"});
                              else if(reports.length>0){
                                 db.collection("reports").count({to:id},function(err,count){
                                    if(err)
                                       res.json({e:4,mes:"Error al páginar resultados"});
                                    else{
                                       utils.giveUsersToItems(reports,db,function(reports){
                                           res.json({e:0,reports:reports,total:count});							
                                       });
                                    }
                                 });
                              }else
                                 res.json({e:0,reports:[],total:0});
                           });
                        }else
                          res.json({e:3,mes:"No tienes permiso para realizar esta acción"});
                     }else
                        res.json({e:2,mes:"Transacción no encontrada"});
                  });
                }else{
                    res.json({e:1,mes:"Token invalido"});
                }
            });
        }else{
            res.json({e:5,mes:"ID de articulo invalido"});
        }
    });
    
    

module.exports = router;    