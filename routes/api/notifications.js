var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;

router.route('/')
	.get(function(req,res){
        var token = req.query.token,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            tokenSecret = req.query.tokensecret,
            db = req.db;
		limit = parseInt(limit);
		skip = parseInt(skip);
		utils.checkHash(db, token, tokenSecret, function(user){
            if (user) {
				db.collection('notifications').find({"$query":{user:new ObjectID(user._id)},"$orderby":{time:-1}},{user:0}).skip(skip).limit(limit).toArray(function(err, notis) {
					if (err) {
						res.json({e:1});
					}else{
						db.collection('notifications').count({user:new ObjectID(user._id)},function(err,count){
							if(err){
								res.json({e:2,mes:"Error páginando resultados"});
								console.log(err);
							}else{
								res.json({e:0,notifications:notis,total:count});
                                db.collection('notifications').update({user:new ObjectID(user._id),status:1},{"$set":{status:2}},{multi: true});
							}
						});
					}
				});
            }else{
                res.json({e:1,mes:"Token invalido"});
            }
        });
	
	
	});
module.exports = router;