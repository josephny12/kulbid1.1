var express = require('express');
var multer = require('multer');
var router = express.Router();
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
var uuid = require('node-uuid');
var Hashids = require("hashids"),
    hashids = new Hashids("kulbid is great ebay is lame");
var utils = require('../utils/utils');
var fileUtils = require('../utils/fileUtils');
var mailer = require('../utils/mailer');
var upload = multer({ dest: './temp/' });
var ObjectID = require('mongoskin').ObjectID;
var paypalnode = require('paypal-rest-sdk');
var openIdConnect = paypalnode.openIdConnect;
var request = require('request');

var Paypal = require('paypal-adaptive');
var paypalSdk = new Paypal({
    userId:    'josebaperezrodrigo_api1.hotmail.com',
    password:  '4GYT7LAQEE5BXJLH',
    signature: 'AAD3KozP8NzbIO4HlwmOVxfLKd60ArD2xNH-lkZBMfCqBp4LEiiV71L1',
    appId: 'APP-49E3692753674460U'
});  

//set configs for openid_client_id and openid_client_secret if they are different from your
//usual client_id and secret. openid_redirect_uri is required
paypalnode.configure({
    'mode': 'live',
    'openid_client_id': 'AQ0dd-aV8M2AtmKk9HZS1FrKsDwJujtBqNY9qhXtf_PEb0MA15-2mW0mh26gliKNMllUChadUK0ogG3B',
    'openid_client_secret': 'EBsmJfPpb7ETObGOozFFV8RruyXcdqD9UAhi0j3LoGiE6N_hz9uaFN_VYDAKwew1j_TFAWA2zgt4hd4-',
    'openid_redirect_uri': 'https://www.kulbid.com/profile/profilepay'

});



function getSalt(cb){
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if(err) {
        	console.error(err);
			cb(false);
   		}
		cb(salt);
	});	
}

function hashPass(salt,pass,cb){
	bcrypt.hash(pass, salt, function(err, hash) {
		if(err) {
			console.error(err);
			cb(false);
		}
		cb(hash);
	});
}

function compareHashPass(hash,pass,cb){
	bcrypt.compare(pass, hash, function(err, isMatch) {
		if(err) {
			console.error(err);
			cb(false);
		}
		cb(isMatch);
    });
}

function getBitacora(days){
	var daysSum = days || 30;
	daysSum = daysSum * 24 * 60 * 60 * 1000;
	
	return {
		token : hashids.encode(Date.now()),
		tokenSecret : uuid.v4(),
		expires: Date.now() + daysSum,
        active: true
	};
}

router.route('/deletepaypal/:id')
	.post(function(req,res){
		var token = req.body.token1,
			tokenSecret = req.body.tokensecret1,
			db = req.db,
			id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			utils.checkHash(db, token, tokenSecret, function(user){
				if(user){
                    db.collection('users').update({_id:id},{"$unset":{"paypal":1}}, function(err){
                        if(err){
                            res.json({e:3,mes:"Error al eliminar cuenta de paypal"});    
                        }else
                            res.json({e:0});
                    });
				}else
					res.json({e:2,mes:"Token invalido"});
			});
		}else{
			res.json({e:1,mes:"ID invalido"});
        }
    });

router.route('/obtenermail')
    .put(function(req,res){
        var db = req.db,
            token = req.body.token,
            tokenSecret = req.body.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                req.db.collection('users').findOne({_id:new ObjectID(user._id)}, function(err, result){
                    if (err) {
                        res.json({e:1,mes:"Error al cargar los datos del perfil"});
                    } else {
                        console.log(result.paypal);
                        if(result.paypal != null)
                            res.json({e:0,result:result})
                        else
                            res.json({e:2})
                    }
                })
            } else {
                res.json({e:3,mes:"Token no encontrada"});
            }
        })
    })


router.route('/obtenerlink')
    .get(function(req,res){
        var link = openIdConnect.authorizeUrl({'scope': 'openid profile email', 'client_id':'AQ0dd-aV8M2AtmKk9HZS1FrKsDwJujtBqNY9qhXtf_PEb0MA15-2mW0mh26gliKNMllUChadUK0ogG3B'});
        res.json({e:0, link});
    })

router.route('/obtenerperfil/:tokenPaypal')
    .put(function(req,res){
        var db = req.db,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            tokenPaypal = req.params.tokenPaypal;
           
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                openIdConnect.tokeninfo.create(tokenPaypal, function (error, tokeninfo) {
                    if (error) {
                        console.log(error);
                        res({e:2,mes: "Error al obtener token de Paypal"});
                    } else {
                        openIdConnect.userinfo.get(tokeninfo.access_token, function (error, userinfo) {
                            if (error) {
                                console.log(error);
                                res({e:3,mes: "Error al obtener el usuario de Paypal"})
                            } else {
                                console.log(userinfo);
                                db.collection('users').update({_id:new ObjectID(user._id)},{"$set":{"paypal":userinfo.email}},function(err){
                                    if(err){
                                        res.json({e:2,mes:"Error al actualizar cuenta de paypal"});    
                                    }else
                                        res.json({e:0,userinfo:userinfo});
                                });
                            }
                        });
                    }
                });
            }else
                res.json({e:1,mes:"Token no encontrada"});
        });
    });

/*
router.route('/obtenerperfil/:tokenPaypal')
    .get(function(req,res){
        var tokenPaypal = req.params.tokenPaypal;
        console.log(tokenPaypal)
        // With Authorizatiion code
        openIdConnect.tokeninfo.create(tokenPaypal, function (error, tokeninfo) {
            if (error) {
                console.log(error);
                res({e:2, userinfo: "Nada"});
            } else {
                openIdConnect.userinfo.get(tokeninfo.access_token, function (error, userinfo) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log(tokeninfo);
                        console.log(userinfo);
                        req.db.collection('paypalDatos').insert({email: userinfo.email, hooked: true}, function(err, result){
                            if (err) {
                                res.json({e:1});
                            } else {
                                res.redirect('/profile/profilepay');
                            }
                        })
                    }
                });
            }
        });
    })
    */

/*router.route('/logout')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        db.collection("users").update({"bitacora.token": token, "bitacora.tokenSecret": tokenSecret},
            {"$set":{"bitacora.$.active":false}},
            function(){});
        res.json({e:0});
    });*/

router.route('/loginadmin')
	.post(function(req,res){
	        function findUsernameCallback(user){
                if(user){
                    var password = user.password,
                        bitacora = getBitacora();
					
					if(user.priv==2){
                        if(user.active!=2){
                            compareHashPass(password,req.body.password,function(isMatch){
                                if(isMatch){
                                    req.db.collection("users").update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},function(err, result){
                                        if (err) {
                                            res.json({e:3,mes:"Error al guardar token"});
                                            throw err;
                                        }else {
                                            res.json({e:0,token:bitacora.token,tokenSecret:bitacora.tokenSecret,perfil:user.perfil});
                                            console.log(user.perfil);
                                        }
                                    });
                                }else{
                                    res.json({e:5,mes:"Usuario o contraseña incorrecta"});  
                                }
                            });
                        }else
                            res.json({e:6,mes:"Acceso revocado"});
					}else
						res.json({e:4,mes:"Usuario invalido"});  
                }else{
                    res.json({
						e:1,
						mes:"Usuario o contraseña incorrecta"
					});
                }
            }
		    utils.findUsername(req.db,req.body.username,findUsernameCallback);
		  });

router.route('/login')
	.post(function(req,res){
	        function findUsernameCallback(user){
                if(user){
                    if(user.active!=2){
                        var password = user.password,
                            bitacora = getBitacora(),
                            mails = user.mails,
                            ml = mails.length,
                            confirmed = false,
                            i = 0;
                        for(;i<ml;i++){
                            if(mails[i].confirmed){
                                confirmed = true;
                                break;
                            }
                        }
                        
                        if(confirmed){
                            compareHashPass(password,req.body.password,function(isMatch){
                                if(isMatch){
                                    req.db.collection("users").update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},function(err, result){
                                        if (err) {
                                            res.json({e:3,mes:"Error al guardar token"});
                                            throw err;
                                        }else {
                                            res.json({e:0,token:bitacora.token,tokenSecret:bitacora.tokenSecret});
                                        }
                                    });
                                }else{
                                    res.json({e:2,mes:"Usuario o contraseña incorrecta"});  
                                }
                            }); 
                        }else
                            res.json({e:4,mes:"Debes confirmar tu cuenta de correo"});
                    }else
                        res.json({e:6,mes:"Acceso revocado"});
                }else{
                    res.json({
						e:1,
						mes:"Usuario o contraseña incorrecta"
					});
                }
            }
			console.log(req.body);
		    utils.findUsername(req.db,req.body.username,findUsernameCallback);
		  });                                                                                                    



router.route('/profile/:username')
	.get(function(req,res){
		utils.findUsername(req.db,req.params.username,function(user){
			if(user){
                utils.getFullUser(req.db,user._id,function(usr){
                    if(usr){
                        res.json({e:0,user:usr});
                    }else
                        res.json({e:2,mes:"Imposible recuperar usuario"});
                    
                });
			}else{
				res.json({
					e:1,
					mes:"No se encontro el usuario"
				});
			}
		});
	});

router.route('/mail/:username/:mailToken')
    .put(function(req,res){
        var username = req.params.username,
            mailToken = req.params.mailToken,
            db = req.db;
            
        db.collection('users').findOne({user:username,"mails.token":mailToken,"mails.confirmed":false},function(err,user){
            if (user) {
                db.collection('users').update({user:username,"mails.token":mailToken,"mails.confirmed":false},
                    {"$set":{"mails.$.confirmed":true},"$unset":{"mails.$.token":""}},
                    function(err){
                        if (err) {
                            res.json({e:2,"mes":"Error al actualizar"});
                            console.log(err);
                            throw err;
                        }
                        res.json({e:0});
                    });
            }else{
                res.json({e:1,mes:"Token no encontrada"});
            }
        });
    });

router.route('/forgotpassword')
	.post(function(req,res){
		var mail = req.body.mail,
            db = req.db,
            user = req.body.user;
		db.collection('users').findOne({user:user,"mails.mail":mail,"mails.confirmed":true},function(err,user){
			if(err)
				res.json({e:1,mes:"Error al recuperar usuario"});
			else if(user){
				var bitacora = getBitacora(2);
				req.db.collection("users").update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},function(err){
					if (err) {
						res.json({e:3,mes:"Error al guardar token"});
						console.log(err);
					}else {
						var mailOptions = mailer.forgotMail(user.particular.name,bitacora.token,bitacora.tokenSecret);
                            mailOptions.to = mail;
                            mailer.sendMail(mailOptions);
						res.json({e:0});
					}
				});
			}else{
				res.json({e:1,mes:"Usuario y E-mail no encontrado"});
			}
		});
    })
    .put(function(req,res){
       var newPassword = req.body.newpassword,
        token = req.body.token,
        tokenSecret = req.body.tokensecret,
        db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                getSalt(function(salt){
                    if(salt){
                        hashPass(salt,newPassword,function(hash){
                            if(hash){
                                var bitacora = getBitacora();
                                db.collection("users").update({_id:new ObjectID(user._id)},
                                    {"$set":{password:hash}},
                                    function(err){
                                        if (err) {
                                            console.log(err);
                                            res.json({e:3,mes:"Error al actualizar contraseña"});
                                        }else {
                                            var bits = user.bitacora, bl = bits.length, i=0;
                                                    
                                            for(;i<bl;i++){
                                                db.collection("users").update({_id:new ObjectID(user._id),"bitacora.token": bits[i].token, "bitacora.tokenSecret": bits[i].tokenSecret},
                                                {"$set":{"bitacora.$.active":false}},
                                                function(){});
                                            }
                                            db.collection("users").update({_id:new ObjectID(user._id)},
                                            {"$push":{bitacora:bitacora}},
                                            function(){});
                                                
                                            res.json({e:0});
                                        }
                                    });
                                    }else{
                                        res.json({e:4,mes:"Error al encriptar contraseña"});
                                    }
                                });
                            }else{
                                res.json({e:5,mes:"Error al encriptar contraseña"});
                            } 
                        });
					     
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
    });

router.route('/forgotuser')
	.post(function(req,res){
		var db = req.db,
            user = req.body.user;
		db.collection('users').findOne({user:user,"mails.confirmed":true},function(err,user){
			if(err)
				res.json({e:1,mes:"Error al recuperar usuario"});
			else if(user){
                var bitacora = getBitacora(2);
                mail = user.mails[0].mail;
				req.db.collection("users").update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},function(err, result){
					if (err) {
						res.json({e:3,mes:"Error al guardar token"});
						console.log(err);
					}else {
						var mailOptions = mailer.forgotMail(user.particular.name,bitacora.token,bitacora.tokenSecret);
                            mailOptions.to = mail;
                            mailer.sendMail(mailOptions);
						res.json({e:0});
					}
				});
			}else{
				res.json({e:1,mes:"Usuario y E-mail no encontrado"});
			}
		});
    })

router.route('/forgotemail')
	.post(function(req,res){
        var db = req.db,
        mail = req.body.mail;
		db.collection('users').findOne({"mails.mail":mail,"mails.confirmed":true},function(err,user){
			if(err)
				res.json({e:1,mes:"Error al recuperar usuario"});
			else if(user){
				var bitacora = getBitacora(2);
				req.db.collection("users").update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},function(err){
					if (err) {
						res.json({e:3,mes:"Error al guardar token"});
						console.log(err);
					}else {
						var mailOptions = mailer.forgotMail(user.particular.name,bitacora.token,bitacora.tokenSecret);
                            mailOptions.to = mail;
                            mailer.sendMail(mailOptions);
						res.json({e:0});
					}
				});
			}else{
				res.json({e:1,mes:"Usuario y E-mail no encontrado"});
			}
		});
    })

router.route('/phone')
    .get(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db;
            
        utils.checkHash(db,token,tokenSecret,function(user){
            if(user){
                res.json({e:0,phones:user.phones});
            }else
                res.json({e:1,mes:"Token Invalido"});
        });    
        
    })
    .put(function(req,res){
        var oldPhone = req.body.oldphone,
            newPhone = req.body.newphone,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                utils.pullAndPush(db.collection('users'),
					{_id:new ObjectID(user._id)},
					{
						options:{},
						statement:{"$pull":{phones:oldPhone}}
					},
					{
						options:{upsert: true, multi: true},
						statement:{"$push":{phones:newPhone}}
					},
					function(success){
						if(success){
							res.json({e:0});
                        }else{
                            res.json({e:2,mes:"Error al actualizar mail"});}
					});
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
        
        
    })
	.post(function(req,res){
		var newPhone = req.body.newphone,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
		utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                utils.pullAndPush(db.collection('users'),
					{_id:new ObjectID(user._id)},
					{
						options:{},
						statement:{"$pull":{phones:newPhone}}
					},
					{
						options:{upsert: true, multi: true},
						statement:{"$push":{phones:newPhone}}
					},
					function(success){
						if(success){
							res.json({e:0});
                        }else{
                            res.json({e:2,mes:"Error al actualizar mail"});}
					});
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
	})
	.delete(function(req,res){
		var oldPhone = req.query.oldphone,
            token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db;
            
            
		utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                db.collection('users').update({_id:new ObjectID(user._id)},
                    {"$pull":{phones:oldPhone}},
                    function(err, result){
                        if(err){
                            console.log(err);
                            res.json({e:2,mes:"Error al eliminar telefono"});
                        }
                        res.json({e:0});
                    });
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
	});
	

router.route('/createuser')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret;
        console.log(body);
        if(!body.user ||
           !body.priv ||
          (parseInt(body.priv)<1 || parseInt(body.priv)>2) ||
           !body.mail ||
           !body.password){
            res.json({e:1,mes:"Debes enviar todos los campos"});
            return;
        }
       
       
       utils.checkHash(db, token, tokenSecret, function(user){
            if(user && user.priv==2){
                
                utils.findUsername(req.db,body.user,function(user){
                    if(user){
                        res.json({
                            e:2,
                            mes:"Username ya existe"
                        });
                    }else{
                        utils.findEmail(req.db,body.mail,function(user){
                            if(user){
                                //fileUtils.deleteFile("routes/uploads/husk/"+path);
                                res.json({
                                    e:2,
                                    mes:"Email ya existe"
                                });
                            }else{
                                getSalt(function(salt){
                                    if(salt){
                                        hashPass(salt,body.password,function(pass){
                                            if(pass){
                                                var bitacora = getBitacora();
                                                var data = {
                                                    user: body.user,
                                                    password: pass,
                                                    priv:parseInt(body.priv),
                                                    perfil: new ObjectID(body.perfil),
                                                    bitacora:[bitacora],
                                                    active: 1,
                                                    mails:[
                                                        {
                                                            mail: body.mail,
                                                            confirmed: true
                                                        }
                                                        
                                                    ]
                                                };
                                                
                                                db.collection('users').insert(data,function(err,result){
                                                    if(err)
                                                        res.json({e:5,mes:"Error al guardar usuario"});
                                                    else{
                                                        res.json({e:0,id:result.insertedIds[0],token:bitacora.token,tokenSecret:bitacora.tokenSecret});
                                                        
                                                    }
                                                });
                                            }else
                                                res.json({e:4,mes:"Error al guardar contraseña"});
                                        });
                                    }else
                                        res.json({e:3,mes:"Error al guardar contraseña"});
                                });
                            }
                        });
                    }
                });
                
            }else
                res.json({e:1,mes:"Token invalido"});
        
        
        });
        
    });
	
router.route('/mail')
    .get(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db;
            
        utils.checkHash(db,token,tokenSecret,function(user){
            if(user){
                var mails = user.mails, ml = mails.length, i=0;
                for(;i<ml;i++){
                    delete mails[i].token;
                }
                res.json({e:0,mails:mails});
                
            }else
                res.json({e:1,mes:"Token Invalido"});
        });    
        
    })
    .put(function(req,res){
        var newMail = req.body.newmail,
            oldMail = req.body.oldmail,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                utils.findEmail(db,newMail,function(is){
                    if(is){
                        res.json({e:3,mes:"El E-mail que intentas agregar ya existe."});
                    }else{
                        var mailToken = hashids.encode(Date.now());
                        utils.pullAndPush(db.collection('users'),
                            {_id:new ObjectID(user._id)},
                            {
                                options:{},
                                statement:{"$pull":{mails:{mail:oldMail}}}
                            },
                            {
                                options:{upsert: true, multi: true},
                                statement:{"$push":{mails:{mail:newMail,confirmed:false,token:mailToken}}}
                            },
                            function(success){
                                if(success){
                                    var mailOptions = mailer.confirmMail(user.particular.name,user.user,mailToken);
                                        mailOptions.to = newMail;
                                        mailer.sendMail(mailOptions);
                                    res.json({e:0});
                                }else{
                                    res.json({e:2,mes:"Error al actualizar mail"});
                                }
                            });
                        }
                    }
                );
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
        
        
    })
    .delete(function(req,res){
        var oldMail = req.query.oldmail,
            token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                var mails = user.mails, ml = mails.length, i=0, confirmed=0;
                for(;i<ml;i++){
                    if(mails[i].confirmed && mails[i].mail != oldMail)
                        confirmed++;
                }
                if(confirmed>0)
                    db.collection('users').update({_id:new ObjectID(user._id)},
                        {"$pull":{mails:{mail:oldMail}}},
                        function(err, rows){
                            if(err){
                                console.log(err);
                                res.json({e:2,mes:"Error al eliminar mail"});
                            }else 
                                res.json({e:0});
                        });
                else
                    res.json({e:3,mes:"Debes tener al menos un mail confirmado"});
            }else
                res.json({e:1,mes:"Token Invalido"});
		});
    })
    .post(function(req,res){
        var newMail = req.body.newmail,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
				var mailToken = hashids.encode(Date.now());
                utils.findEmail(db,newMail,function(is){
                    if(is){
                        res.json({e:3,mes:"El E-mail que intentas agregar ya existe."});
                    }else{
                        db.collection('users').update({_id:new ObjectID(user._id)},
                        {"$push":{mails:{mail:newMail,confirmed:false,token:mailToken}}},
                        function(err){
                            if(err)
                                res.json({e:2,mes:"Error al agregarmail"});
                            else{
                                var mailOptions = mailer.confirmMail(user.particular.name,user.user,mailToken);
                                    mailOptions.to = newMail;
                                    mailer.sendMail(mailOptions);
                                res.json({e:0});
                            }
                        });
                    }
                });
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
    });

router.route('/password')
    .put(function(req,res){
       var oldPassword = req.body.oldpassword,
        newPassword = req.body.newpassword,
        token = req.body.token,
        tokenSecret = req.body.tokensecret,
        db = req.db;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
               compareHashPass(user.password,oldPassword,function(isMatch){
					if(isMatch){
                        getSalt(function(salt){
                            if(salt){
                                hashPass(salt,newPassword,function(hash){
                                    if(hash){
                                        var bitacora = getBitacora();
                                       db.collection("users").update({_id:new ObjectID(user._id)},
                                            {"$set":{password:hash}},
                                            function(err){
                                                if (err) {
                                                    console.log(err);
                                                    res.json({e:3,mes:"Error al actualizar contraseña"});
                                                }else {
                                                    //"$push":{bitacora:bitacora}     
                                                    var bits = user.bitacora, bl = bits.length, i=0;
                                                    
                                                    for(;i<bl;i++){
                                                        db.collection("users").update({_id:new ObjectID(user._id),"bitacora.token": bits[i].token, "bitacora.tokenSecret": bits[i].tokenSecret},
                                                            {"$set":{"bitacora.$.active":false}},
                                                            function(){});
                                                    }
                                                    db.collection("users").update({_id:new ObjectID(user._id)},
                                                            {"$push":{bitacora:bitacora}},
                                                            function(){});
                                                    
                                                    res.json({e:0,token:bitacora.token,tokenSecret:bitacora.tokenSecret});
                                                }
                                            });
                                    }else{
                                        res.json({e:4,mes:"Error al encriptar contraseña"});
                                    }
                                });
                            }else{
                                res.json({e:5,mes:"Error al encriptar contraseña"});
                            } 
                        });
					}else{
						res.json({e:2,mes:"Contraseña incorrecta"});  
					}
				});      
            }else
                res.json({e:1,mes:"Token Invalido"});
        });
    });


router.route('/disable/:id')
    .delete(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db,
            id = req.params.id;
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
               if(user && user.priv==2){
                    db.collection('users').update({_id:id},{"$set":{active:2,bitacora:[]}},function(err){
                        if(err)
                            res.json({e:3,mes:"Error de solicitud"});
                        else
                            res.json({e:0});
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else{
            res.json({e:1,mes:"ID invalido"});
        }
    });
    
router.route('/enable/:id')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db,
            id = req.params.id;
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
               if(user && user.priv==2){
                    db.collection('users').update({_id:id},{"$set":{active:1,bitacora:[]}},function(err){
                        if(err)
                            res.json({e:3,mes:"Error de solicitud"});
                        else
                            res.json({e:0});
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else{
            res.json({e:1,mes:"ID invalido"});
        }
    });    

router.route('/profilepic')
    .put(upload.single('profilepic'),function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        if(req.file){
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user){
                    fileUtils.saveFile([req.file],"image",function(){
                        if(!arguments[0]){
                            res.json({
                                e:5,
                                mes:"Error al guardar el archivo"
                            });
                            return;
                        }
                        var path= arguments[0][0];
                        db.collection('users').update({_id:user._id},
                            {"$set":{photo:path}},function(err){
                                if(err){
                                    res.json({e:6,mes:"Error al actualizar datos"});
                                    //fileUtils.deleteFile("routes/uploads/husk/"+path);
                                    console.log(err);    
                                }
                                //fileUtils.deleteFile("routes/uploads/husk/"+user.photo);
                                res.json({e:0,profilePic:path});
                            });
                    });
                }else
                    res.json({e:1,mes:"Token Invalido"});
            });
        }else{
            res.json({e:1,mes:"Token Invalido"});
        }    
        
    });


router.route('/store')
    .put(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        if(!req.body.storename ||
           !req.body.storeaddress ||
           !req.body.storetype){
            res.json({e:4,mes:"Debes enviar todos los campos"});
            return;
        }
        utils.checkHash(db, token, tokenSecret, function(user){
			if(user){
                if(user.store){
                    var store = {
                        name: req.body.storename,
                        address: req.body.storeaddress
                    };
                    
                    var storeTypes = ['SA','SACV','SRL','SNC','SCS'];
                    if(storeTypes.indexOf(req.body.storetype) == -1){
                        res.json({
                                e:3,
                                mes:"Tipo de tienda invalido"
                        });
                        return;
                    }
                    
                    store.type = req.body.type;
                    
                    db.collection('users').update({_id:new ObjectID(user._id)},
                    {"$set":{store:store}}, function(err){
                        if(err){
                            res.json({e:1,mes:"Error al actualizar"});
                            console.log(err);   
                        }
                        res.json({e:0,mes:"Actualizado correctamente"});
                    }); 
                    
                }else{
                    res.json({
                        e:2,
                        mes:"No puedes actualizar esto"
                    });
                }
            }else{
				res.json({
					e:1,
					mes:"Token invalida"
				});
			}
        });
    });

router.route('/paypal')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                res.json({e:0,user:user.paypal});
            }else
                res.json({e:1,mes:"Token no encontrada"});
        });
    })
    .put(function(req,res){
        var db = req.db,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            name = req.body.name,
            lastName = req.body.lastname,
            payMail = req.body.mail;
           
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                var payload = {
                    requestEnvelope: {
                        errorLanguage:  'en_US'
                    },
                    accountIdentifier:{
                      emailAddress:payMail
                    },
                    matchCriteria:"NAME",
                    firstName:name,
                    lastName: lastName
                };
                
                paypalSdk.getVerifiedStatus(payload, function(err,response){
                    if(err){
                        console.log(response);
                        res.json({e:2,mes:"Error al validar cuenta de paypal"});
                    }else if(response.accountStatus=="VERIFIED" || response.accountStatus=="UNVERIFIED"){
                        var data = {
                            name: name,
                            lastName: lastName,
                            payMail: payMail
                        };
                        db.collection('users').update({_id:new ObjectID(user._id)},{"$set":{paypal:data}},function(err){
                            if(err){
                                res.json({e:2,mes:"Error al actualizar cuenta de paypal"});    
                            }else
                                res.json({e:0});
                        });
                    }else{
						res.json({e:2,mes:"Tu cuenta de paypal no se encuentra verificada"});
					}
                });
            }else
                res.json({e:1,mes:"Token no encontrada"});
        });
    });

router.route('/canpublish')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                if(user.paypal)
                    res.json({e:0});
                else
                    res.json({e:2,mes:"Debes afiliar una cuenta de paypal para poder publicar"});
            }else
                res.json({e:1,mes:"Token invalido"});
        });
        
    });

router.route('/particular')
    .put(function(req,res){
        
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
            
        utils.checkHash(db, token, tokenSecret, function(user){
			if(user){
				var data = {};
                
                if(req.body.country && ObjectID.isValid(req.body.country))
                    data.country = new ObjectID(req.body.country);
                if(req.body.state && ObjectID.isValid(req.body.state))
                    data.state = new ObjectID(req.body.state);
                if(req.body.city && ObjectID.isValid(req.body.city))
                    data.city = new ObjectID(req.body.city);
                    
                if(req.body.postal)
                    data.postal = req.body.postal;
                    
                var particular = false;
                if(req.body.name){
                    if(!particular)
                        particular = {};
                    particular.name = req.body.name;
                }
                
                if(req.body.deliveryaddress)
                    data.deliveryAddress = req.body.deliveryaddress;
                
                if(req.body.lastname){
                    if(!particular)
                        particular = {};
                    particular.lastName = req.body.lastname;
                }
                
                if(req.body.dob){
                    if(!particular)
                        particular = {};
                    particular.DoB = req.body.dob;
                }
                
                if(particular)
                    data.particular = particular;
                
                if(Object.keys(data).length>0)
                    db.collection('users').update({_id:new ObjectID(user._id)},
                        {"$set":data}, function(err){
                            if(err){
                                res.json({e:1,mes:"Error al actualizar"});
                                console.log(err);   
                            }
                            res.json({e:0,mes:"Actualizado correctamente"});
                        });
                else
                    res.json({e:2,mes:"Nada para actualizar"});
			}else{
				res.json({
					e:1,
					mes:"Token invalida"
				});
			}
		});    
    });

router.route('/')
	.get(function(req,res){
		var token = req.query.token;
		var tokenSecret = req.query.tokensecret;
		utils.checkHash(req.db, token, tokenSecret, function(user){
			if(user){
				delete user.bitacora;
				delete user.mails;
				delete user.phones;
				delete user.type;
				delete user.priv;
				delete user.password;
				res.json({e:0,user:user});
			}else{
				res.json({
					e:1,
					mes:"Token invalida"
				});
				return;
			}
		
		});
	})
	.post(upload.single('profilepic'),function (req,res){
		//req.db.collection('users').remove({},function(){});
		var body = req.body;
        console.log(body);
		if(!body.username){
			res.json({
				e:7,
				mes:"Debes enviar el nombre de usuario"
			});
			return;
		}
		if(!body.type){
			res.json({
				e:7,
				mes:"Debes enviar el tipo"
			});
			return;
		}
		if(!body.postal){
			res.json({
				e:7,
				mes:"Debes enviar el código postal"
			});
			return;
		}
		if(!body.password){
			res.json({
				e:7,
				mes:"Debes enviar la contraseña"
			});
			return;
		}
		if(body.password.length<8){
			res.json({
				e:7,
				mes:"La contraseña debe tener al menos 8 digitos"
			});
			return;
		}
		if(!body.mail){
			res.json({
				e:7,
				mes:"Debes enviar el correo electronico"
			});
			return;
		}
		if(!body.phone){
			res.json({
				e:7,
				mes:"Debes la contraseña"
			});
			return;
		}
		if(!body.name){
			res.json({
				e:7,
				mes:"Debes enviar el nombre"
			});
			return;
		}
		if(!body.lastname){
			res.json({
				e:7,
				mes:"Debes enviar el apellido"
			});
			return;
		}
		if(!body.dob){
			res.json({
				e:7,
				mes:"Debes enviar la fecha de nacimiento"
			});
			return;
		}
		if(!body.deliveryaddress){
			res.json({
				e:7,
				mes:"Debes enviar la dirección de envios"
			});
			return;
		}
		
		if(!ObjectID.isValid(body.country) || 
			!ObjectID.isValid(body.state)){
			res.json({
				e:7,
				mes:"Debes validar el estado y pais"
			});
			return;
		}
		
		
		if(body.type<1 || body.type>2){
			res.json({
					e:7,
					mes:"Datos invaidos"
			});
			return;
		}
		
		if(body.type == 2){
			if(!body.storetype || !body.storename || !body.storeaddress){
				res.json({
						e:7,
						mes:"Debes enviar todos los campos de tienda"
				});
				return;
			}
			
			var storeTypes = ['SA','SACV','SRL','SNC','SCS'];
			if(storeTypes.indexOf(body.storetype) == -1){
				res.json({
						e:7,
						mes:"Tipo de tienda invalido"
				});
				return;
			}	
		}
        
        function saveUser(path){
            var username=	body.username.toLowerCase(),
			name	=	body.name,
			lastName=	body.lastname,
			type	=	body.type,
			DoB		=	body.dob,
			country = 	new ObjectID(body.country),
			state	=	new ObjectID(body.state),
			city	=	false,
			postal	= 	body.postal,
			phone	= 	body.phone,
			mail	=	body.mail,
			password=	body.password;
            deliveryAddress = body.deliveryaddress;
			
			if(body.city){
				if(!ObjectID.isValid(body.city)){
					res.json({
						e:7,
						mes:"Debes validar la ciudad"
					});
					return;
				}
				city = new ObjectID(body.city);
			}
			
			var countryName,stateName,cityName;
			
			if(type==2){
				var storeType = body.storetype,
				storeName = body.storename,
				storeAddress = body.storeaddress;
			}
			
			
			function hashCallback(hash){
				if(hash){
					var mailToken = hashids.encode(Date.now());
                    var bitacora = getBitacora();
					var data = {
                        user:   username,
                        type:   type,
                        priv:   1,
                        active: 1,
                        city:   city,
                        state:  state,
                        country:country,
                        postal: postal,
                        deliveryAddress:deliveryAddress,
                        phones: [
                            phone
                        ],
                        mails:   [
                            {
                                mail: mail,
                                confirmed: false,
								token: mailToken
                            }
                        ],
						photo:	path,
                        password: hash,
                        bitacora: [
                            bitacora
                        ],
						categories: [],
						createdAt:Date.now(),
						particular:{
							name: name,
							lastName: lastName,
							DoB: DoB
						}
                    };
					
					if(type==2){
						data.store = {
							name: storeName,
							address: storeAddress,
							type: storeType
						};
					}
                    
                    req.db.collection("users").insert(data,function(err, result){
                        if (err) {
                            console.log(err);
							fileUtils.deleteFile("routes/uploads/husk/"+path);
                            res.json({e:6,mes:"Error al guardar usuario"});
                        }else{
                            var mailOptions = mailer.confirmMail(name,username,mailToken);
                            mailOptions.to = mail;
                            mailer.sendMail(mailOptions);
							
                            var mailOptions2 = mailer.welcomeMail();
							mailOptions2.to = mail;
                            mailer.sendMail(mailOptions2);
								
                            var history = {
                                user : new ObjectID(result.insertedIds[0]),
                                items : [],
                                users: []
                            };
                            req.db.collection('history').insert(history,function(){});
                            
                            
                            var favorites = {
                                user : new ObjectID(result.insertedIds[0]),
                                items : [],
                                users: []
                            };
                            req.db.collection('favorites').insert(favorites,function(){});
							
							var cart = {
                                user : new ObjectID(result.insertedIds[0]),
                                items : []
                            };
                            req.db.collection('cart').insert(cart,function(){});
                            
                            res.json({e:0,id:result.insertedIds[0],token:bitacora.token,tokenSecret:bitacora.tokenSecret});
                        }
                    });
				}else{
					fileUtils.deleteFile("routes/uploads/husk/"+path);
					res.json({
						e:4,
						mes:"Error al generar contraseña"
					});
				}
			}
			
			
			function getSaltCallback(salt){
				if(salt){
					hashPass(salt,password,hashCallback);
				}else{
					fileUtils.deleteFile("routes/uploads/husk/"+path);
					res.json({
						e:3,
						mes:"Error al generar contraseña"
					});
				}
			}
			
			
			function freeEmailCallback(user){
				if(user){
					if(path.length>3)
						fileUtils.deleteFile("routes/uploads/husk/"+path);
					res.json({
						e:2,
						mes:"Email ya existe"
					});
				}else{
					getSalt(getSaltCallback);
				}
			}
			
			
			function freeUserNameCallback(user){
				if(user){
					if(path.length>3)
						fileUtils.deleteFile("routes/uploads/husk/"+path);
					res.json({
						e:1,
						mes:"Username ya existe"
					});
				}else{
					utils.findEmail(req.db,mail,freeEmailCallback);
				}
			
			}
			
			req.db.collection('countries').findOne({_id:country},function(err,cc){
				if(cc){
					countryName = cc.name;
					req.db.collection('states').findOne({_id:state},function(err,ss){
						if(ss){
							if(country.equals(new ObjectID(ss.country))){
								stateName = ss.name;
								if(city){
									req.db.collection('cities').findOne({_id:city},function(err,ci){
										if(ci){
											if(state.equals(new ObjectID(ci.state))){
												cityName = ci.name;
												utils.findUsername(req.db,username,freeUserNameCallback);
											}else{
												res.json({
													e:7,
													mes:"Ciudad no pertenece al estado seleccionado"
												});
											}
										}else{
											res.json({
												e:7,
												mes:"Ciudad invalida"
											});
										}
									});
								}else{
									cityName = "";
									utils.findUsername(req.db,username,freeUserNameCallback);
								}
							}else{
								res.json({
									e:7,
									mes:"Estado no pertenece al pais seleccionado"
								});
							}
						}else{
							res.json({
								e:7,
								mes:"Estado invalido"
							});
						}
					});
				}else{
					res.json({
						e:7,
						mes:"Pais invalido"
					});
				}
			});
        }
        
		function saveCallback(){
			if(!arguments[0]){
				res.json({
					e:5,
					mes:"Error al guardar el archivo"
				});
				return;
			}
            saveUser(arguments[0][0]);
		}
        if(req.file)
		    fileUtils.saveFile([req.file],"image",saveCallback);
        else
            saveUser("");
		
	});

module.exports = router;