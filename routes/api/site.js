var express = require('express');
var multer = require('multer');
var router = express.Router();
var utils = require('../utils/utils');
var utils = require('../utils/utils');
var fileUtils = require('../utils/fileUtils');
var upload = multer({ dest: './temp/' });
var ObjectID = require('mongoskin').ObjectID;

router.route('/banner/:id')
    .delete(function(req,res){
        var db = req.db,
            token = req.query.token,
            id = req.params.id,
            tokenSecret = req.query.tokensecret;
        if(ObjectID.isValid(id)){
            utils.checkHash(db,token,tokenSecret,function(user){
                if(user && user.priv==2){
                    db.collection('banners').remove({_id:new ObjectID(id)},function(err){
                        if(err)
                            res.json({e:1,mes:"Error al eliminar banner"});
                        else
                            res.json({e:0});
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else
            res.json({e:1,mes:"ID invalido"});
    });
    
router.route('/tax')
   .get(function(req,res){
      var db = req.db;
      db.collection('tax').findOne({"$query":{},"$orderby":{_id:-1}},function(err,tax){
         if(err){
            res.json({e:1,mes:"Error al recuperar comisión"});
         }else if(tax){
            res.json({e:0,tax:tax.tax});
         }else
            res.json({e:0,tax:0});
      });
      
   })
   .post(function(req,res){
      var token = req.body.token,
         tokenSecret = req.body.tokensecret,
         db = req.db,
         tax = parseInt(req.body.tax);
         console.log(req.body);
         utils.checkHash(db, token, tokenSecret, function(user){
            console.log(user);
            if(user && user.priv==2){
               db.collection('tax').insert({tax:tax},function(err){
                  if(err){
                     res.json({e:2,mes:"Error al insertar comisión"});   
                  }else
                     res.json({e:0});
               });
            }else{
               res.json({e:1,mes:"Token invalido"});
            }
            
         });
      
   });

router.route('/banners')
    .get(function(req,res){
        var db = req.db;
        db.collection('banners').find({}).toArray(function(err,banners){
            res.json({e:0,banners:banners});    
        });
    })
    .post(upload.single('banner'),function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        if(req.file)  
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user && user.priv==2){
                    fileUtils.saveFile([req.file],"image",function(){
                        if(!arguments[0]){
                            res.json({
                                e:3,
                                mes:"Error al guardar el archivo"
                            });
                            return;
                        }
                        var path= arguments[0][0];
                        var data = {
                            href :  req.body.href,
                            image:path
                        };
                        db.collection('banners').insert(data,function(err,result){
                            if(err){
                                res.json({e:4,mes:"Error al insertar banner"});
                                fileUtils.deleteFile("routes/uploads/husk/"+path);
                            }else
                                res.json({e:0,id:result.insertedIds[0],image:path});
                            
                        });
                    });
                }else
                    res.json({e:2,mes:"Login invalido"});
                
            });    
        else
            res.json({e:1,mes:"No se ha recibido la imagen"});
        
    });

module.exports = router;