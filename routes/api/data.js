var express = require('express');
var router = express.Router();
var multer = require('multer');
var utils = require('../utils/utils');
var fileUtils = require('../utils/fileUtils');
var ObjectID = require('mongoskin').ObjectID;

router.route("/countries")
	.get(function(req,res){
		req.db.collection("countries").find({"$query": {}, "$orderby":{name:1}}).toArray(function(err, countries){
			if(err){ console.log(err);res.json({e:1,mes:"Error al recuperar paises"});}
			else res.json({e:0,countries:countries});
		});
	})
	.post(function(req,res){
		var body = req.body;
		if(!body.name ||
		  !body.iso){
			res.json({e:1,mes:"No se han recibido todos los campos requeridos"});
			return;
		}else{
			var data = {
				name: body.name,
				iso: body.iso
			};
			req.db.collection("countries").insert(data,function(err, result){
				if (err) {
					res.json({e:2,mes:"Error al guardar pais"});
				}else {
					res.json({e:0,id:result.insertedIds[0]});
           		}
			});
		}
	});

router.route('/states/:countryid')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.countryid)){
			req.db.collection('states').find({country:new ObjectID(req.params.countryid)}).toArray(function(err, result){
				if (err) {
					res.json({e:1,mes:"Error al cargar estados"});
				}else {
					res.json({e:0,states:result});
				}
					
			});
		}else{
			res.json({e:2,mes:"ID de pais invalido"});
		}
	})
	.post(function(req,res){
		var body = req.body;
		if(!ObjectID.isValid(req.params.countryid) ||
		  !body.name){
			res.json({e:1,mes:"No se han recibido todos los campos requeridos"});
			return;
		}
	
		var data = {
				country : new ObjectID(req.params.countryid),
				name:	body.name
			};
			req.db.collection("states").insert(data,function(err, result){
				if (err) {
					res.json({e:2,mes:"Error al guardar pais"});
				}else {
					res.json({e:0,id:result.insertedIds[0]});
           		}
			});
	});

router.route('/cities/:stateid')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.stateid)){
			req.db.collection('cities').find({state:new ObjectID(req.params.stateid)}).toArray(function(err, result){
				if (err) {
					res.json({e:1,mes:"Error al cargar ciudades"});
				}else {
					res.json({e:0,cities:result});
				}		
			});
		}else{
			res.json({e:2,mes:"ID de estado invalido"});
		}
	})
	.post(function(req,res){
		var body = req.body;
		if(!ObjectID.isValid(req.params.stateid) ||
		  !body.name){
			res.json({e:1,mes:"No se han recibido todos los campos requeridos"});
			return;
		}
	
		var data = {
				state : new ObjectID(req.params.stateid),
				name:	body.name
			};
			req.db.collection("cities").insert(data,function(err, result){
				if (err) {
					res.json({e:2,mes:"Error al guardar ciudad"});
				}else {
					res.json({e:0,id:result.insertedIds[0]});
           		}
			});
	});

module.exports = router;
