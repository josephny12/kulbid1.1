var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;

router.route('/items')
    .get(function(req,res){
        var token = req.query.token,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            tokenSecret = req.query.tokensecret,
            db = req.db;
		limit = parseInt(limit);
		skip = parseInt(skip);
        utils.checkHash(db, token, tokenSecret, function(user){
            if (user) {
                db.collection('favorites').findOne({user:new ObjectID(user._id)},function(err,favs){
                        if(err){
                            console.log(err);    
                        }else if(favs){
                            var fl = favs.items.length, itemIDs = [], i=0, count = favs.items.length;
                            for(;i<fl;i++){
                                itemIDs.push(new ObjectID(favs.items[i].item));
                            }
                            itemIDs = itemIDs.splice(skip,limit);
                            utils.getFullItems(db,itemIDs,function(items){
                                utils.itemsInFavAndCart(db,user._id,items,function(itms){
                                    res.json({e:0,items:itms,total:count});
                                });
                            });
                        }else{
                             res.json({e:0,items:[],total:0});
                        }
                    });
            }else{
                res.json({e:1,mes:"Token invalido"});
            }
        });
    });

router.route('/item/:id')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            id = req.params.id,
            db = req.db;
        if(ObjectID.isValid(id)){    
			utils.checkHash(db, token, tokenSecret, function(user){
				if (user) {
					db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
						if(err){
							res.json({e:2,mes:"Articulo no encontrado"});
						}else if(item){
							if(item.active==1){
								var userObjectID = new ObjectID(user._id),
									itemUserObjectID = new ObjectID(item.user);
								if(userObjectID.equals(itemUserObjectID)){
									res.json({e:4,mes:"No puedes agregar tu propio articulo a favoritos"});
								}else{
									utils.pullAndPush(db.collection('favorites'),
										{user:new ObjectID(user._id)},
										{
											options:{},
											statement:{"$pull":{items:{item:new ObjectID(item._id)}}}
										},
										{
											options:{upsert: true, multi: true},
											statement:{"$push":{items:{"$each":[{item:new ObjectID(item._id),date: Date.now()}],$position:0}}}
										},
										function(success){
											if(success)
												res.json({e:0});
											else
												res.json({e:3,mes:"Error al agregar a favoritos"});
										}
									);
								}
							}else
								res.json({e:3,mes:"No puedes agregar un articulo expirado a favoritos"});
						}else{
							res.json({e:2,mes:"Articulo no encontrado"});
						}
					});
				}else{
					res.json({e:1,mes:"Token invalido"});
				}
			});    
		}else{
             res.json({e:4,mes:"ID invalido"});
        }
    })
    .delete(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            id = req.params.id,
            db = req.db;
        if(ObjectID.isValid(id)){  
            
			utils.checkHash(db, token, tokenSecret, function(user){
				if (user) {
					db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
						if(err){
							res.json({e:2,mes:"Articulo no encontrado"});
						}else{
							db.collection('favorites').update({user:new ObjectID(user._id)},
								{"$pull":{items:{item:new ObjectID(item._id)}}},
								function(err,rows){
									if(err){
										res.json({e:3,mes:"Error al eliminar favorito"});
										console.log(err);
									}
									else
										res.json({e:0});
								})
						}
					});
				}else{
					res.json({e:1,mes:"Token invalido"});
				}
			});
		}else{
             res.json({e:4,mes:"ID invalido"});
        }
    });

module.exports = router;