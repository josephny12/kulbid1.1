var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID;
var utils = require('../utils/utils');


router.route('/perfiles')
    .get(function(req,res){
        req.db.collection('perfilPermiso').find().toArray(function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar los perfiles"});
            } else {
                res.json({e:0,perfiles:result});
            }
        });
    })

router.route('/getperfiles')
    .get(function(req,res){
        req.db.collection('perfilPermiso').find().toArray(function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar los perfiles"});
            } else {
                res.json({e:0,perfiles:result});
                console.log(result);
            }
        })
    })

router.route('/getname/:id')
    .get(function(req,res){
        req.db.collection('perfilPermiso').findOne({_id: new ObjectID(req.params.id)}, function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar el nombre del perfil"});
            } else {
                res.json({e:0,nombrePerfil:result})
            }
        })
    })

router.route('/getpagesenabled')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                req.db.collection('permissions').find({"perfil": new ObjectID(user.perfil)}).toArray(function(err, permisos){
                    if (err) {
                        res.json({e:2,mes:"Permisos no encontrados"});
                    } else {
                        res.json({e:0,permisos:permisos});
                        console.log(permisos);
                    }
                })
            } else{
                res.json({e:1,mes:"Token no encontrada"});
                console.log("Not Ok");
            }
        })
    })

router.route('/getpage/:id')
    .get(function(req,res){
        id = parseInt(req.params.id);
        req.db.collection('pages').find({"element": id}).toArray(function(err, page){
            if (err) {
                res.json({e:2,mes:"Página no encontrada"});
            } else {
                res.json({e:0,page:page});
                
                console.log("Esta es el id de la pagina" + page[0]._id);
            }
        })
    })

router.route('/editar/:id')
	.get(function(req,res){
			req.db.collection('permissions').find({"perfil": new ObjectID(req.params.id)}).toArray(function(err, permisos){
				if (err) {
					res.json({e:2,mes:"Error al cargar el perfil"});
				}else {
                    res.json({e:0,permisos:permisos});
                    console.log(permisos);
			    }
		    });
     });

router.route('/edit/:id')
	.get(function(req,res){
			req.db.collection('permissions').aggregate([{$match: {"perfil": new ObjectID(req.params.id)}},{
                $lookup: {
                        from: "pages",
                        localField: "pagina",
                        foreignField: "_id",
                        as: "pagina"
                    }
            },{ $unwind : "$pagina" },{
                $lookup: {
                       from: "perfilPermiso",
                       localField: "perfil",
                       foreignField: "_id",
                       as: "perfil"
                     }
            },{ $unwind : "$perfil" }], function(err, permisos){
				if (err) {
					res.json({e:2,mes:"Error al cargar el perfil"});
				}else {
                    res.json({e:0,permisos:permisos});
                }
            })
    });

router.route('/delete/:id')
	.delete(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			db = req.db,
			id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			utils.checkHash(db, token, tokenSecret, function(user){
				if(user && user.priv==2){
                    db.collection('permissions').deleteMany({perfil:id},function(err){
                        if(err)
                            res.json({e:4,mes:"Error al eliminar permisos"});
                        else
                        {
                            db.collection('perfilPermiso').remove({_id:id},function(err){
                                if(err)
                                    res.json({e:3,mes:"Error al eliminar"});
                                else
                                    res.json({e:0});	
                            })
                        }
                    })
				}else
					res.json({e:2,mes:"Token invalido"});
			});
		}else{
			res.json({e:1,mes:"ID invalido"});
        }
    });
    
router.route('/createperfil')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret,
            pagescheck = body.pagescheck;
        //console.log(body);
        var myobj = [];
        if(!body.name){
            res.json({e:1,mes:"Debes enviar todos los campos"});
            return;
        }
       
       utils.checkHash(db, token, tokenSecret, function(user){
            var data = {
                name: body.name
            };
            if(user && user.priv==2){      
                db.collection('perfilPermiso').insert(data,function(err,result){
                    if(err)
                        res.json({e:5,mes:"Error al guardar usuario"});
                    else{
                        res.json({e:0,id:result.insertedIds[0]});
                        var perfil = result.ops[0]._id;
                        for (var key in pagescheck) {
                            if (pagescheck.hasOwnProperty(key)) {
                               var permisos = { perfil: perfil, pagina: key, checked: true };
                               myobj.push(permisos);
                            }
                         }
                         db.collection("permissions").insertMany(myobj, function(err, res) {
                            if (err)
                                res.json({e:6,mes:"Error al cargar permisos"});
                            else{
                                console.log("Number of documents inserted: " + res.insertedCount);
                            }
                          });
                    }
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
        
    });

router.route('/')
	.get(function(req,res){
		req.db.collection('pages').find().toArray(function(err, result){
			if (err) {
                res.json({e:1,mes:"Error al cargar paginas"});
			} else {
                res.json({e:0,pages:result});
			}	
		});
    })
module.exports = router;