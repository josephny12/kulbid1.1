var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;
var mailer = require('../utils/mailer');
var notification = require('../utils/notification');
var Hashids = require("hashids"),
    hashids = new Hashids("kulbid is great ebay is lame");
var Paypal = require('paypal-adaptive');

var paypalSdk = new Paypal({
    userId:    'josebaperezrodrigo_api1.hotmail.com',
    password:  '4GYT7LAQEE5BXJLH',
    signature: 'AAD3KozP8NzbIO4HlwmOVxfLKd60ArD2xNH-lkZBMfCqBp4LEiiV71L1',
    appId: 'APP-49E3692753674460U'
});     
    
router.route('/buy/:id')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            id = req.params.id,
            quantity = parseInt(req.body.quantity),
            db = req.db;
        
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            db.collection('items').findOne({_id:id},function(err,item){
                if(err)
                    res.json({e:2,mes:"Error al recuperar articulo"});
                else if(item){
                    if(item.type!="A"){
                        if(item.active==1){
                            if(item.time>Date.now()){
                                if(item.stock>=quantity){
                                    utils.checkHash(db, token, tokenSecret, function(buyer){
                                       if(buyer){
                                            var itemUser = new ObjectID(item.user);
                                            if(!new ObjectID(buyer._id).equals(itemUser)){
                                                db.collection('users').findOne({_id:itemUser},function(err,seller){
                                                   if(seller){
														var data = {
                                                            item: new ObjectID(item._id),
                                                            time: Date.now(),
                                                            buyer: new ObjectID(buyer._id),
                                                            seller: new ObjectID(seller._id),
                                                            ammount: quantity,
                                                            price: item.price*quantity,
                                                            kulbidTax: item.tax*quantity,
                                                            payed: false,
                                                            kulbidPayed: false,
                                                            paymentType: "A",
                                                            closed:false,
                                                            paypalTransactions: [],
                                                            rates: {
                                                                buyer: {
                                                                    status: 'P'
                                                                },
                                                                seller: {
                                                                    status: 'P'
                                                                }
                                                            }
                                                        };
                                                        
                                                        db.collection('transactions').insert(data,function(err,result){
                                                            if(err){
                                                                res.json({e:7,mes:"Fue imposible registrar la venta"});
                                                            }else{
                                                                
                                                                var sellerMails = [], buyerMails = [];
                                                                for(var i = 0 ; i < seller.mails.length ; i++)
                                                                    if(seller.mails[i].confirmed)
                                                                        sellerMails[i] = seller.mails[i].mail;
                                                                        
                                                                for(var j = 0 ; j < buyer.mails.length ; j++)
                                                                    if(buyer.mails[j].confirmed)
                                                                        buyerMails[j] = buyer.mails[j].mail;
                                                                
                                                                db.collection('items').update({_id:new ObjectID(id)},{"$inc":{stock:-quantity}},function(err,result){
                                                                    if(!err){
                                                                        if((item.stock-quantity)<=0){
                                                                            db.collection('items').update({_id:id},{"$set":{active:2}},function(){});
                                                                            var mailStock = mailer.outOfStock(seller.particular.name, item.name);
                                                                                mailStock.to = sellerMails.join(',');
                                                                                mailer.sendMail(mailStock);
                                                                            notification.outOfStock(db,item.name,seller._id);
                                                                        }
                                                                    }
                                                                });  
                                                                
                                                                
                                                                var mailPurchase = mailer.newPurchase(buyer.particular.name, item.name, item.price*quantity, quantity, seller);
                                                                    mailPurchase.to = buyerMails.join(',');
                                                                    mailer.sendMail(mailPurchase);
                                                                notification.newPurchase(db,item.name,buyer._id);
                                                                
                                                                var mailSale = mailer.newSale(seller.particular.name, item.name, item.price*quantity, quantity, buyer, item.tax*quantity);
                                                                    mailSale.to = sellerMails.join(',');
                                                                    mailer.sendMail(mailSale);
                                                                notification.newSale(db,item.name,seller._id);
                                                                    
                                                                
                                                                res.json({e:0,id:result.insertedIds[0]});
                                                            }
                                                        });
                                                    }else
                                                        res.json({e:6,mes:"Fue imposible localizar al vendedor"});
                                                });
                                            }else
                                                res.json({e:7,mes:"No puedes comprar tu propio articulo"});
                                        }else
                                            res.json({e:5,mes:"Token invalido"});
                                    });
                                }else
                                    res.json({e:4,mes:"No hay disponibilidad"});
                            }else
                                res.json({e:4,mes:"Este articulo ya ha expirado"});
                        }else
                            res.json({e:4,mes:"Solo puedes comprar articulos activos"});
                    }else
                        res.json({e:3,mes:"No puedes comprar una subasta"});
                    
                }else
                    res.json({e:2,mes:"Articulo no encontrado"});
            });
        }else
            res.json({e:1,mes:"ID invalido"});
    });
    
    
	
	
router.route('/bid/:id')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            bidSum = parseFloat(req.body.bid),
            db = req.db,
            id = req.params.id,
			type = req.body.type ? parseInt(req.body.type) : 0;
		//0 = regular bid
		//1 = max bid
		console.log(req.body);
        if(ObjectID.isValid(id)){
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user){
                    db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
                        if(err)
                            res.json({e:3,mes:"Error al encontrar item"});
                        else if(item){
                            var itemUser = new ObjectID(item.user),
                                userOID = new ObjectID(user._id);
                            if(userOID.equals(itemUser)){
                                res.json({e:4,mes:"No puedes pujar tu propio articulo"});   
                            }else{
                                if(item.type=="A"){
                                    if(item.active==1){
                                        if(item.time>Date.now()){
											utils.getHighestBid(db, item._id, function(bid){
												var currentBid = item.price;
												if(bid)
													currentBid = bid.bid;
												if(bidSum>(currentBid+5)){
													if(type==0){
															
														db.collection("maxBids").findOne({item:new ObjectID(item._id)}, function(err, maxBid){
															if(err){
																res.json({e:7,mes:"Error al recuperar puja actual"});
															}else if(maxBid){
																if(new ObjectID(maxBid.user).equals(userOID)){
																	var maxBid = {
																		user: new ObjectID(user._id),
																		bid: bidSum,
																	};
																	db.collection("maxBids").update({item: new ObjectID(item._id)},{$set:maxBid},function(err){
																		if(err){
																			res.json({e:8,mes:"Error al ajustar puja máxima"});
																		}else{
																			res.json({e:0});
																		}
																	});
																
																}else{
																	if(maxBid.bid >= bidSum){
																		var newMaxBid = bidSum;
																		for(var i = 5 ; i > 0 ; i--)
																			if((newMaxBid + i) <= maxBid.bid){
																				newMaxBid = newMaxBid + i;
																				break;
																			}
																		if(newMaxBid==bidSum){
																			//solve this mess with rate
																			utils.getUserAvg(db, maxBid.user, function(oldbuyer, oldseller){
																				utils.getUserAvg(db, userOID, function(newbuyer, newseller){
																					if(newbuyer > oldbuyer){
																						var data = {
																							user: user._id,
																							bid: bidSum,
																							time: Date.now(),
																							item: item._id,
																						};
																						db.collection('bids').insert(data,function(err,result){
																						   if(err)
																								res.json({e:7,mes:"Error al agregar puja"});
																							else{
																								res.json({e:0,id:result.insertedIds[0]});
																								db.collection("maxBids").remove({item:new ObjectID(item._id)});
																								
																								if(!user._id.equals(new ObjectID(maxBid.user))){
																									utils.getCompleteUser(db, maxBid.user, function(prevbidder){
																										var bidderMails = [];
																										for(var i = 0 ; i < prevbidder.mails.length ; i++)
																											if(prevbidder.mails[i].confirmed)
																												bidderMails.push(prevbidder.mails[i].mail);
																										var mo = mailer.overBid(prevbidder.particular.name, item.name, item.url, maxBid.bid);
																											mo.to = bidderMails.join(',');
																										mailer.sendMail(mo);
																										notification.overBid(db, item.name, bid.user._id, item.url, maxBid.bid);
																									});
																								}
																								
																							}
																						});
																					}else{
																						res.json({e:0});
																					}
																				});
																			});
																		}else{
																			var data = {
																				user: new ObjectID(maxBid.user),
																				bid: newMaxBid,
																				time: Date.now(),
																				item: item._id,
																			};
																			db.collection('bids').insert(data,function(err,result){
																			   if(err)
																					res.json({e:7,mes:"Error al agregar puja"});
																				else{
																					res.json({e:0,id:result.insertedIds[0]});
																				}
																			});
																		}
																	}else{
																		var data = {
																			user: user._id,
																			bid: bidSum,
																			time: Date.now(),
																			item: item._id,
																		};
																		db.collection('bids').insert(data,function(err,result){
																		   if(err)
																				res.json({e:7,mes:"Error al agregar puja"});
																			else{
																				res.json({e:0,id:result.insertedIds[0]});
																				db.collection("maxBids").remove({item:new ObjectID(item._id)});
																				
																				if(!user._id.equals(new ObjectID(maxBid.user))){
																					utils.getCompleteUser(db, maxBid.user, function(prevbidder){
																						var bidderMails = [];
																						for(var i = 0 ; i < prevbidder.mails.length ; i++)
																							if(prevbidder.mails[i].confirmed)
																								bidderMails.push(prevbidder.mails[i].mail);
																						var mo = mailer.overBid(prevbidder.particular.name, item.name, item.url, maxBid.bid);
																							mo.to = bidderMails.join(',');
																						mailer.sendMail(mo);
																						notification.overBid(db, item.name, bid.user._id, item.url, maxBid.bid);
																					});
																				}
																				
																			}
																		});
																	}
																
																}
															}else{
																var data = {
																	user: user._id,
																	bid: bidSum,
																	time: Date.now(),
																	item: item._id,
																};
																db.collection('bids').insert(data,function(err,result){
																   if(err)
																		res.json({e:7,mes:"Error al agregar puja"});
																	else{
																		res.json({e:0,id:result.insertedIds[0]});
																	}
																});
															}
														});
													}else{
														//this is a maxbid
														//check if there is a highermax bid
															//raise the current bid
														//set the maxbid
															//make the bid (current + up to 5)
														db.collection("maxBids").findOne({item:new ObjectID(item._id)}, function(err, maxBid){
															if(err){
																res.json({e:7,mes:"Error al recuperar puja actual"});
															}else if(maxBid){
																if(maxBid.bid >= bidSum){
																	var newMaxBid = bidSum;
																	for(var i = 5 ; i > 0 ; i--)
																		if((newMaxBid + i) <= maxBid.bid){
																			newMaxBid = newMaxBid + i;
																			break;
																		}
																	if(newMaxBid==bidSum){
																		//solve this mess with rate
																		utils.getUserAvg(db, maxBid.user, function(oldbuyer, oldseller){
																			utils.getUserAvg(db, userOID, function(newbuyer, newseller){
																				if(newbuyer > oldbuyer){
																					var maxBid = {
																						user: new ObjectID(user._id),
																						bid: bidSum,
																					};
																					db.collection("maxBids").update({item: new ObjectID(item._id)},{$set:maxBid},function(err){
																						if(err){
																							res.json({e:8,mes:"Error al ajustar puja máxima"});
																						}else{
																							var data = {
																								user: new ObjectID(user._id),
																								bid: bidSum,
																								time: Date.now(),
																								item: new ObjectID(item._id),
																							};
																							db.collection('bids').insert(data,function(err,result){
																							   if(err)
																									res.json({e:7,mes:"Error al agregar puja"});
																								else{
																									res.json({e:0,id:result.insertedIds[0]});
																									if(!user._id.equals(new ObjectID(maxBid.user))){
																										utils.getCompleteUser(db, maxBid.user, function(prevbidder){
																											var bidderMails = [];
																											for(var i = 0 ; i < prevbidder.mails.length ; i++)
																												if(prevbidder.mails[i].confirmed)
																													bidderMails.push(prevbidder.mails[i].mail);
																											var mo = mailer.overBid(prevbidder.particular.name, item.name, item.url, maxBid.bid);
																												mo.to = bidderMails.join(',');
																											mailer.sendMail(mo);
																											notification.overBid(db, item.name, bid.user._id, item.url, maxBid.bid);
																										});
																									}
																								}
																							});
																						}
																					});
																				}else{
																					res.json({e:0});
																				}
																			});
																		});
																	}else{
																		var data = {
																			user: new ObjectID(maxBid.user),
																			bid: newMaxBid,
																			time: Date.now(),
																			item: item._id,
																		};
																		db.collection('bids').insert(data,function(err,result){
																		   if(err)
																				res.json({e:7,mes:"Error al agregar puja"});
																			else{
																				res.json({e:0,id:result.insertedIds[0]});
																			}
																		});
																	}
																}else{
																	console.log("overmaxbid");
																	var nmaxBid = {
																		user: new ObjectID(user._id),
																		bid: bidSum,
																	};
																	db.collection("maxBids").update({item: new ObjectID(item._id)},{"$set":nmaxBid},function(err){
																		if(err){
																			res.json({e:8,mes:"Error al ajustar puja máxima"});
																		}else{
																			var data = {
																				user: new ObjectID(user._id),
																				bid: maxBid.bid+5,
																				time: Date.now(),
																				item: new ObjectID(item._id),
																			};
																			db.collection('bids').insert(data,function(err,result){
																			   if(err)
																					res.json({e:7,mes:"Error al agregar puja"});
																				else{
																					res.json({e:0,id:result.insertedIds[0]});
																					if(!user._id.equals(new ObjectID(maxBid.user))){
																						utils.getCompleteUser(db, maxBid.user, function(prevbidder){
																							var bidderMails = [];
																							for(var i = 0 ; i < prevbidder.mails.length ; i++)
																								if(prevbidder.mails[i].confirmed)
																									bidderMails.push(prevbidder.mails[i].mail);
																							var mo = mailer.overBid(prevbidder.particular.name, item.name, item.url, maxBid.bid);
																								mo.to = bidderMails.join(',');
																							mailer.sendMail(mo);
																							notification.overBid(db, item.name, bid.user._id, item.url, maxBid.bid);
																						});
																					}
																				}
																			});
																		}
																	});
																}
															}else{
																console.log("new maxbid");
																var nmaxBid = {
																	user: new ObjectID(user._id),
																	bid: bidSum,
																	item: new ObjectID(item._id)
																};
																db.collection("maxBids").insert(nmaxBid,function(err){
																	if(err){
																		res.json({e:8,mes:"Error al agregar puja máxima"});
																	}else{
																		var data = {
																			user: new ObjectID(user._id),
																			bid: currentBid+5,
																			time: Date.now(),
																			item: item._id,
																		};
																		db.collection('bids').insert(data,function(err,result){
																		   if(err)
																				res.json({e:9,mes:"Error al agregar puja"});
																			else{
																				res.json({e:0,id:result.insertedIds[0]});
																			}
																		});
																	}
																});
															};
														});
													}
												}else
													res.json({e:6,mes:"La puja debe ser mayor a "+(currentBid+5)});
											});
                                        }else
                                            res.json({e:5,mes:"Esta subasta ya ha expirado"});
                                    }else
                                        res.json({e:5,mes:"Solo puedes pujar en subastas activas"});
                                }else
                                    res.json({e:4,mes:"Solo puedes pujar en subastas"});
                            }
                        }else
                            res.json({e:3,mes:"Item no encontrado"});
                        
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });  
        }else
            res.json({e:1,mes:"ID invalido"});
    });
    

    
router.route('/mypurchases')
    .get(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db,
            skip = req.query.start || 0,
            limit = req.query.max || 10;
        skip = parseInt(skip);
        limit = parseInt(limit);
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                db.collection('transactions').find({"$query":{buyer:new ObjectID(user._id)},"$orderby":{time:-1}}).skip(skip).limit(limit).toArray(function(err,transactions){
                    if(err)
                        res.json({e:2,mes:"Error al encontrar transacciones"});
                    else if(transactions){
                        db.collection('items').count({buyer:new ObjectID(user._id)},function(err,count){
                            if(err){
                                res.json({e:3,mes:"Error de paginación"});
                            }else{
                                var tl = transactions.length, i = 0, rounds = 1;
                                handleLoop = function(k){
                                    utils.completeTransaction(db, transactions[k], function(t){
                                        transactions[k] = t;
                                        if(rounds == tl){
                                            res.json({e:0,transactions:transactions,total:count});   
                                        }
                                        rounds++;    
                                    });
                                };
                                if(tl>0)
                                    for(; i < tl ; i++)
                                        handleLoop(i);
                                else
                                    res.json({e:1,mes:"No se han encontrado compras"});
                            }
                        });
                    }else
                        res.json({e:1,mes:"No se han encontrado compras"});
                });
            }else{
                res.json({e:1,mes:'Token invalido'});
            }
            
        });
    });
    
router.route('/mysales')
    .get(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            db = req.db,
            skip = req.query.start || 0,
            limit = req.query.max || 10;
        skip = parseInt(skip);
        limit = parseInt(limit);
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                db.collection('transactions').find({"$query":{seller:new ObjectID(user._id)},"$orderby":{time:-1}}).skip(skip).limit(limit).toArray(function(err,transactions){
                    if(err)
                        res.json({e:2,mes:"Error al encontrar transacciones"});
                    else if(transactions){
                        db.collection('items').count({seller:new ObjectID(user._id)},function(err,count){
                            if(err){
                                res.json({e:3,mes:"Error de paginación"});
                            }else{
                                var tl = transactions.length, i = 0, rounds = 1;
                                handleLoop = function(k){
                                    utils.completeTransaction(db, transactions[k], function(t){
                                        transactions[k] = t;
                                        if(rounds == tl){
                                            res.json({e:0,transactions:transactions,total:count});   
                                        }
                                        rounds++;    
                                    });
                                };
                                if(tl>0)
                                    for(; i < tl ; i++)
                                        handleLoop(i);
                                else
                                    res.json({e:1,mes:"No se han encontrado ventas"});
                            }
                        });
                    }else
                        res.json({e:1,mes:"No se han encontrado ventas"});
                });
            }else{
                res.json({e:1,mes:'Token invalido'});
            }
            
        });
    });
    
router.route('/highestbid/:id')
    .get(function(req,res){
        var db = req.db,
            id = req.params.id;
		
        if(ObjectID.isValid(id)){
			db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
				if(err){
					res.json({e:2,mes:"Error al recuperar articulo"});
				}else if(item){
					if(item.type=='A'){
						utils.getHighestBid(db, item._id, function(bid){
							if(bid)
								res.json({e:0,bid:bid.bid});
							else
								res.json({e:0,bid:item.price});
						});
					}else{
						res.json({e:3,mes:"Articulo invalido"});
					}
				
				}else{
					res.json({e:2,mes:"Articulo no encontrado"});
				}
			
			
			});
		}else{
			res.json({e:1,mes:'ID invalido'});
		}
    });

router.route('/pay/:id')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret,
            id = req.params.id;
			
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db,token,tokenSecret, function(user){
                if(user){
                    db.collection('transactions').findOne({_id:id,"$or":[{seller:new ObjectID(user._id)},{buyer:new ObjectID(user._id)}]},function(err,transaction){
                        if(err)
                            res.json({e:3,mes:"Error al recuperar transacción"});
                        else if(transaction){
							if(transaction.rates.buyer.status=='P'){
								if(!transaction.payed){
									if(new ObjectID(user._id).equals(new ObjectID(transaction.buyer))){
										var seller = new ObjectID(transaction.seller);
										db.collection('users').findOne({_id:seller},function(err,seller){
											if(err)
												res.json({e:5,mes:"Error al recuperar información del vendedor"});
											else if(seller){
												if(seller.paypal !== undefined){
													var successID = hashids.encode(Date.now()),
														failID = hashids.encode(Date.now()-30000),
														payload = {
														requestEnvelope: {
															errorLanguage:  'en_US'
														},
														actionType:     'PAY',
														currencyCode:   'MXN',
														feesPayer:      'EACHRECEIVER',
														memo:           'Compra en Kulbid',
														cancelUrl:      'https://kulbid.com/transaction/'+id+'/payment/'+failID,
														returnUrl:      'https://kulbid.com/transaction/'+id+'/payment/'+successID,
														receiverList: {
															receiver: [
																{
																	email:  seller.paypal,
																	amount: transaction.price,
																	primary:'true'
																},
																{
																	email:  'josebaperezrodrigo@hotmail.com',
																	amount: transaction.kulbidTax,
																	primary:'false'
																}
															]
														}
													};
													console.log(payload);
													paypalSdk.pay(payload, function (err, response) {
														if (err) {
														   console.log(response);
														   res.json({e:7,mes:"Error al redirigir a paypal"});
														}else {
															var payT = {
																payKey:response.payKey,
																sellerCut: transaction.price,
																kulbidCut: transaction.kulbidTax+4,
																successID: successID,
																failID: failID,
																url: response.paymentApprovalUrl,
																completed:false,
																status:'P'
															};
															db.collection('transactions').update({_id:id},{"$push":{paypalTransactions:payT}},function(err){
																if(err){
																	res.json({e:8,mes:"Error al generar enlace con Paypal"});    
																}else{
																	
																	var d = {
																		transaction : id,
																		payKey : response.payKey,
																		paystatus:successID,
																		time: Date.now()
																	};
																	db.collection('transactionsToCheck').insert(d);
																	
																	res.json({e:0,url:response.paymentApprovalUrl});
																}
																
															});
														}
													});
												}else{
													var sellerMails = [];
													for(i = 0 ; i < seller.mails.length ; i++){
														if(seller.mails[i].confirmed)
															sellerMails.push(seller.mails[i].mail);
													}
													db.collection("items").findOne({_id:new ObjectID(transaction.item)}, function(err, item){
														if(err)
															console.log(err);
														else if(item){
															db.collection("users").findOne({_id:new ObjectID(transaction.buyer)},function(err, buyer){
																if(err)
																	console.log(err);
																else if(buyer){
																
																	var mailOptions = mailer.noPayPal(seller.particular.name, buyer.user, item.name);
																		mailOptions.to = sellerMails.join(',');
																		mailer.sendMail(mailOptions);
																
																	res.json({e:6,mes:"Este usuario no acepta pagos en paypal"});
																
																}
															
															});
														}
													
													});
													
												}
											}else
												res.json({e:5,mes:"Vendedor no encontrado"});
										});
									}else{
										res.json({e:4,mes:"No puedes completar esta acción"});
									}
								}else
									res.json({e:3,mes:"Esta transacción ya ha sido pagada"});
							}else{
								console.log(transaction.rates.buyer.status);
								switch(transaction.rates.buyer.status){
									case "C":
										res.json({e:3,mes:"No puedes pagar una transacción que ya has calificado como cancelada"});
										break;
									case "S":
										res.json({e:3,mes:"No puedes pagar una transacción que ya has calificado como exitosa"});
										break;
									case "F":
										res.json({e:3,mes:"No puedes pagar una transacción que ya has calificado como fallida"});
										break;
									default:
										res.json({e:3,mes:"No puedes pagar una transacción que ya has calificado"});
										break;
								}
							}
                        }else
                            res.json({e:3,mes:"Transaccón no encontrada"});
                    });
                }else{
                    res.json({e:2,mes:"Token invalida"});
                } 
                
            });   
            
        }else
            res.json({e:1,mes:"ID invalida"});
        
    });

router.route('/status/:transaction/:paystatus')
    .post(function(req,res){
        var db = req.db,
            transaction = req.params.transaction,
            paystatus = req.params.paystatus;
        if(ObjectID.isValid(transaction)){
            transaction = new ObjectID(transaction);
            db.collection('transactions').findOne({_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},{"paypalTransactions.$":1,_id:0},function(err,trans){
                if(err){
                    res.json({e:2,mes:"Error al recuperar transacción"});
                }else if(trans && trans.length>0){
                    
                    var params = {
                        payKey: trans[0].payKey  
                    };
                    paypalSdk.paymentDetails(params, function (err, response) {
                        if (err) {
                            res.json('Error al validar pago.');
                        } else {
                            //var d;
                            switch(response.status){
                                case "COMPLETED":
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"S"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    db.collection('transactions').update(
                                        {_id:transaction},
                                        {"$set":{kulbidPayed:true,paymentType:'P',payed:true}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    
                                    utils.completeTransaction(db,trans,function(trns){
                                        
                                        var buyerMails = [],
                                            sellerMails = [];
                                        for(var i = 0 ; i < trns.buyer.mails.length ; i++){
                                            if(trns.buyer.mails[i].confirmed)
                                                buyerMails.push(trns.buyer.mails[i].mail);
                                        }
                                        
                                        for(i = 0 ; i < trns.seller.mails.length ; i++){
                                            if(trns.seller.mails[i].confirmed)
                                                sellerMails.push(trns.seller.mails[i].mail);
                                        }
                                        
                                        var mailOptions = mailer.productPayed(
                                            trns.seller.particular.name,
                                            trns.item.name,
                                            transaction,
                                            trns.buyer.user);
                                            mailOptions.to = sellerMails.join(',');
                                            mailer.sendMail(mailOptions);
                                            
                                        var mailOptions2 = mailer.paymentProcesed(
                                            trns.buyer.particular.name,
                                            trns.item.name,
                                            transaction);
                                            mailOptions2.to = buyerMails.join(',');
                                            mailer.sendMail(mailOptions2);
                                            
                                        notification.paymentProcesed(db, trns.buyer.user, transaction, trns.buyer._id);
                                        notification.productPayed(db, trns.buyer, trns.item.name, transaction, trns.seller._id);
                                    });
                                    res.json({e:0});
                                    break;
                                case "PROCESING":
                                    /*d = {
                                        transaction : transaction,
                                        payKey : trans[0].payKey,
                                        paystatus:paystatus,
                                        time: Date.now()
                                    };
                                    db.collection('transactionsToCheck').insert(d);*/
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"PR"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    res.json({e:5,mes:"Pago pendiente, te informaremos cuando finalice"});
                                    break;
                                case "PENDING":
                                    /*d = {
                                        transaction : transaction,
                                        payKey : trans[0].payKey,
                                        paystatus:paystatus,
                                        time: Date.now()
                                    };
                                    db.collection('transactionsToCheck').insert(d);*/
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"PR"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    res.json({e:5,mes:"Pago pendiente, te informaremos cuando finalice"});
                                    break;
                                case "INCOMPLETE":
                                    /*d = {
                                        transaction : transaction,
                                        payKey : trans[0].payKey,
                                        paystatus:paystatus,
                                        time: Date.now()
                                    };
                                    db.collection('transactionsToCheck').insert(d);*/
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"PR"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    res.json({e:5,mes:"Pago pendiente, te informaremos cuando finalice"});
                                    break;
                                case "ERROR":
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"F"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    res.json({e:5,mes:"Error al procesar pago"});
                                    break;
                                case "CANCELED":
                                    db.collection('transactions').update(
                                        {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                                        {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.status":"F"}},
                                        function(err){
                                            if(err) console.log(err);
                                        }
                                    );
                                    res.json({e:5,mes:"Pago cancelado por el usuario"});
                                    break;
                            }
                        }
                    });
                    
                    
                    /*if(trans[0].successID==paystatus){
                        
                    }else{
                        res.json({e:4,mes:"Transacción fallida"});
                        db.collection('transactions').update(
                            {_id:transaction,"$or":[{"paypalTransactions.successID":paystatus},{"paypalTransactions.failID":paystatus}]},
                            {"$set":{"paypalTransactions.$.completed":true,"paypalTransactions.$.stats":"F"}},
                            function(err){
                                if(err) console.log(err);
                                res.json({e:4,mes:"Error al actualizar"});
                            }
                        );
                    }*/
                    
                }else res.json({e:3,mes:"Transacción no encontrada"});
                
                
            });
        }else
            res.json({e:1,mes:"ID de transacción invalido"});
        
    });

router.route('/close/:id')
    .post(function(req,res){
        var db = req.db,
            token = req.body.token,
            tokenSecret = req.body.tokensecret,
            id = req.params.id;
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user && user.priv==2){
                    db.collection("transactions").update({_id:id},{"$set":{closed:true}},function(err){
                        if(err)
                            res.json({e:3,mes:"Error al cerrar reportes"});
                        else
                            res.json({e:0});
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else
            res.json({e:1,mes:"Token invalido"});
    });

router.route('/rate/:id')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            id = req.params.id,
            db = req.db;
            
        if(!req.body.status ||
           !req.body.comment  ||
           !req.body.communication ||
           !req.body.avialability||
           !req.body.quality){
            res.json({e:1,mes:"No se han recibido los parametros necesarios"});
            return;
        }
        var comment = req.body.comment;
        if(comment.length>280){
            res.json({e:1,mes:"Comentario muy largo"});
            return;
        }
        
        var statuses = ['S','C','F'], status = req.body.status;
        if(statuses.indexOf(status)<0){
            res.json({e:1,mes:"Status invalido"});
            return;
        }
        
        var avialability = req.body.avialability,
            communication= req.body.communication,
            quality = req.body.quality;
            
        if(avialability<0 || avialability>4 ||
           communication<0 || communication>4 ||
           quality<0 || quality>4){
            res.json({e:1,mes:"Calificación invalida"});
            return;
        }
        
        if(status == 'F'){
            avialability = 0;
            communication = 0;
            quality = 0;
        }
        
        
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db,token,tokenSecret, function(user){
                if(user){
                    db.collection('transactions').findOne({_id:id,"$or":[{seller:new ObjectID(user._id)},{buyer:new ObjectID(user._id)}]},function(err,transaction){
                        if(err)
                            res.json({e:3,mes:"Error al encontrar transacción"});
                        else if(transaction){
                            if(!transaction.payed && status == 'S'){
                                res.json({e:3,mes:"La transacción debe ser pagada antes de poder calificar como exitosa"});
                                return;
                            }
                                var change = false;
                                if(new ObjectID(user._id).equals(new ObjectID(transaction.seller))){
                                    if(transaction.rates.seller.status!="P"){
                                        res.json({e:4,mes:"No puedes aleterar esta calificación"});
                                        return;
                                    }    
                                    change = "S";
                                }else if(new ObjectID(user._id).equals(new ObjectID(transaction.buyer))){
                                    if(transaction.rates.buyer.status!="P"){
                                        res.json({e:4,mes:"No puedes alterar esta calificación"});
                                        return;
                                    }    
                                    change = "B";
                                }else{
                                    res.json({e:4,mes:"No tienes permisos para ver esta transacción"});
                                    return;
                                }
                                
                                var data = {
                                    status : status,
                                    comment: comment,
                                    rate: {
                                        communication: parseInt(communication),
                                        avialability: parseInt(avialability),
                                        quality: parseInt(quality)
                                    },
                                    time: Date.now() 
                                };
                                
                                if(change=="B")
                                    data = {buyer:data,seller:transaction.rates.seller};
                                else
                                    data = {seller:data,buyer:transaction.rates.buyer};
                                    
                                    
                                
                                db.collection('transactions').update({_id:new ObjectID(id)},{"$set":{rates:data}},function(err,rows){
                                    if(err){
                                        res.json({e:5,mes:"Imposible guardar calificación"});
                                    }else{
										var notID;
										if(change=="B"){
											//notificar vendedor
											notID = new ObjectID(transaction.seller);
										}else{
											//notificar comprador
											notID = new ObjectID(transaction.buyer);
										}
										
										db.collection("users").findOne({_id:notID},function(err,user){
											if(!err && user){
												var mails = [], i = 0;
												for(; i < user.mails.length ; i++)
													if(user.mails[i].confirmed)
														mails.push(user.mails[i].mail);
												var statuses = "";
												switch(status){
													case 'C':
														statuses = "Cancelada";
														break;
													case 'F':
														statuses = "Fallida";
														break;
													case 'S':
														statuses = "Exitosa";
														break;
												}
												db.collection("items").findOne({_id:new ObjectID(transaction.item)}, function(err, item){
													var mailOptions = mailer.itemRated(user.particular.name, item.name, id, statuses);
													mailOptions.to = mails.join(',');
													mailer.sendMail(mailOptions);
													notification.itemRated(db, item.name, id, notID );
												});
											
											};
										});
										
                                        res.json({e:0});
                                    }
                                    
                                });
                            
                        }else
                            res.json({e:3,mes:"Transacción no encontrada"});
                        
                        
                    });
                }else
                    res.json({e:2,mes:"Token invalida"});
            
            });
        }else
            res.json({e:1,mes:"ID invalida"});
        
    });

router.route('/:id')
    .get(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            id = req.params.id,
            db = req.db;
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db,token,tokenSecret, function(user){
                if(user){
					//,"$or":[{seller:new ObjectID(user._id)},{buyer:new ObjectID(user._id)}]
                    db.collection('transactions').findOne({_id:id},function(err,transaction){
                        if(err)
                            res.json({e:3,mes:"Error al recuperar transacción"});
                        else if(transaction){
                            if(new ObjectID(user._id).equals(new ObjectID(transaction.seller)))
                                transaction.youAre = "S";
                            else if(new ObjectID(user._id).equals(new ObjectID(transaction.buyer)))
                                transaction.youAre = "B";
                            else if(user.priv==2){
                                transaction.youAre = "A";
                            }else{
                                res.json({e:4,mes:"No tienes permisos para ver esta transacción"});
                                return;
                            }
                            utils.completeTransaction(db,transaction,function(trans){
                                res.json({e:0,transaction:trans});
                            });
                        
                        }else
                            res.json({e:3,mes:"Transaccón no encontrada"});
                    });
                }else{
                    res.json({e:2,mes:"Token invalida"});
                } 
                
            });   
            
        }else
            res.json({e:1,mes:"ID invalida"});
    });
	
router.route('/')
	.get(function(req,res){
		var db = req.db,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
			token = req.query.token,
			tokenSecret = req.query.tokensecret;
		limit = parseInt(limit);
		skip = parseInt(skip);
		
		utils.checkHash(db,token,tokenSecret, function(user){
			if(user && user.priv==2){
				db.collection('transactions').find({"$query":{},"$orderby":{_id:-1}}).skip(skip).limit(limit).toArray(function(err, transactions){
					if (err) {
						res.json({e:1});
					}else{
						db.collection('transactions').count({},function(err,count){
							if(err){
								res.json({e:2,mes:"Error páginando resultados"});
								console.log(err);
							}else{
								var tl = transactions.length, i = 0, rounds = 1;
                                handleLoop = function(k){
                                    utils.completeTransaction(db, transactions[k], function(t){
                                        transactions[k] = t;
                                        if(rounds == tl){
                                            res.json({e:0,transactions:transactions,total:count});   
                                        }
                                        rounds++;    
                                    });
                                };
                                if(tl>0)
                                    for(; i < tl ; i++)
                                        handleLoop(i);
                                else
                                    res.json({e:1,mes:"No se han encontrado transacciones"});
							}
						});
					}
				});
			}else
				res.json({e:1,mes:"Token invalido"});
		
		});
		
		
	});
 
module.exports = router;   