var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;

router.route('/items')
    .get(function(req,res){
        var token = req.query.token,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            tokenSecret = req.query.tokensecret,
            db = req.db;
		limit = parseInt(limit);
		skip = parseInt(skip);
        utils.checkHash(db, token, tokenSecret, function(user){
            if (user) {
                db.collection('history').findOne({user:new ObjectID(user._id)},function(err,hist){
                    if(err){
                        console.log(err);    
                    }else if(hist){
                        var fl = hist.items.length, itemIDs = [], i=0, count = hist.items.length;
                        for(;i<fl;i++){
                           itemIDs.push(new ObjectID(hist.items[i].item));
                        }
                        itemIDs = itemIDs.splice(skip,limit);
                        utils.getFullItems(db,itemIDs,function(items){
                            utils.itemsInFavAndCart(db,user._id,items,function(itms){
                                res.json({e:0,items:itms,total:count});
                            });
                        });
                    }else{
                        res.json({e:0,items:[],total:0});
                    }
                });
            }else{
                res.json({e:1,mes:"Token invalido"});
            }
        });
    });

module.exports = router;