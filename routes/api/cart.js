var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;
var mailer = require('../utils/mailer');
var notification = require('../utils/notification');
var Hashids = require("hashids"),
    hashids = new Hashids("kulbid is great ebay is lame");

router.route('/')
    .get(function(req,res){
        var token = req.query.token,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            tokenSecret = req.query.tokensecret,
            db = req.db;
            
            utils.checkHash(db, token, tokenSecret, function(user){
                if (user) {
                    db.collection('cart').findOne({user:new ObjectID(user._id)},function(err,cart){
                        if(err){
                            console.log(err);    
                        }else if(cart){
                            var fl = cart.items.length, itemIDs = [], i=0, count = cart.items.length;
                            for(;i<fl;i++){
                               itemIDs.push(new ObjectID(cart.items[i].item));
                            }
                            itemIDs = itemIDs.splice(skip,limit);
                            utils.getFullItems(db,itemIDs,function(items){
                                 utils.itemsInFavAndCart(db,user._id,items,function(itms){
                                    res.json({e:0,items:itms,total:count});
                                });
                            });
                        }else{
                            res.json({e:0,items:[],total:0});
                        }
                    });
                }else{
                    res.json({e:1,mes:"Token invalido"});
                }
            });    
            
    });
    
    
router.route('/buyall')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db;
        utils.checkHash(db, token, tokenSecret, function(buyer){
            if (buyer) {
                db.collection('cart').findOne({user:new ObjectID(buyer._id)},function(err,cart){
                    if(err){
                        console.log(err);    
                    }else if(cart){
                        var fl = cart.items.length, itemIDs = [], i=0;
                        for(;i<fl;i++){
                           itemIDs.push(new ObjectID(cart.items[i].item));
                        }
                        utils.getFullItems(db,itemIDs,function(items){
                            var handleLoop = function(item){
                                quantity = 1;
                                if(item.type!="A"){
                                    if(item.active==1){
                                        if(item.time>Date.now()){
                                            if(item.stock>=quantity){
                                                var itemUser = new ObjectID(item.user._id);
                                                if(!new ObjectID(buyer._id).equals(itemUser)){
                                                    db.collection('users').findOne({_id:itemUser},function(err,seller){
                                                    if(seller){
                                                        var data = {
                                                            item: new ObjectID(item._id),
                                                            time: Date.now(),
                                                            buyer: new ObjectID(buyer._id),
                                                            seller: new ObjectID(seller._id),
                                                            ammount: quantity,
                                                            price: item.price*quantity,
                                                            kulbidTax: item.tax*quantity,
                                                            payed: false,
                                                            kulbidPayed: false,
                                                            paymentType: "A",
                                                            closed:false,
                                                            paypalTransactions: [],
                                                            rates: {
                                                                buyer: {
                                                                    status: 'P'
                                                                },
                                                                seller: {
                                                                    status: 'P'
                                                                }
                                                            }
                                                        };
                                                        var id = new ObjectID(item._id);
                                                        db.collection('transactions').insert(data,function(err,result){
                                                            if(err){
                                                                res.json({e:7,mes:"Fue imposible registrar la venta"});
                                                            }else{    
                                                                db.collection('items').update({_id:id},{"$inc":{stock:-quantity}},function(err,result){
                                                                    console.log(err);
                                                                    console.log(result);
                                                                    if(!err){
                                                                        if((item.stock-quantity)<=0){
                                                                            db.collection('items').update({_id:id},{"$set":{active:2}},function(){});
                                                                            var mailStock = mailer.outOfStock(seller.particular.name, item.name);
                                                                                mailStock.from = '"Registro Kulbid" <registro@kulbid.com>';
                                                                                mailStock.to = sellerMails.join(',');
                                                                                mailer.sendMail(mailStock);
                                                                            notification.outOfStock(db,item.name,seller._id);
                                                                        }
                                                                    }
                                                                });    
                                                                
                                                                var sellerMails = [], buyerMails = [];
                                                                for(var i = 0 ; i < seller.mails.length ; i++)
                                                                    if(seller.mails[i].confirmed)
                                                                        sellerMails[i] = seller.mails[i].mail;
                                                                            
                                                                            
                                                                for(var j = 0 ; j < buyer.mails.length ; j++)
                                                                    if(buyer.mails[j].confirmed)
                                                                        buyerMails[j] = buyer.mails[j].mail;
                                                                            
                                                                var mailPurchase = mailer.newPurchase(buyer.particular.name, item.name, item.price*quantity, quantity, seller);
                                                                    mailPurchase.to = buyerMails.join(',');
                                                                    mailer.sendMail(mailPurchase);
                                                                notification.newPurchase(db,item.name,buyer._id);
                                                                                
                                                                var mailSale = mailer.newSale(seller.particular.name, item.name, item.price*quantity, quantity, buyer, item.tax*quantity);
                                                                    mailSale.to = sellerMails.join(',');
                                                                    mailer.sendMail(mailSale);
                                                                notification.newSale(db,item.name,seller._id);
                                                                                
                                                                            
                                                                
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                    
                                            }
                                        }
                                    }
                                }
                            };
                            
                            for(var j = 0; j < items.length ; j++)
                                handleLoop(items[j]);
                            
                            db.collection('cart').update({user:new ObjectID(buyer._id)},{"$set":{items:[]}},function(err){
                                if(err)
                                    console.log(err); 
                            });
                            res.json({e:0});
                        });
                    }else{
                        res.json({e:2,mes:"No se encontraron items en el carrito"});
                    }
                });
            }else{
                res.json({e:1,mes:"Token invalido"});    
            }        
        });
    });
    
router.route('/:id')
    .delete(function(req,res){
        var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            id = req.params.id,
            db = req.db;
            
        if(ObjectID.isValid(id)){
            utils.checkHash(db, token, tokenSecret, function(user){
                if (user) {
                    db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
                        if(err){
                            res.json({e:2,mes:"Articulo no encontrado"});
                        }else if(item){
							if(item.type!="A"){
								db.collection('cart').update({user:new ObjectID(user._id)},
									{"$pull":{items:{item:new ObjectID(item._id)}}},
									function(err,rows){
										if(err){
											res.json({e:3,mes:"Error al agregar al carrito de compras"});
											console.log(err);
										}
										else
											res.json({e:0});
									});
							}else
								res.json({e:4,mes:"No puedes agregar una subasta al carrito de compras"});
                        }else{
                            res.json({e:2,mes:"Articulo no encontrado"});
						}
                    });
                }else{
                    res.json({e:1,mes:"Token invalido"});
                }
            }); 
        }else{
             res.json({e:4,mes:"ID invalido"});
        }
    })
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            id = req.params.id,
            db = req.db;
        if(ObjectID.isValid(id)){
            utils.checkHash(db, token, tokenSecret, function(user){
                if (user) {
                    db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
                        if(err){
                            res.json({e:2,mes:"Articulo no encontrado"});
                        }else if(item){
							if(item.active==1){
								var userObjectID = new ObjectID(user._id),
									itemUserObjectID = new ObjectID(item.user);
									
								if(userObjectID.equals(itemUserObjectID)){
									res.json({e:4,mes:"No puedes agregar tu propio articulo al carrito de compras"});
								}else{
									utils.pullAndPush(db.collection('cart'),
										{user:new ObjectID(user._id)},
										{
											options:{},
											statement:{"$pull":{items:{item:new ObjectID(item._id)}}}
										},
										{
											options:{upsert: true, multi: true},
											statement:{"$push":{items:{"$each":[{item:new ObjectID(item._id),date: Date.now()}],$position:0}}}
										},
										function(success){
											if(success)
												res.json({e:0});
											else
												res.json({e:3,mes:"Error al agregar al carrito de compras"});
										}
									);
								}
							}else
								res.json({e:3,mes:"No puedes agregar un articulo expirado al carrito de compras"});
                        }else{
                            res.json({e:2,mes:"Articulo no encontrado"});
						}
                    });
                }else{
                    res.json({e:1,mes:"Token invalido"});
                }
            }); 
        }else{
             res.json({e:4,mes:"ID invalido"});
        } 
    });

module.exports = router;