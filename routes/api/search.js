var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;

var milesToRadian = function(miles){
    var earthRadiusInMiles = 3959;
    return miles / earthRadiusInMiles;
};

router.route('/stores')
	.get(function(req,res){
		var db = req.db,
			category = req.query.category,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
			query = {type:"2"};
		limit = parseInt(limit);
		skip = parseInt(skip);
		
		if(ObjectID.isValid(category)){
			query.categories=new ObjectID(category);
		}
		
		db.collection('users').find({"$query":query,"$orderby":{_id:-1}},{bitacora:0,password:0,mails:0,phones:0,particular:0,priv:0}).skip(skip).limit(limit).toArray(function(err, users){
			if (err) {
				res.json({e:1});
            }else{
				db.collection('users').count(query,function(err,count){
					if(err){
						res.json({e:2,mes:"Error páginando resultados"});
						console.log(err);
					}else{
						var ul = users.length, i=0, rounds = 1;
						var handleLoop = function(k){
							utils.giveLocationToUser(db,users[k],function(data){
								users[k] = data;
								utils.getUserRate(db,users[k]._id,function(rate){
										users[k].rate = rate;     
										if(rounds == ul){
											res.json({e:0,users:users,total:count});
										}else
											rounds++;
								});
							});
						};
						
						
						if(ul>0){
							for(;i<ul;i++)
								handleLoop(i);
						}else
							res.json({e:0,users:users,total:count});
					}
				});
			}
		});
	});



router.route('/users')
	.get(function(req,res){
		var db = req.db,
			token = req.query.token,
			tokenSecret = req.query.tokensecret,
			term = req.query.term,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
			query = {};
			
		utils.checkHash(db, token, tokenSecret, function(user){
			if(user && user.priv==2){
				limit = parseInt(limit);
				skip = parseInt(skip);
				if (term && term.length>0) {
					var regex = term;
					query.$or = [{user:{"$regex":new RegExp(regex,'i')}},
								 {"mails.mail":{"$regex":new RegExp(regex,'i')}}];
				}	
				db.collection('users').find({"$query":query,"$orderby":{createdAt:-1}},{password:0,bitacora:0}).skip(skip).limit(limit).toArray(function(err, users) {
					if (err) {
						res.json({e:1});
					}else{
						db.collection('users').count(query,function(err,count){
							if(err){
								res.json({e:2,mes:"Error páginando resultados"});
								console.log(err);
							}else{
								res.json({e:0,users:users,total:count});
							}
						});
					}
				});
			}else{
				res.json({e:1,mes:"Token invalido"});
			}
		});
	});


router.route('/admin/item')
	.get(function(req,res){
		var db = req.db,
			token = req.query.token,
			tokenSecret = req.query.tokensecret,
			term = req.query.term,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
			type = req.query.type || false,
			status = req.query.status || false,
			active = req.query.active,
			query = {};
		utils.checkHash(db,token, tokenSecret, function(user){
			if(user && user.priv==2){
				limit = parseInt(limit);
				skip = parseInt(skip);
				if(active){
					active = parseInt(active);
					query.active = active;
				}
				if (term && term.length>0) {
					var regex = term;
					query.$or = [{name:{"$regex":new RegExp(regex,'i')}},
								 {descriptionLong:{"$regex":new RegExp(regex,'i')}},
								 {descriptionShort:{"$regex":new RegExp(regex,'i')}}];
				}
				var statuses = ['N','U','S','SN'], types = ['S','A','L'];
			
				if(statuses.indexOf(status)>-1)
					query.status=status;
				
				if(types.indexOf(type)>-1)
					query.type=type;
				
				db.collection('items').find({"$query":query,"$orderby":{createdAt:-1}}).skip(skip).limit(limit).toArray(function(err, items) {
					if (err) {
						res.json({e:1});
					}else{
						db.collection('items').count(query,function(err,count){
							if(err){
								res.json({e:2,mes:"Error páginando resultados"});
								console.log(err);
							}else{
								utils.giveUsersToItems(items,db,function(itms){
									utils.giveCategoryToItems(db,itms,function(ims){
										res.json({e:0,items:ims,total:count});
									});
								});
							}
						});
					}
				});
				
				
			}else{
				res.json({e:1,mes:"Token invalido"});
			}
		});	
			
		
		
		
		
		
	});


router.route('/item')
	.get(function(req,res){
		var db = req.db,
			term = req.query.term,
			category = req.query.category,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
			lat = req.query.lat || false,
			lng = req.query.lng || false,
			maxPrice = req.query.maxprice || false,
			minPrice = req.query.minprice || false,
			type = req.query.type || false,
			status = req.query.status || false,
			query = {
					stock:{
							"$gt":0
						},
					active:1,
					time:{
							"$gt":Date.now()
						}
					};
		limit = parseInt(limit);
		skip = parseInt(skip);
		
		if(lat && lng){
			lat = parseFloat(lat);
			lng = parseFloat(lng);
			query.position = {
				"$geoWithin": {
					"$centerSphere" : [[lng,lat],milesToRadian(5)]
				}
			};
		
		}
		
		
		if(ObjectID.isValid(category)){
			query.category=new ObjectID(category);
		}
		if (term && term.length>0) {
			var regex = term;
			query.$or = [{name:{"$regex":new RegExp(regex,'i')}},
						 {descriptionLong:{"$regex":new RegExp(regex,'i')}},
						 {descriptionShort:{"$regex":new RegExp(regex,'i')}}];
        }
		var statuses = ['N','U','S','SN'], types = ['S','A','L'];
			
		if(statuses.indexOf(status)>-1)
			query.status=status;
		
		if(types.indexOf(type)>-1)
			query.type=type;
			
		if(minPrice>0 && !isNaN(parseFloat(minPrice))){
			if(!query.price)
				query.price = {};
		
			query.price["$gte"] = parseFloat(minPrice);}
		
		if(maxPrice>0 && !isNaN(parseFloat(maxPrice))){
			if(!query.price)
					query.price = {};
			query.price["$lte"] = parseFloat(maxPrice);}
			
		var orderBy = req.query.order || "d", orderOrientation = req.query.orientation || -1;
        orderOrientation = parseInt(orderOrientation);
        switch(orderBy){
            case "d":
                orderBy = {createdAt:orderOrientation};
                break;
            case "p":
                orderBy = {price:orderOrientation};
                break;
            case "r":
                orderBy = {rate:-1};
                break;
            default:
                orderBy = {createdAt:-1};
                break;
        } 
        
        
        
		db.collection('items').find({"$query":query,"$orderby":orderBy}).skip(skip).limit(limit).toArray(function(err, items) {
			if (err) {
				res.json({e:1});
            }else{
				db.collection('items').count(query,function(err,count){
					if(err){
						res.json({e:2,mes:"Error páginando resultados"});
						console.log(err);
					}else{
						utils.giveUsersToItems(items,db,function(itms){
							utils.giveCategoryToItems(db,itms,function(ims){
								if(req.query.token && req.query.tokensecret){
									utils.checkHash(db, req.query.token, req.query.tokensecret, function(usr){
										if(usr){
											utils.itemsInFavAndCart(db,usr._id,ims,function(its){
												res.json({e:0,items:its,total:count});
											});
										}else    
											res.json({e:0,items:ims,total:count});
									});
								}else 
									res.json({e:0,items:ims,total:count});
							});
						});
					}
				});
			}
		});
	});

router.route('/category')
	.get(function(req,res){
		var db = req.db,
			term = req.query.term,
			skip = req.query.start || 0,
			limit = req.query.max || 10;
		var regex = '/'+term+'/i';
		db.collection('categories').find({"$query":{name:new RegExp(regex,'i')},"$orderby":{name:1}}).skip(skip).limit(limit).toArray(function(err, items) {
			if (err) {
                console.log(err);
				res.json({e:1})
            }else{
				res.json({e:0,items:items});
			}
		});
	});

router.route('/')
	.get(function(req,res){
		var db = req.db,
			term = req.query.term,
			skip = req.query.start || 0,
			limit = req.query.max || 10;
		var cats, items, done = 0;
		function process(type, res) {
            if (type==0) {
                items = res;
            }
			if (type==1) {
                cats = res;
            }
			if (done==2) {
                res.json({
						e:0,
						items: items,
						cats : cats
					});
            }
			done++;
        }
		
		var regex = '/'+term+'/i';
		db.collection('categories').find({"$query":{name:new RegExp(regex,'i')},"$orderby":{name:1}}).skip(skip).limit(limit).toArray(function(err, cats) {
			if (err) {
                console.log(err);
            }else{
				process(1,cats);
			}
		});	
		
		db.collection('items').find({"$query":{name:new RegExp(regex,'i')},"$orderby":{name:1}}).skip(skip).limit(limit).toArray(function(err, items) {
			if (err) {
                console.log(err);
            }else{
				process(0,items);
			}
		});
			
	});



module.exports = router;