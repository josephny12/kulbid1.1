var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var mailer = require('../utils/mailer');
var notification = require('../utils/notification');
var ObjectID = require('mongoskin').ObjectID;

function findReplies(comments,db,globalReply,userID,cb){
	var cl = comments.length, round = 1, i = 0;
	
	function callBack(){
		if(round == cl)
			cb(comments);
		round++;
	}

	function handleLoop(k){
		db.collection('comments').find({"$query":{replyTo:new ObjectID(comments[i]._id)},"$orderby":{date:1}}).toArray(function(err,cunts){
			if(err){
				console.log(err);
			}
			utils.giveUsersToItems(cunts,db,function(cmts){
					comments[k].replies = cmts;
					callBack();
			});
		});
	}
	
	if(cl>0)
		for(;i<cl;i++){
			if(globalReply){
				comments[i].global = true;
				comments[i].canReply = true;
			}else if(userID && userID.equals(new ObjectID(comments[i].user._id)))
					comments[i].canReply = true;
			else
				comments[i].canReply = false;
			comments[i].replies = [];
			handleLoop(i);
		}
	else
		cb(comments);

}


router.route('/store/:name')
	.get(function(req,res){
		var db = req.db,
			username = req.params.name;
		utils.findUsername(db,username,function(store){
			if(store && store.type=="2"){
				db.collection('comments').find({"$query":{store:new ObjectID(store._id),replyTo:false},"$orderby":{time:-1}}).skip(0).limit(20).toArray(function(err,comments){
					if(err){
						res.json({e:2,mes:"Error al recuperar"});
					}else{
						db.collection('comments').count({store:new ObjectID(store._id),replyTo:false},function(err,count){
							utils.giveUsersToItems(comments,db,function(cmts){
								var token=false, tokenSecret=false;
								if(req.query.token && req.query.tokensecret){
									token = req.query.token;
									tokenSecret = req.query.tokensecret;
								}
									
								utils.checkHash(db,token,tokenSecret,function(user){
										var userID = false,
											globalReply = false;
										if(user){
											userID = new ObjectID(user._id);
											
											if(userID.equals(store._id))
												globalReply = true;
										}
										findReplies(cmts,db,globalReply,userID,function(cms){
											res.json({e:0,comments:cms,total:count});
										});
								});							
							});
						});
					}
				});
			}else{
				res.json({e:1,mes:"Usuario no encontrado"});
			}
		});	
			
	})
	.post(function(req,res){
		var token = req.body.token,
			tokenSecret = req.body.tokensecret,
            comment =   req.body.comment,
            db      =   req.db,
            username    =   req.params.name,
            replyTo =   req.body.replyto;
		if(!comment ||
		   !token ||
		   !tokenSecret ){
            res.json({
				e:1,
				mes:"No se han recibido todos los campos requeridos"
			});
			return;  
        }
		
        utils.checkHash(req.db, token, tokenSecret, function(user){
			if(user){
				utils.findUsername(db,username,function(store){
					if(store && store.type=="2"){
						if (replyTo) {
							if (ObjectID.isValid(replyTo)) {
								db.collection('comments').findOne({_id:new ObjectID(replyTo)},function(err,commnt){
									if(err){
										res.json({e:5,mes:"Error al recuperar comentario respuesta"});
									}else if (commnt) {
										if (commnt.replyTo) {
											res.json({e:6,mes:"No puedes responer una respuesta"});
										}else{
											var userObjectID = new ObjectID(user._id),
												storeUserObjectID = new ObjectID(store.user),
												commntUserObjectID = new ObjectID(commnt.user);
											if(userObjectID.equals(storeUserObjectID ) || userObjectID.equals(commntUserObjectID)){
												var data = {
													user : new ObjectID(user._id),
													store : new ObjectID(store._id),
													replyTo: new ObjectID(replyTo),
													comment: comment,
													time: Date.now()
												};
														
												if(!new ObjectID(user._id).equals(new ObjectID(commnt.user))){
													db.collection('users').findOne({_id:commnt.user},function(err,og){
														if(!err && og){
															var mails=[];
															for(var i = 0 ; i < og.mails.length ; i++)
																if(og.mails[i].confirmed)
																	mails[i] = og.mails[i].mail;
																			
																	var mailOptions = mailer.newReplyStore(og.particular.name,store.user,user.user, user.photo, comment);
																		mailOptions.to = mails.join(',');
																		mailer.sendMail(mailOptions);
																	notification.newReplyStore(db,store.user,og._id);
														}else
															console.log(err);
													});
												}
												
												if(!new ObjectID(user._id).equals(new ObjectID(store._id))){
													db.collection('users').findOne({_id:new ObjectID(store._id)},function(err,seller){
														if(!err && seller){
															var mails=[];
															for(var i = 0 ; i < seller.mails.length ; i++)
																if(seller.mails[i].confirmed)
																	mails[i] = seller.mails[i].mail;
																			
															var mailOptions = mailer.newCommentStore(seller.particular.name,store.user,user.user, user.photo, comment);
																mailOptions.to = mails.join(',');
																mailer.sendMail(mailOptions);
															notification.newCommentStore(db,store.user,store._id);
														}else
															console.log(err);
													});
												}
															
															
												db.collection('comments').insert(data,function(err, result){
													if (err) {
														res.json({e:7,mes:"Error al guardar comentario"});
														return;
													}else {
														res.json({e:0,id:result.insertedIds[0]});
														return;
													}
												});
											}else{
												res.json({e:8,mes:"No tienes permiso para responder esta cadena"});
											}				
										}
									}else{
										res.json({e:5,mes:"ID de comentario invalido"});
									}
								});
							}else{
								res.json({e:4,mes:"ID de comentario invalido"});
							}
						}else{
							var data = {
								user : new ObjectID(user._id),
								store : new ObjectID(store._id),
								replyTo: false,
								comment: comment,
								time: Date.now()
							};
										
							if(!new ObjectID(user._id).equals(new ObjectID(store._id))){
								db.collection('users').findOne({_id:new ObjectID(store._id)},function(err,seller){
									if(!err && seller){
										var mails=[];
										for(var i = 0 ; i < seller.mails.length ; i++)
											if(seller.mails[i].confirmed)
												mails[i] = seller.mails[i].mail;
													
											var mailOptions = mailer.newCommentStore(seller.particular.name,store.user,user.user, user.photo, comment);
												mailOptions.to = mails.join(',');
												mailer.sendMail(mailOptions);
											notification.newCommentStore(db,store.user,seller._id);
									}
								});
							}
										
										
							db.collection('comments').insert(data,function(err, result){
								if (err) {
									res.json({e:7,mes:"Error al guardar comentario"});
									return;
								}else{
									res.json({e:0,id:result.insertedIds[0]});
									return;
								}
							});
						}
					}else
						res.json({e:2,mes:"Tienda no encontrada"});
				});
			}else
				res.json({e:1,mes:"Token invalido"});
		});
		
		
	});


router.route('/item/:id')
    .get(function(req,res){
        var db      =   req.db,
			skip = req.query.start || 0,
			limit = req.query.max || 10,
            itemi    =   req.params.id;
		limit = parseInt(limit);
		skip = parseInt(skip);
        if(ObjectID.isValid(itemi)){
			db.collection('items').findOne({_id:new ObjectID(itemi)},function(err,item){
				if(err)
					res.json({e:2,mes:"Error al recuperar articulo"});
				else if(item){
					db.collection('comments').find({"$query":{item:new ObjectID(itemi),replyTo:false},"$orderby":{time:-1}}).skip(skip).limit(limit).toArray(function(err,comments){
						if(err){
							res.json({e:2,mes:"Error al recuperar"});
						}else{
							db.collection('comments').count({item:new ObjectID(itemi),replyTo:false},function(err,count){
								utils.giveUsersToItems(comments,db,function(cmts){
									var token=false, tokenSecret=false;
									if(req.query.token && req.query.tokensecret){
										token = req.query.token;
										tokenSecret = req.query.tokensecret;
									}
									
									utils.checkHash(db,token,tokenSecret,function(user){
										var itemUserID = new ObjectID(item.user),
											userID = false,
											globalReply = false;
										if(user){
											userID = new ObjectID(user._id);
											
											if(userID.equals(itemUserID))
												globalReply = true;
										}
										findReplies(cmts,db,globalReply,userID,function(cms){
											res.json({e:0,comments:cms,total:count});
										});
									});
									
									
								});
							});
						}
					});
				}else
					res.json({e:2,mes:"No se ha encontrado el articulo"});
				
			});
        }else{
            res.json({
				e:1,
				mes:"ID de Item invalida"
			});
        }
    })
    .post(function(req,res){
        var token = req.body.token,
			tokenSecret = req.body.tokensecret,
            comment =   req.body.comment,
            db      =   req.db,
            item    =   req.params.id,
            replyTo =   req.body.replyto;
		if(!ObjectID.isValid(item) ||
           !token ||
		   !tokenSecret ){
            res.json({
				e:1,
				mes:"No se han recibido todos los campos requeridos"
			});
			return;  
        }
        utils.checkHash(req.db, token, tokenSecret, function(user){
			if(user){
				db.collection('items').findOne({_id:new ObjectID(item)},function(err,item){
					if(err){
						res.json({e:2,mes:"Error al recuperar"});
					}else if(item){
						if (item.active==1) {
                            if (replyTo) {
                                if (ObjectID.isValid(replyTo)) {
                                    db.collection('comments').findOne({_id:new ObjectID(replyTo)},function(err,commnt){
                                        if(err){
                                            res.json({e:5,mes:"Error al recuperar comentario respuesta"});
                                        }else if (commnt) {
                                            if (commnt.replyTo) {
                                                res.json({e:6,mes:"No puedes responer una respuesta"});
                                            }else{
												var userObjectID = new ObjectID(user._id),
													itemUserObjectID = new ObjectID(item.user),
													commntUserObjectID = new ObjectID(commnt.user);
												if(userObjectID.equals(itemUserObjectID) || userObjectID.equals(commntUserObjectID)){
													var data = {
														user : new ObjectID(user._id),
														item : new ObjectID(item._id),
														replyTo: new ObjectID(replyTo),
														comment: comment,
														time: Date.now()
													};
													
													if(!new ObjectID(user._id).equals(new ObjectID(commnt.user))){
														db.collection('users').findOne({_id:commnt.user},function(err,og){
															if(!err && og){
																var mails=[];
																for(var i = 0 ; i < og.mails.length ; i++)
																	if(og.mails[i].confirmed)
																		mails[i] = og.mails[i].mail;
																
																var mailOptions = mailer.newReply(og.particular.name,item.name,item.url,user.user, user.photo, comment);
																	mailOptions.to = mails.join(',');
																	mailer.sendMail(mailOptions);
																notification.newReplyItem(db,item.name,item.url,og._id);
															}else
																console.log(err);
														});
													}
													
													
													
													if(!new ObjectID(user._id).equals(new ObjectID(item.user))){
														db.collection('users').findOne({_id:item.user},function(err,seller){
															if(!err && seller){
																var mails=[];
																for(var i = 0 ; i < seller.mails.length ; i++)
																	if(seller.mails[i].confirmed)
																		mails[i] = seller.mails[i].mail;
																
																var mailOptions = mailer.newComment(seller.particular.name,item.name,item.url,user.user, user.photo, comment);
																	mailOptions.to = mails.join(',');
																	mailer.sendMail(mailOptions);
																notification.newCommentItem(db, item.name, item.url, seller._id);
															}else
																console.log(err);
														});
													}
													
													
													db.collection('comments').insert(data,function(err, result){
														if (err) {
															res.json({e:7,mes:"Error al guardar comentario"});
															return;
														}else {
															res.json({e:0,id:result.insertedIds[0]});
															return;
														}
													});
												}else{
													res.json({e:8,mes:"No tienes permiso para responder esta cadena"});
												}
                                                
                                            }
                                        }else{
                                            res.json({e:5,mes:"ID de comentario invalido"});
                                        }
                                    });
                                }else{
                                	res.json({e:4,mes:"ID de comentario invalido"});
                                }
                            }else{
                                var data = {
                                    user : new ObjectID(user._id),
                                    item : new ObjectID(item._id),
                                    replyTo: false,
                                    comment: comment,
									time: Date.now()
                                };
								
								if(!new ObjectID(user._id).equals(new ObjectID(item.user))){
									db.collection('users').findOne({_id:item.user},function(err,seller){
										if(!err && seller){
											var mails=[];
											for(var i = 0 ; i < seller.mails.length ; i++)
												if(seller.mails[i].confirmed)
													mails[i] = seller.mails[i].mail;
											console.log(mails);
											
											var mailOptions = mailer.newComment(seller.particular.name,item.name,item.url,user.user, user.photo, comment);
												mailOptions.to = mails.join(',');
												mailer.sendMail(mailOptions);
											notification.newCommentItem(db, item.name, item.url, seller._id);
										}
									});
								}
								
								
                                db.collection('comments').insert(data,function(err, result){
                                    if (err) {
                                        res.json({e:7,mes:"Error al guardar comentario"});
                                        return;
                                    }else{
                                        res.json({e:0,id:result.insertedIds[0]});
                                        return;
                                    }
                                });
                            }
                        }else{
                            res.json({e:3,mes:"Es imposible comentar este articulo"});
                        }
					}else{
						res.json({e:2,mes:"ID de producto invalido"});
                    }
				});
			}else{
				res.json({e:2,mes:"Token invalido"});
			}
		 });
    })
    .put(function(req,res){
        
    })
    .delete(function(req,res){
        
    });  

module.exports = router;