var express = require('express');
var multer = require('multer');
var router = express.Router();
var Parse = require('csv-parse');
var Hashids = require("hashids"),
    hashids = new Hashids("kulbid is great ebay is lame");
var utils = require('../utils/utils');
var fileUtils = require('../utils/fileUtils');
var upload = multer({ dest: './temp/' });
var ObjectID = require('mongoskin').ObjectID;
var mailer = require('../utils/mailer');
var fs = require('fs');
var Long = require('mongodb').Long;


function addToHistory(db,token,tokenSecret,itemID) {
    utils.checkHash(db, token, tokenSecret, function(user){
        if (user) {
			utils.pullAndPush(db.collection('history'),
				{user:new ObjectID(user._id)},
					{
						options:{},
						statement:{"$pull":{items:{item:new ObjectID(itemID)}}}
					},
					{
						options:{upsert: true, multi: true},
						statement:{"$push":{items:{$each:[{item:new ObjectID(itemID),date: Date.now()}],$position:0}}}
					},
					function(success){
						
					});
        }
    });
}

router.route('/shop/:urlname')
    .get(function(req,res){
        var urlname = encodeURIComponent(req.params.urlname),
            db = req.db;
        console.log(urlname);
        db.collection('items').findOne({url:urlname,active:{"$ne":4}},function(err,item){
			if(err){
            	res.json({e:2,mes:"Error al recuperar"});
			}else if(item){
                if (req.query.token && req.query.tokensecret) {
                    addToHistory(db, req.query.token, req.query.tokensecret, item._id);
                }
				
				var giveBid = function(item){
					if(item.type=="A"){
						utils.getHighestBid(db, item._id, function(bid){
							if(bid)
								item.price = bid.bid;
							res.json({e:0,item:item});
						});
					}else
						res.json({e:0,item:item});
				};
				
				utils.getFullUser(db,item.user,function(user){
					if(user){
						item.user = user;
					}
                    if(req.query.token && req.query.tokensecret){
                        utils.checkHash(req.db, req.query.token, req.query.tokensecret, function(usr){
                            if(usr){
                                utils.isInFavAndCart(db,usr._id,item._id,function(isFav,isInCart){
									item.isFavorite = isFav;
									item.isInCart = isInCart;
									giveBid(item);
                                });
                            }else    
								giveBid(item);
                        });
                    }else
						giveBid(item);
				});
			}else
            	res.json({e:3,mes:"Item no encontrado"});
		});
    });  

router.route('/details/:id')
    .get(function(req,res){
       var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            id = req.params.id,
            db = req.db;
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user){
                    db.collection('items').findOne({_id:new ObjectID(id),active:{"$ne":4}},function(err,item){
                        if(err)
                            res.json({e:2,mes:"Error al encontrar articulo"});
                        else if(item){
                            var itemUserID = new ObjectID(item.user),
                                userID = new ObjectID(user._id);
                            if(userID.equals(itemUserID) || user.priv==2){
                                db.collection('categories').findOne({_id:new ObjectID(item.category)},function(err,pcat){
                                    if(err) console.log(err);
                                    if(pcat)
                                        item.parentCategory = pcat.parent;
                                    res.json({e:0,item:item});
                                });
                            }else{
                                res.json({e:4,mes:"No tienes permisos para editar este articulo"});
                            }
                        }else
                            res.json({e:3,mes:"Articulo no encontrado"});
                    });
                }else
                    res.json({e:1,mes:"Token invalido"});
            });
            
        }else
            res.json({e:1,mes:"ID invalido"});
    });

router.route('/changepictures/:id')
    .put(upload.array('itempics',5),function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db,
            id = req.params.id,
            files = req.files;
        if(ObjectID.isValid(id)){
            utils.checkHash(db, token, tokenSecret, function(user){
			if(user){
                db.collection('items').findOne({_id:new ObjectID(id),active:{"$ne":4}},function(err,item){
                    if(err)
                        res.json({e:2,mes:"Error al encontrar articulo"});
                    else if(item){
                        var itemID = new ObjectID(id),
                            itemUserID = new ObjectID(item.user),
                            userID = new ObjectID(user._id);
                        if(userID.equals(itemUserID) || user.priv==2){
                            if(files.length>0){
                                fileUtils.saveFile(req.files,"image",function(){
                                    if(!arguments[0]){
                                        res.json({
                                            e:4,
                                            mes:"Error al guardar el archivo"
                                        });
                                        return;
                                    }
                                    var pictures = arguments[0];
                                    var oldPictures = item.images;
                                    
                                    db.collection('items').update({_id:itemID},
                                        {$set:{images:pictures}},
                                        function(err){
                                            if(err){
                                                var il = pictures.length,j=0;
                                                for(;j<il;j++){
                                                    //fileUtils.deleteFile("routes/uploads/husk/"+pictures[j]);
                                                }
                                                res.json({e:6,mes:"Error al actualizar imagenes"});
                                            }else{
                                                var ol = oldPictures.length,k=0;
                                                for(;k<ol;k++){
                                                    //fileUtils.deleteFile("routes/uploads/husk/"+oldPictures[k]);
                                                }
                                                res.json({e:0});
                                            }
                                    });
                                    
                                });
                            }else
                                res.json({e:5,mes:"Debes enviar al menos un archivo"});
                        }else{
                            res.json({e:4,mes:"No tienes permisos para editar este articulo"});
                        }
                    }else
                        res.json({e:3,mes:"Articulo no encontrado"});
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
        }else{
            res.json({e:1,mes:"ID invalido"});   
        }    
    });


router.route('/edit/:id')
	.put(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db,
            id = req.params.id;
        if(ObjectID.isValid(id)){
            utils.checkHash(db,token,tokenSecret,function(user){
                if(user){
                    db.collection('items').findOne({_id:new ObjectID(id),active:{"$ne":4}},function(err,item){
                        if(err)
                            res.json({e:2,mes:"Error al encontrar articulo"});
                        else if(item){
                            var itemUser = new ObjectID(item.user);
                            if(itemUser.equals(new ObjectID(user._id))  || user.priv==2){
                                db.collection('tax').findOne({"$query":{},"$orderby":{_id:-1}},function(err,tax){
                                    tax = tax.tax || 5;
                                    tax = tax / 100;
                                    
                                    var body = req.body;
                                    if(!body.token ||
                                       !body.tokensecret ||
                                      !body.name ||
                                      !body.descriptionShort ||
                                      !body.descriptionLong ||
                                      !body.price ||
                                      !body.stock ||
                                      !ObjectID.isValid(body.category) ||
                                      !body.type  ||
                                      !body.status ||
                                      !body.active  ||
                                      !body.time){
                                        res.json({
                                                e:1,
                                                mes:"No se han recibido todos los campos requeridos"
                                            });
                                            return;
                                    }
                                    
                                    if(isNaN(body.price) || body.price<1 ||
                                       isNaN(body.stock) || body.stock<0){
                                        res.json({
                                                e:1,
                                                mes:"No se han recibido todos los campos requeridos"
                                            });
                                            return;}
                                            
                                    if(body.price>99999999999)
                                        body.price = 99999999999;
                                    
                                    if(body.stock > 99999999999)
                                        body.stock = 99999999999;
                                    
                                    
                                    var statuses = ['N','U','S','SN'], types = ['S','A','L'];
                                    if(types.indexOf(body.type) == -1 || statuses.indexOf(body.status) == -1){
                                        res.json({e:1,mes:"Tipo o estado invalido"});
                                        return;
                                    }
									
									if(body.type=='A')
										body.stock = 1;
                                    
                                    if(body.active <1 && body.active>3){
                                        res.json({e:1,mes:"Nivel de actividad invalido"});
                                        return;
                                    }
                                    
                                    tax = Math.round((parseFloat(body.price) * tax) * 100) / 100;
                                    var data = {
                                        name: 				body.name,
                                        descriptionLong:	body.descriptionLong,
                                        descriptionShort:	body.descriptionShort,
                                        category:			new ObjectID(body.category),
                                        stock:				parseInt(body.stock),
                                        price:				parseFloat(body.price),
                                        tax:                tax+4,
                                        type:				body.type,
                                        status:				body.status,
                                        active:             parseInt(body.active),
                                        time:				Date.now() + (parseInt(body.time)*86400000),
                                        lastModified:		Date.now()
                                    };
                                    
                                    if(body.lat && body.lng) {
                                        data.position = {
                                            type: "Point",
                                            coordinates: [parseFloat(body.lng),parseFloat(body.lat)]
                                        };
                                    }
                                    db.collection('items').update({_id:new ObjectID(id)},
                                        {"$set":data}, function(err){
                                        if(err){
                                            res.json({e:1,mes:"Error al actualizar"});
                                            console.log(err);   
                                        }else
                                            res.json({e:0});
                                    });
                                });
                            }else{
                                res.json({e:3,mes:"No tienes permiso para modificar este articulo"});
                            }
                        }else
                            res.json({e:3,mes:"Articulo no encontrado"});
                    });
                }else{
                    res.json({e:2,mes:"Token invalida"});
                }
            });  
        }else{
            res.json({e:1,mes:"ID Invalido"});
        }   
            
            
	});
router.route('/delete/:id')
    .delete(function(req,res){
        var db = req.db,
            id = req.params.id,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
         
        if(ObjectID.isValid(id)){
            id = new ObjectID(id);
            utils.checkHash(db, token, tokenSecret, function(user){
               if(user && user.priv==2){
                    db.collection('items').update({_id:id},{"$set":{active:4}},function(err){
                        if(err)
                            res.json({e:3,mes:"Error al eliminado"});
                        else
                            res.json({e:0});
                    });
                }else{
                    res.json({e:2,mes:"Token invalido"});    
                } 
            });
        }else{
            res.json({e:1,mes:"ID invalido"});    
        }   
            
	});

router.route('/activate/:id')
    .put(function(req,res){
        if(ObjectID.isValid(id)){
            utils.checkHash(req.db, token, tokenSecret, function(user){
                if(user && user.priv==2){
                    db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
                        if(err)
                            res.json({e:3,mes:"Error al encontrar articulo"});
                        else if(item){
                            if(parseInt(item.stock)>0){
                                db.collection('items').update({_id:new ObjectID(id)},{$set:{active:1}},function(err){
                                    if(err)
                                        res.json({e:5,mes:"Error al pausar"});
                                    else
                                        res.json({e:0});
                                });
                            }else
                                res.json({e:6,mes:"No puedes activar un articulo sin stock"});
                            
                        }else
                            res.json({e:4,mes:"Articulo no encontrado"});
                        
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else
            res.json({e:1,mes:"ID invalido"});
    });

router.route('/pause/:id')
    .put(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            db = req.db,
            id = req.params.id;
            
        if(ObjectID.isValid(id)){
            utils.checkHash(db, token, tokenSecret, function(user){
                if(user && user.priv==2){
                    db.collection('items').findOne({_id:new ObjectID(id)},function(err,item){
                        if(err)
                            res.json({e:3,mes:"Error al encontrar articulo"});
                        else if(item){
                            db.collection('items').update({_id:new ObjectID(id)},{$set:{active:2}},function(err){
                                if(err)
                                    res.json({e:5,mes:"Error al pausar"});
                                else
                                    res.json({e:0});
                            });
                        }else
                            res.json({e:4,mes:"Articulo no encontrado"});
                        
                    });
                }else
                    res.json({e:2,mes:"Token invalido"});
            });
        }else
            res.json({e:1,mes:"ID invalido"});
        
    });

router.route('/uploadbulk')
	.post(upload.single('bulk'),function(req,res){
		var db = req.db;
		function parseCSVFile(sourceFilePath, baseTax, columns, onNewRecord, handleError, done){
			var source = fs.createReadStream(sourceFilePath);

			var linesRead = 0;

			var parser = Parse({
				delimiter: ',', 
				columns:columns,
				trim: true,
				auto_parse: true,
				
			});

			parser.on("readable", function(){
				var record;
				while (record = parser.read()) {
					linesRead++;
					onNewRecord(record,baseTax);
				}
			});

			parser.on("error", function(error){
				handleError(error);
			});

			parser.on("end", function(){
				done(linesRead);
			});

			source.pipe(parser);
		}
		
	
		var userID;
		
		function onNewRecord(body, baseTax){
			console.log('record');
			if(!body.titulo ||
			!body.descripcionCorta ||
			!body.descripcionLarga ||
			!(body.precio>0) ||
			!(body.stock>0) ||
			!ObjectID.isValid(body.categoria) ||
			!body.tipo ||
			!body.status ||
			!body.imagenUno){
				console.log('invalid record');
				return;
			}
			var statuses = ['N','U','S','SN'], types = ['S','A','L'];
			if(types.indexOf(body.tipo) == -1 || statuses.indexOf(body.status) == -1){
				console.log('invalid record');
				return;
			}
			
			if(body.tipo=='A')
			body.stock = 1;
			
			var token = hashids.encode(Date.now());
			var urlname = encodeURIComponent(body.titulo);
            
            var tax = Math.round((parseFloat(body.precio) * baseTax) * 100) / 100;
			var data = {
				name: 				body.titulo,
				user:				userID,
				descriptionLong:	body.descripcionLarga,
				descriptionShort:	body.descripcionCorta,
				category:			new ObjectID(body.categoria),
				url:				token+"_"+urlname,
				urlShort:			token,
				stock:				body.stock,
				price:				body.precio,
                tax:                tax+4,
				type:				body.tipo,
				images:				[],
				status:				body.status,
                active:             1,
				time:				Date.now() + (parseInt(body.tiempo)*86400000),
				createdAt:			Date.now()
			};
            
            
            if(body.lat && body.lng) {
                data.position = {
                    type: "Point",
                    coordinates: [parseFloat(body.lng),parseFloat(body.lat)]
                };
            }
            
			req.db.collection("items").insert(data,function(err, result){
				if (err) {
					console.log('err');
					return;
				}else {
					var files = [body.imagenUno];
					if(body.imagenDos)
						files.push(body.imagenDos);
					if(body.imagenTres)
						files.push(body.imagenTres);
					if(body.imagenCuatro)
						files.push(body.imagenCuatro);
					if(body.imagenCinco)
						files.push(body.imagenCinco);
					var itemID = result.insertedIds[0];	
					fileUtils.downloadAndSave(files,"image",function(doneFiles){
						console.log(doneFiles);
						if(doneFiles)
							req.db.collection("items").update({_id:new ObjectID(itemID)},{ $push: { images: { $each: doneFiles } } },function(err){
								if (err) {
									console.log(err);
									console.log('error al agregar archivos');
								}else {
									console.log('todo bien');
								}
							});
					});	
					return;
				}
			});
			
			
			
		}

		function onError(error){
			console.log(error);
		}

		function done(linesRead){
			//res.json({e:0,processed:linesRead})
		}

		utils.checkHash(req.db, req.body.token, req.body.tokensecret, function(user){
			if(user){
				var columns = true; 
				userID = new ObjectID(user._id);
                req.db.collection('tax').findOne({"$query":{},"$orderby":{_id:-1}},function(err,tax){
                    tax = tax.tax || 5;
                    tax = tax / 100;
                    parseCSVFile(req.file.path, tax, columns, onNewRecord, onError, done);
                });
				res.json({
						e:0,
						mes:"Archivo recibido, procesando"
				});
			}else{
				res.json({
						e:2,
						mes:"Token invalida"
				});
				return;
		}});
	
	
	});

router.route('/user/shop/:username')
	.get(function(req,res){
		var username = req.params.username,
			skip = req.query.start || 0,
			limit = req.query.max  || 10,
            db = req.db;
        skip = parseInt(skip);
        limit = parseInt(limit);
		utils.findUsername(db, username, function(user){
			if(user){
				db.collection('items').find({"$query":{user:new ObjectID(user._id),active:1,stock:{"$gt":0},time:{"$gt":Date.now()}},"$orderby":{name:1}}).skip(skip).limit(limit).toArray(function(err, items) {
					if (err) {
						res.json({e:2,mes:"Error al recuperar items"});
					}else if(items){
                        db.collection('items').count({user:new ObjectID(user._id),active:1,stock:{"$gt":0},time:{"$gt":Date.now()}},function(err,count){
                            if(err){
                                res.json({e:2,mes:"Error páginando resultados"});
                                console.log(err);
                            }else{
                                utils.giveUsersToItems(items,db,function(itms){
                                    utils.giveCategoryToItems(db,itms,function(ims){
                                        if(req.query.token && req.query.tokensecret){
                                            utils.checkHash(db, req.query.token, req.query.tokensecret, function(usr){
                                                if(usr){
                                                    utils.itemsInFavAndCart(db,usr._id,ims,function(its){
                                                        res.json({e:0,items:its,total:count});
                                                    });
                                                }else    
                                                    res.json({e:0,items:ims,total:count});
                                            });
                                        }else 
                                            res.json({e:0,items:ims,total:count});
                                    });
                                });
                            }
                        });
                    }else
						res.json({e:0,items:items,total:0}); 
				});
			}else{
				res.json({e:1,mes:"Usuario no encontrado"});
			}
		
		});
	});	
	
router.route('/')
	.get(function(req,res){
		var token = req.query.token,
            tokenSecret = req.query.tokensecret,
            limit = req.query.max || 10,
            skip = req.query.start || 0,
            db = req.db;
        
        skip = parseInt(skip);
        limit = parseInt(limit);
		utils.checkHash(req.db, token, tokenSecret, function(user){
			if(user){
				db.collection('items').find({user:new ObjectID(user._id),active:{"$ne":4}}).sort({active:1}).limit(limit).skip(skip).toArray(function(err,items){
					if(err){
						res.json({e:2,mes:"Error al recuperar"});
					}else{
                        db.collection('items').count({user:new ObjectID(user._id)},function(err,count){
                            if(err){
                                res.json({e:3,mes:"Error páginando resultados"});
                                console.log(err);
                            }else{
                                utils.giveUsersToItems(items,db,function(itms){
                                    utils.giveCategoryToItems(db,itms,function(ims){
                                            for(var i=0;i<ims.length;i++)
                                                ims[i].canEdit = true;
                                        
                                            res.json({e:0,items:ims,total:count});
                                        });
                                    });
                            }
                        });
					}
				});
			}else{
				res.json({e:1,mes:"Token invalido"});
			}
		 });
	})
	.post(upload.array('itempics',5),function(req,res){
		var body = req.body;
		
		if(!body.token ||
           !body.tokensecret ||
		  !ObjectID.isValid(body.category)){
			res.json({
				e:1,
				mes:"No se han recibido todos los campos requeridos"
			});
			return;
		}
		
		if(!body.name){
			res.json({
				e:1,
				mes:"Debes enviar el nombre del articulo"
			});
			return;
		}
		
		if(!body.descriptionShort){
			res.json({
				e:1,
				mes:"Debes enviar la descripción corta del articulo"
			});
			return;
		}
		
		if(!body.descriptionLong){
			res.json({
				e:1,
				mes:"Debes enviar la descripción corta del articulo"
			});
			return;
		}
		
		if(!body.price){
			res.json({
				e:1,
				mes:"Debes enviar el precio del articulo"
			});
			return;
		}
		
		if(!body.stock){
			res.json({
				e:1,
				mes:"Debes enviar el inventario del articulo"
			});
			return;
		}
		
		if(!body.type){
			res.json({
				e:1,
				mes:"Debes enviar el tipo de transacción"
			});
			return;
		}
		
		if(!body.status){
			res.json({
				e:1,
				mes:"Debes enviar el estado del artículo"
			});
			return;
		}
		
		if(!body.time){
			res.json({
				e:1,
				mes:"Debes enviar el tiempo de la publicación"
			});
			return;
		}
		
		body.price = parseFloat(body.price);
		body.stock = parseInt(body.stock);
		
		if(isNaN(body.price) || body.price<1 ||
           isNaN(body.stock) || body.stock<1){
			res.json({
					e:1,
					mes:"No se han recibido todos los campos requeridos"
				});
				return;}
				
		if(body.price>99999999999)
			body.price = 99999999999;
		
		if(body.stock > 99999999999)
			body.stock = 99999999999;
		
		
		var statuses = ['N','U','S','SN'], types = ['S','A','L'];
		if(types.indexOf(body.type) == -1 || statuses.indexOf(body.status) == -1){
			res.json({
					e:1,
					mes:"Datos invalidos"
				});
				return;
		}
		
		if(body.type=='A')
			body.stock = 1;
		
		utils.checkHash(req.db, body.token, body.tokensecret, function(user){
			if(user){
                //if(user.paypal){
                    if(req.files.length>0){
                        fileUtils.saveFile(req.files,"image",function(){
                            if(!arguments[0]){
                                res.json({
                                    e:4,
                                    mes:"Error al guardar el archivo"
                                });
                                return;
                            }
                            var token = hashids.encode(Date.now());
                            var urlname = encodeURIComponent(body.name);
                            var pictures = arguments[0];
                            req.db.collection('tax').findOne({"$query":{},"$orderby":{_id:-1}},function(err,tax){
                                tax = tax.tax || 5;
                                tax = tax / 100;
                                tax = Math.round((parseFloat(body.price) * tax) * 100) / 100;
                            
                                var data = {
                                    name: 				body.name,
                                    user:				new ObjectID(user._id),
                                    descriptionLong:	body.descriptionLong,
                                    descriptionShort:	body.descriptionShort,
                                    category:			new ObjectID(body.category),
                                    url:				token+"_"+urlname,
                                    urlShort:			token,
                                    stock:				parseInt(body.stock),
                                    price:				parseFloat(body.price),
                                    tax:                tax+4,
                                    type:				body.type,
                                    status:				body.status,
                                    active:             1,
                                    images:				pictures,
                                    time:				Date.now() + (parseInt(body.time)*86400000),
                                    createdAt:			Date.now()
                                };
                                
                                if(body.lat && body.lng) {
                                    data.position = {
                                        type: "Point",
                                        coordinates: [parseFloat(body.lng),parseFloat(body.lat)]
                                    };
                                }
                                req.db.collection("items").insert(data,function(err, result){
                                    if (err) {
                                        console.log(err);
                                        var il = pictures.length,j=0;
                                        for(;j<il;j++){
                                            //fileUtils.deleteFile("routes/uploads/husk/"+arguments[0][j]);
                                        }
                                        res.json({e:6,mes:"Error al guardar item"});
                                        
                                        return;
                                    }else{
                                        utils.pullAndPush(req.db.collection('users'),
                                            {_id:new ObjectID(user._id)},
                                            {
                                                options:{},
                                                statement:{"$pull":{categories:new ObjectID(body.category)}}
                                            },
                                            {
                                                options:{upsert: true, multi: true},
                                                statement:{"$push":{categories:{"$each":[new ObjectID(body.category)],$position:0}}}
                                            },
                                            function(success){
                                                console.log("did it happened? "+success);
                                            }
                                        );
                                        res.json({e:0,id:result.insertedIds[0],url:data.url,urlShort:data.urlShort});
                                        return;
                                    }
                                });
                            });
                        });
                    }else{
                        res.json({
                            e:3,
                            mes:"No se han recibido las fotos del articulo"
                        });
                        return;
                    }
                /*}else
                    res.json({e:2,mes:"Debes asociar una cuenta de paypal para poder publicar"});*/
			}else{
				res.json({
					e:2,
					mes:"Token invalida"
				});
				return;
			}
		});
	});


module.exports = router;