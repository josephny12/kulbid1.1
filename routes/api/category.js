var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './temp/')   
    },
    filename: function (req, file, cb) {
        cb(null, path.extname(file.originalname))      
    }
})
var upload = multer({ storage: storage });
var utils = require('../utils/utils');
var fileUtils = require('../utils/fileUtils');
var ObjectID = require('mongoskin').ObjectID;
var fs = require("fs");

//db.ObjectID.createFromHexString
function findCatByID(db,id,cb){
	db.collection("categories").findOne({"_id":new ObjectID(id)},function(err,cat){
		if(cat){
			cb(cat);
		}else
			cb(false);
		}
	);
}

function findCatByUrlName(db,name,cb){
	db.collection("categories").findOne({"url_name":name},function(err,cat){
		if(cat){
			cb(cat);
		}else
			cb(false);
		}
	);
}

function findCatByName(db,name,cb){
	db.collection("categories").findOne({"name":name},function(err,cat){
		if(cat){
			cb(cat);
		}else
			cb(false);
		}
	);
}

router.route('/name/:name')
	.get(function(req,res){
		findCatByName(req.db,req.params.name,function(cat){
			if(cat){
				res.json({e:0,category:cat});
			}else{
				res.json({e:1,mes:"Categoria invalida"});
			}
		});
	});

router.route('/childs/name/:urlname')
	.get(function(req,res){
		findCatByUrlName(req.db,req.params.urlname,function(cat){
			if(cat){
				req.db.collection('categories').find({parent:cat._id}).toArray(function(err, result){
					if (err) {
						res.json({e:2,mes:"Error al cargar categorias"});
					}else {
						res.json({e:0,categories:result});
					}

				});
			}else{
				res.json({e:1,mes:"No existe una categoria con ese nombre"});
				return;
			}
		});
	});

router.route('/childs/id/:id')
	.get(function(req,res){
		findCatByID(req.db,req.params.id,function(cat){
			if(cat){
				req.db.collection('categories').find({parent:cat._id}).toArray(function(err, result){
					if (err) {
						res.json({e:2,mes:"Error al cargar categorias"});
					}else {
						res.json({e:0,categories:result});
					}
				});
			}else{
				res.json({e:1,mes:"No existe una categoria con ese nombre"});
				return;
			}
		});
	});
	
router.route('/parents')
		.get(function(req,res){
			req.db.collection('categories').find({parent:0}).toArray(function(err, result){
				if (err) {
					res.json({e:1,mes:"Error al cargar categorias"});
				}else {
					res.json({e:0,categories:result});
				}	
			});
		});

router.route('/changepic/:id')
	.put(upload.single('categorypic'),function(req,res, next){
		db.collection('categories').findOne({_id:new ObjectID(req.params.id)},function(err,category){
			if(err)
				res.json({e:2,mes:"Error al cargar imagen"});
			else if(category){
				fileUtils.saveFile([req.file],"image",function(){
					if(!arguments[0]){
						res.json({
							e:3,
							mes:"Error al guardar el archivo"
						});
						return;
					}
					var path= arguments[0][0];
					
					db.collection('categories').update({_id:new ObjectID(req.params.id)},
                        {$set:{image:path}},
							function(err){
                                if(err){
                                    //fileUtils.deleteFile("routes/uploads/husk/"+path);
                                    res.json({e:6,mes:"Error al actualizar imagenes"});
                                }else{
                                    //fileUtils.deleteFile("routes/uploads/husk/"+category.image);
                                    res.json({e:0});
								}
							});
				});
			}else
				res.json({e:3,mes:"Categoria no encontrada"});
		});
	});
	
router.route('/details/:id')
	.get(function(req,res){
		if(ObjectID.isValid(req.params.id)){
			req.db.collection('categories').findOne({_id:new ObjectID(req.params.id)},function(err,category){
				if(err){
					res.json({e:2,mes:"Error al recuperar categoria"});
				}else if(category){
					res.json({e:0,category:category});
				}else
					res.json({e:2,mes:"Categoria no encontrada"});
			});
		}else
			res.json({e:1,mes:"ID invalida"});
	});

router.route('/delete/:id')
	.delete(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			db = req.db,
			id = req.params.id;
			
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			utils.checkHash(db, token, tokenSecret, function(user){
				if(user && user.priv==2){
					db.collection('categories').update({parent:id},{"$set":{parent:0}});
					db.collection('categories').remove({_id:id},function(err){
						if(err)
							res.json({e:3,mes:"Error al eliminar"});
						else
							res.json({e:0});	
					});
				}else
					res.json({e:2,mes:"Token invalido"});
			});
		}else{
			res.json({e:1,mes:"ID invalido"});
		}
	});

router.route('/edit/:id')
	.put(upload.single('categorypic'),function(req,res){
		//agregar categoria
		var body = req.body,
			token = body.token,
			id = req.params.id,
			tokenSecret = body.tokensecret,
			db = req.db;
		if(!body.name ||
		  !ObjectID.isValid(id)){
			res.json({
				e:7,
				mes:"Debes enviar todos los campos requeridos"
			});
			return;
		}
		id = new ObjectID(id);
		utils.checkHash(db, token, tokenSecret, function(user){
			if(user && user.priv==2){
				
				db.collection('categories').findOne({_id:id},function(err,category){
					if(err){
						res.json({e:2,mes:"Error al recuperar categoria"});
					}else if(category){
					
						
						var saveCategory = function(path){
							var name	=	body.name,
							urlName	=	encodeURIComponent(name.replace(" ",'_')),
							parent	=	body.parent;
							findCatByUrlName(req.db,urlName, function(cat){
								if(cat && !new ObjectID(cat._id).equals(id)){
									res.json({
										e:4,
										mes:"Ya existe una categoria con ese nombre"
									});
									return;
								}else{
									if(ObjectID.isValid(parent)){
										var findCatCallback = function(cat){
											if(cat && !new ObjectID(cat._id).equals(id)){
												var data = {
													name	:	name,
													parent	:	new ObjectID(parent),
													url_name:	urlName
												};
												if(path)
													data.image = path;
												req.db.collection('categories').update({_id:id},{"$set":data},function(err){
													if (err) {
														if(path)
															//fileUtils.deleteFile("routes/uploads/husk/"+path);
														res.json({e:1,mes:"Error al guardar categoria en la base de datos"});
													}else {
														if(path)
															//fileUtils.deleteFile("routes/uploads/husk/"+category.image);
														res.json({e:0,image:path});
													}
												});
											}else{
												res.json({e:2,mes:"Categoria padre no existe"});
											}
										};
										findCatByID(req.db,parent,findCatCallback);
									}else{
										var data = {
											name	:	name,
											url_name:	urlName
										};
										if(path)
											data.image = path;
										req.db.collection('categories').update({_id:id},{"$set":data},function(err){
											if (err) {
												if(path)
													//fileUtils.deleteFile("routes/uploads/husk/"+path);
												res.json({e:1,mes:"Error al guardar categoria en la base de datos"});
											}else {
												if(path)
													//fileUtils.deleteFile("routes/uploads/husk/"+category.image);
												res.json({e:0,image:path});
											}
										});
									}
								}
							});
						};
						var saveCallback = function(){
							if(!arguments[0]){
								res.json({
									e:3,
									mes:"Error al guardar el archivo"
								});
								return;
							}
							saveCategory(arguments[0][0]);
						};
						if(req.file)
							fileUtils.saveFile([req.file],"image",saveCallback);
						else
							saveCategory(false);
					}else
						res.json({e:2,mes:"Categoria no encontrada"});
				});
			}else
				res.json({e:1,mes:"Usuario invalido"});
		});
	});	

router.route('/')
	.get(function(req,res){
		req.db.collection('categories').find().toArray(function(err, result){
			if (err) {
				res.json({e:1,mes:"Error al cargar categorias"});
			}else {
				res.json({e:0,categories:result});
			}	
		});
	})
	
	.post(upload.single('categorypic'),function(req,res){
		//console.log(req.file.originalname);
		//agregar categoria
		var body = req.body;
		console.log(body);
		if(!body.name  ||
		  !req.file){
			res.json({
				e:7,
				mes:"Debes enviar todos los campos requeridos"
			});
			return;
		}
		function saveCallback(){
			if(!arguments[0]){
				res.json({
					e:3,
					mes:"Error al guardar el archivo"
				});
				return;
			}
			//console.log(arguments[0][0]);
			var path= arguments[0][0],
			name	=	body.name,
			urlName	=	encodeURIComponent(name.replace(/ /gi,'_')),
			parent	=	body.parent;

			console.log(urlName);
			
			
			findCatByUrlName(req.db,urlName, function(cat){
				if(cat){
					res.json({
						e:4,
						mes:"Ya existe una categoria con ese nombre"
					});
					return;
				}else{
					if(parent==0){
						var data = {
							name	:	name,
							parent	:	0,
							image	:	path,
							url_name:	urlName
						}
						req.db.collection('categories').insert(data,function(err, result){
							if (err) {
								res.json({e:1,mes:"Error al guardar categoria en la base de datos"});
							}else {
								res.json({e:0,id:result.insertedIds[0],image:path});
							}

						});
					}else if(ObjectID.isValid(parent)){
						function findCatCallback(cat){
							if(cat){
								var data = {
									name	:	name,
									parent	:	new ObjectID(parent),
									image	:	path,
									url_name:	urlName
								}
								req.db.collection('categories').insert(data,function(err, result){
									if (err) {
										res.json({e:1,mes:"Error al guardar categoria en la base de datos"});
									}else {
										res.json({e:0,id:result.insertedIds[0],image:path});
									}

								});
							}else{
								res.json({e:2,mes:"Categoria padre no existe"});
							}

						}
						findCatByID(req.db,parent,findCatCallback);
					}else{
							res.json({e:5,mes:"Solicitud invalida"});
						}
				}
			});
		}
		fileUtils.saveFile([req.file],"image",saveCallback);
	});

module.exports = router;
