var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var ObjectID = require('mongoskin').ObjectID;
var mailer = require('../utils/mailer');
var notification = require('../utils/notification');
var Hashids = require("hashids"),
    hashids = new Hashids("kulbid is great ebay is lame");
var Paypal = require('paypal-adaptive');

var paypalSdk = new Paypal({
    userId:    'josebaperezrodrigo_api1.hotmail.com',
    password:  '4GYT7LAQEE5BXJLH',
    signature: 'AAD3KozP8NzbIO4HlwmOVxfLKd60ArD2xNH-lkZBMfCqBp4LEiiV71L1',
    appId: 'APP-49E3692753674460U'
});


router.route('/')
    .get(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                utils.getDebt(db, user._id, function(debt){
                    if(debt){
                        debt.e=0;
                        res.json(debt);
                    }else
                        res.json({e:2,mes:"Error al recuperar deuda"});
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
        
    });
    
router.route('/pay')
    .post(function(req,res){
        var db = req.db,
            token = req.query.token,
            tokenSecret = req.query.tokensecret;
        utils.checkHash(db, token, tokenSecret, function(user){
            if(user){
                utils.getDebt(db, user._id, function(debt){
                    if(debt){
                        var successID = hashids.encode(Date.now()),
                            failID = hashids.encode(Date.now()-30000),
                            payload = {
                                requestEnvelope: {
                                    errorLanguage:  'en_US'
                                },
                                actionType:     'PAY',
                                currencyCode:   'MXN',
                                feesPayer:      'EACHRECEIVER',
                                memo:           'Pago de deuda en Kulbid',
                                cancelUrl:      'https://kulbid.com:444/paydebt/'+failID,
                                returnUrl:      'https://kulbid.com:444/paydebt/'+successID,
                                receiverList: {
                                    receiver: [
                                        {
                                            email:  'buyer-kulbid@gmail.com',
                                            amount: debt.total,
                                            primary:'true'
                                        }
                                    ]
                                }
                            };
                        
                        paypalSdk.pay(payload, function (err, response) {
                            if (err) {
                               console.log(response);
                               res.json({e:7,mes:"Error al redirigir a paypal"});
                            }else {
                                var transIDs = [];
                                for(var j = 0 ; i < debt.list.length ; j++)
                                    transIDs.push(new ObjectID(debt.list[i]._id));
                                var payT = {
                                    user: user._id,
                                    appliesTo: transIDs,
                                    payKey:response.payKey,
                                    sum: debt.total,
                                    successID: successID,
                                    failID: failID,
                                    url: response.paymentApprovalUrl,
                                    completed:false,
                                    status:'P'
                                };
                                db.collection('debtPayments').insert(payT,function(err){
                                    if(err)
                                        res.json({e:3,mes:"Error al registrar pago"});
                                    else
                                        res.json({e:0,url:response.paymentApprovalUrl});
                                });
                            }
                        });
                    }else
                        res.json({e:2,mes:"Error al recuperar deuda"});
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
        
    });
    
router.route('/status/:paystatus')
    .get(function(req,res){
        var db = req.db,
            paystatus = req.params.paystatus;
        db.collection('debtPayments').findOne({"$or":[{successID:paystatus},{failID:paystatus}]},function(err,result){
            if(err)
                res.json({e:1,mes:"Error al encontrar transacción"});
            else if(result){
                var params = {
                    paykey : result.payKey
                };
                
                paypalSdk.paymentDetails(params, function (err, response) {
                        if (err) {
                            res.json('Error al validar pago.');
                        } else {
                            switch(response.status){
                                case "COMPLETED":
                                    db.collection('debtPayments').update({_id:new ObjectID(result._id)},{"$set":{completed:true,status:"S"}});
                                    db.collection('transactions').update({_id:{"$in":result.appliesTo}},{"$set":{kulbidPayed:true}});
                                    res.json({e:0,mes:"Deuda saldada"});
                                    break;
                                case "PROCESING":
                                case "INCOMPLETE":
                                case "PENDING":
                                    var d = {
                                        payKey: result.payKey  
                                    };
                                    db.collection("paymentsToCheck").insert(d);
                                    res.json({e:5,mes:"Pago pendiente, te informaremos cuando finalice"});
                                    break;
                                case "ERROR":
                                    db.collection('debtPayments').update({_id:new ObjectID(result._id)},{$set:{completed:true,status:"F"}});
                                    res.json({e:5,mes:"Error al procesar pago"});
                                    break;
                                case "CANCELED":
                                    db.collection('debtPayments').update({_id:new ObjectID(result._id)},{$set:{completed:true,status:"C"}});
                                    res.json({e:5,mes:"Pago cancelado por el usuario"});
                                    break;
                            }
                        }
                    });
                
            }else
                res.json({e:1,mes:"Transacción no encontrada"});
            
        });
        
    });

module.exports = router;  