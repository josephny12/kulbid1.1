var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID;
var utils = require('../utils/utils');

router.route('/contacto')
    .get(function(req,res){
        req.db.collection('bloqueContacto').findOne(function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar los datos de contacto"});
            } else {
                res.json({e:0,result:result});
                console.log(result);
            }
        });
    })

router.route('/terminos')
    .get(function(req,res){
        req.db.collection('post').findOne({elemento: parseInt(1)}, function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar página"});
            } else {
                res.json({e:0,result:result});
                console.log(result);
            }
        });
    })

router.route('/privacidad')
    .get(function(req,res){
        req.db.collection('post').findOne({elemento: parseInt(2)}, function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar página"});
            } else {
                res.json({e:0,result:result});
                console.log(result);
            }
        });
    })

router.route('/actualizarcontacto')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret;
       utils.checkHash(db, token, tokenSecret, function(user){
            if(user && user.priv==2){
                var id = new ObjectID(body._id);
                var data = {
                    email: body.email,
                    direccion: body.direccion,
                    telefono: body.telefono
                };
                console.log(new ObjectID(body._id));
                db.collection('bloqueContacto').update({_id:id},{$set: data}, function(err){
                    if(err)
                        res.json({e:5,mes:"Error al actualizar información de contacto"});
                    else{
                        res.json({e:0});
                    }
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
    });

router.route('/actualizarsociales')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret;
       utils.checkHash(db, token, tokenSecret, function(user){
            if(user && user.priv==2){
                var id = new ObjectID(body._id);
                var data = {
                    youtube: body.youtube,
                    facebook: body.facebook,
                    instagram: body.instagram,
                    twitter: body.twitter
                };
                db.collection('bloqueRedesSociales').update({_id:id},{$set: data}, function(err){
                    if(err)
                        res.json({e:5,mes:"Error al actualizar información de redes sociales"});
                    else{
                        res.json({e:0});
                    }
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
    });

router.route('/actualizarterminos')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret;
       utils.checkHash(db, token, tokenSecret, function(user){
            if(user && user.priv==2){
                var id = new ObjectID(body.id);
                var data = {
                    texto: body.texto,
                    titulo: body.titulo
                };
                db.collection('post').update({_id:id},{$set: data}, function(err, result){
                    if(err)
                        res.json({e:5,mes:"Error al actualizar información del post"});
                    else{
                        res.json({e:0});
                    }
                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
    });

router.route('/actualizarprivacidad')
    .post(function(req,res){
        var db = req.db,
            body = req.body,
            token = body.token,
            tokenSecret = body.tokensecret;
       utils.checkHash(db, token, tokenSecret, function(user){
            if(user && user.priv==2){
                var id = new ObjectID(body.id);
                var data = {
                    texto: body.texto,
                    titulo: body.titulo
                };
                db.collection('post').update({_id:id},{$set: data}, function(err){
                    if (err) {
                        res.json({e:1,mes:"Error al guardar el post"});
                    }else {
                        res.json({e:0});
                    }

                });
            }else
                res.json({e:1,mes:"Token invalido"});
        });
    });

router.route('/social')
    .get(function(req,res){
        req.db.collection('bloqueRedesSociales').findOne(function(err, result){
            if (err) {
                res.json({e:1,mes:"Error al cargar las redes sociales"});
            } else {
                res.json({e:0,result:result});
                //console.log(result);
            }
        });
    })

module.exports = router;