var express = require('express');
var app = require('./app.js');
var https = require('http');

var httpsServer = https.createServer(app);
var servers = httpsServer.listen(8080);